package adapters;

import fr.vitamines.annuaire.android.R;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class NavigationDropDownRowAdapter extends BaseAdapter{

	private String[] data={"Annuaire","Sodexo","Oasis","Profil","A propos"};
	private Context context;
	private int[] resIconId={R.drawable.find_user_bleu,R.drawable.sodexo_bleu,R.drawable.oasis_bleu,R.drawable.profil_bleu,R.drawable.info};

	public NavigationDropDownRowAdapter(Context context) {
		this.context=context;
	}


	@Override
	public int getCount() {
		return data.length;
	}

	@Override
	public Object getItem(int position) {
		return null;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){
			LayoutInflater mInflater = (LayoutInflater)	context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
			convertView = mInflater.inflate(R.layout.navigation_drop_down_row, null);
		}
		ImageView icon = (ImageView) convertView.findViewById(R.id.navigation_icon);
		TextView title = (TextView) convertView.findViewById(R.id.navigation_title);
		title.setText(data[position]);
		icon.setImageResource(resIconId[position]);
		
		return convertView;
	}

}
