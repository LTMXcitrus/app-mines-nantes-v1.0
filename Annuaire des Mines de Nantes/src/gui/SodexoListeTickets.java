package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import main.Mulot;
import main.PersonneMulot;
import main.Sodexo;
import fr.vitamines.annuaire.android.R;
import adapters.ArrayAdapterTicketSodexo;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class SodexoListeTickets extends Activity implements ActionBar.OnNavigationListener{
	
	private boolean allumage=true;
	private EditText edit;

	private Sodexo compteUtilisateur;
	private PullToRefreshListView mPullRefreshListView;
	private ArrayAdapterTicketSodexo mArrayAdapter;

	private ActionBar bar;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sodexolistetickets);

		bar=getActionBar();
		bar.setTitle("Tickets");
		/*bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		SpinnerAdapter mSpinnerAdapter = new NavigationDropDownRowAdapter(this);
		bar.setListNavigationCallbacks(mSpinnerAdapter, this);
		bar.setSelectedNavigationItem(1);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);*/

		compteUtilisateur=SodexoConnexion.compteUtilisateur;

		mPullRefreshListView = 
				(PullToRefreshListView) findViewById(R.id.ticketlist);

		if(compteUtilisateur!=null){

			mArrayAdapter = new ArrayAdapterTicketSodexo(SodexoListeTickets.this,R.layout.itemticket,compteUtilisateur.getTickets());

			mPullRefreshListView.setAdapter(mArrayAdapter);
			mPullRefreshListView.setMode(Mode.PULL_FROM_END);
			mPullRefreshListView.setShowIndicator(false); //disable indicator


			System.out.println("nombre d'items: "+mArrayAdapter.getCount());
			System.out.println("nombre d'�l�ments affich�s :"+mPullRefreshListView.getChildCount());

			mPullRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
				public void onRefresh(PullToRefreshBase<ListView> refreshView) {
					new RefreshList().execute();
				}
			});

			mPullRefreshListView.setOnItemClickListener(new OnItemClickListener(){

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					String num = compteUtilisateur.getTickets()[position-1][0];
					Bundle args = new Bundle();
					args.putString("id", num);
					Intent intent = new Intent(SodexoListeTickets.this,AffichageTicket.class);
					intent.putExtras(args);
					startActivity(intent);
				}

			});
		}
		else{
			startActivity(new Intent(SodexoListeTickets.this,Accueil.class));
		}
	}

	private class RefreshList extends AsyncTask<Void,Void,Void>{

		protected Void doInBackground(Void... params) {
			try {
				compteUtilisateur.getMoreTickets();
			} catch (ClientProtocolException e) {
				System.out.println(e);
			} catch (IOException e) {
				System.out.println(e);
			}

			return null;
		}

		protected void onPostExecute(Void result){
			super.onPostExecute(result);
			mArrayAdapter=new ArrayAdapterTicketSodexo(SodexoListeTickets.this,R.layout.itemticket,compteUtilisateur.getTickets());
			mPullRefreshListView.setAdapter(mArrayAdapter);
			mPullRefreshListView.onRefreshComplete();
		}

	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(allumage){
			allumage=false;
			return true;
		}
		if(itemPosition==0){
			startActivity(new Intent(SodexoListeTickets.this,SearchActivity.class));
			return true;
		}
		if(itemPosition==2){
			Toast.makeText(SodexoListeTickets.this, "Prochainement...", Toast.LENGTH_LONG).show();
			return true;
		}
		/*if(itemPosition==1){
			startActivity(new Intent(SodexoConnexion.this,SodexoConnexion.class));
			return true;
		}*/
		if(itemPosition==3){
			Builder dialo= new  AlertDialog.Builder(SodexoListeTickets.this);
			dialo.setCancelable(true);
			LayoutInflater factory = LayoutInflater.from(SodexoListeTickets.this);
			final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
			dialo.setView(alertDialogView);
			TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
			demande.setText("Le mot de passe fort est n�cessaire pour �diter son profil: ");
			edit=(EditText) alertDialogView.findViewById(R.id.editpws);
			dialo.setTitle("Mot de passe fort");
			dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					String strongpassword=edit.getText().toString();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
					try {
						if(new GetProfil().execute(strongpassword).get().getCn()!="echec"){
							Intent intent = new Intent(SodexoListeTickets.this,EditprofilMulot.class);
							Bundle extras = new Bundle();
							extras.putString("strongpassword", strongpassword);
							intent.putExtras(extras);
							startActivity(intent);
						}
						else{
							Builder erreur= new  AlertDialog.Builder(SodexoListeTickets.this);
							erreur.setCancelable(true);
							erreur.setTitle("Mot de passe erron�!");
							erreur.show();
						}
					} catch (InterruptedException e) {
						System.out.println(e);
					} catch (ExecutionException e) {
						System.out.println(e);
					}
				}
			});
			dialo.show();
			return true;
		}
		else{
			return false;
		}
	}
	
	private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			Mulot m = SearchActivity.m;

			try {
				if(m!=null){
					SearchActivity.profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(SodexoListeTickets.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}
			return SearchActivity.profil;
		}
	}
}
