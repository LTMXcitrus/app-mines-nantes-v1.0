package gui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

import main.Mulot;

import org.apache.http.client.ClientProtocolException;

import fr.vitamines.annuaire.android.R;
import android.app.ActionBar;
import android.app.AlertDialog.Builder;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


public class Connexion extends Activity  {

	//d�claration des diff�rents widgets utilis�s
	private EditText identifiantET;
	private EditText passwordET;
	private Button valider;
	private String identifiant;
	private String weakpassword;
	private SharedPreferences preferences;
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.mainactivity);


		ActionBar bar = getActionBar();
		bar.setTitle("Connexion");

		preferences = PreferenceManager.getDefaultSharedPreferences(this);

		identifiantET=(EditText) findViewById(R.id.identifiant);
		passwordET=(EditText) findViewById(R.id.password);
		valider=(Button) findViewById(R.id.valider);	

		valider.setOnClickListener(new OnClickListener(){
			public void onClick (View view){
				//on r�cup�re ce qu'il y a dans les EditTexts
				identifiant=identifiantET.getText().toString();
				weakpassword=passwordET.getText().toString();
				//Si les identifiants sont bien  renseign�s, on continue
				if(identifiant!=null && weakpassword!=null){
					Mulot user = null;
					try {
						user = new Mulot(identifiant,weakpassword);
					} catch (UnsupportedEncodingException e) {
						System.out.println(e);
					}

					preferences.edit().putString("username", identifiant).putString("mdp", weakpassword ).commit();

					//Si le portable est bien connect� � internet, on lance l'activit� suivante
					if(isDeviceConnected(Connexion.this)){
						Connect connect = new Connect();
						boolean connected=false;
						try {
							connected = connect.execute(user).get();
						} catch (InterruptedException e) {
							System.out.println(e);
						} catch (ExecutionException e) {
							System.out.println(e);
						}
						if(connected){
							startActivity(new Intent(Connexion.this,SearchActivity.class));
						}
						else{
							Builder dialo= new  AlertDialog.Builder(Connexion.this);
							dialo.setCancelable(true);
							dialo.setTitle("Identifiant ou mot de passe erron�!");
							dialo.show();
						}
					}
					//Si le portable n'est pas connect� � internet on averti l'utilisateur
					else{
						Builder notConnected= new AlertDialog.Builder(Connexion.this);
						notConnected.setCancelable(true);
						notConnected.setPositiveButton("OK", new DialogInterface.OnClickListener(){
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								Intent intent = new Intent(Intent.ACTION_MAIN);
								intent.addCategory(Intent.CATEGORY_HOME);
								intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
								startActivity(intent);				
							}
						});
						LayoutInflater factory = LayoutInflater.from(Connexion.this);
						final View notconnectedDialog = factory.inflate(R.layout.device_not_connected_dialog, null);
						notConnected.setView(notconnectedDialog);
						notConnected.setTitle("Non connect�");
						notConnected.show();
					}
				}
			}
		});
		if(preferences.getString("username", null)!=null && preferences.getString("mdp",null)!=null){
			identifiantET.setText(preferences.getString("username",""));
			passwordET.setText(preferences.getString("mdp",""));
			valider.performClick();
		}
	}


	/**
	 * @param context --> activit� dans laquelle on se trouve
	 * @return boolean --> est ce que le portable (ou la tablette) est connect�e � internet
	 */
	public static boolean isDeviceConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected());
	}


	private class Connect extends AsyncTask<Mulot,Void,Boolean>{

		protected Boolean doInBackground(Mulot... params) {
			Mulot mulot = params[0];
			boolean result = false;
			try {
				result= mulot.connect(mulot.getKey());
			} catch (ClientProtocolException e) {
				System.out.println(e);
			} catch (IOException e) {
				System.out.println(e);
			}
			return result;
		}
	}
}