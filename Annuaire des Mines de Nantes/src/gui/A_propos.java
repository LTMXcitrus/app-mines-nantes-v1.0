package gui;

import fr.vitamines.annuaire.android.BuildConfig;
import fr.vitamines.annuaire.android.R;
import android.app.Activity;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.widget.TextView;

public class A_propos extends Activity{
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.a_propos);
		
		TextView appVersion =(TextView) findViewById(R.id.appversion);

		PackageInfo mPackageInfo=null;
		try {
			mPackageInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
		} catch (NameNotFoundException e) {
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		}
		if(mPackageInfo!=null){
			appVersion.setText("Version: "+mPackageInfo.versionName);
		}
	}

}
