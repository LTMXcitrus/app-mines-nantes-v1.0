package gui;


import java.io.IOException;
import java.io.UnsupportedEncodingException;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import main.Mulot;
import main.PersonneMulot;
import fr.vitamines.annuaire.android.R;
import adapters.NavigationDropDownRowAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class EditprofilMulot extends Activity implements ActionBar.OnNavigationListener{
	
	PersonneMulot PAncien;
	PersonneMulot PActualise;
	String strong;
	public String nom;
	public static Mulot m=null;
	
	private TextView nomEdit;

	private Bitmap bMap2;
	private ImageView photo;
	
	private Button editer;

	private String numberfixe="";
	private String numbermob="";
	private EditText fixeEdit;
	private EditText mobileEdit;
	

	
	private String mailint="";
	private String mailext="";
	private TextView mailintEdit;
	private EditText mailextEdit;

	private String ADR="";
	private EditText AdrEdit;

	private String ADR2="";
	private EditText Adr2Edit;

	private String ADR3="";
	private EditText Adr3Edit;

	private String CPO="";
	private EditText CpoEdit;

	private String promo="";
	private EditText promoEdit;

	private String ville="";
	private EditText villeEdit;

	private String etat="";
	private EditText etatEdit;

	private String Surnom="";
	private EditText SurnomEdit;

	private String roomNumber="";
	private EditText roomNumberEdit;

	public static String result;
	
	private String identifiant;
	private String weakpassword;
	
	private SharedPreferences preferences;
	
	
	public void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editprofilmulot);
		
		ActionBar bar = getActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		SpinnerAdapter mSpinnerAdapter = new NavigationDropDownRowAdapter(this);
		bar.setListNavigationCallbacks(mSpinnerAdapter, this);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);
		bar.setSelectedNavigationItem(3);
		
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		
		Bundle bundle=this.getIntent().getExtras();
		strong = bundle.getString("strongpassword");
		weakpassword=preferences.getString("mdp", null);
		identifiant=preferences.getString("username", null);
		
		photo = (ImageView) findViewById(R.id.photoedit);
		nomEdit= (TextView) findViewById(R.id.nomedit);
		editer=(Button) findViewById(R.id.editer);
		fixeEdit=(EditText)findViewById(R.id.fixeEdit);
		mobileEdit=(EditText)findViewById(R.id.mobileEdit);
		roomNumberEdit=(EditText) findViewById(R.id.roomNumberEdit);
		mailintEdit=(TextView) findViewById(R.id.mailintEdit);
		mailextEdit=(EditText) findViewById(R.id.mailextEdit);
		SurnomEdit=(EditText) findViewById(R.id.SurnomEdit);
		promoEdit=(EditText) findViewById(R.id.promoEdit);
		AdrEdit=(EditText) findViewById(R.id.adrEdit);
		Adr2Edit=(EditText) findViewById(R.id.Adr2Edit);
		Adr3Edit=(EditText) findViewById(R.id.Adr3Edit);
		CpoEdit=(EditText) findViewById(R.id.CpoEdit);
		villeEdit=(EditText) findViewById(R.id.VilleEdit);
		etatEdit=(EditText) findViewById(R.id.etatEdit);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mailintEdit.getWindowToken(), 0);
		
		
		taskEdit tsk = new taskEdit();
		tsk.execute();

	}
	
	private class taskEdit extends AsyncTask<Void,Void,String>{
		
		public taskEdit(){
		}

		@Override
		protected String doInBackground(Void... params) {
			PersonneMulot PAncien= SearchActivity.profil;
			m=SearchActivity.m;
			
				PActualise=PAncien;
				if(PAncien.getCn()!=null){
					nom=PAncien.getCn();
				}
				if(PAncien.getA_nickname()!=null){
					Surnom=PAncien.getA_nickname();
					System.out.println(Surnom);
				}
				if(PAncien.getA_promo()!=null){
					promo =PAncien.getA_promo();
					System.out.println(promo);
				}
				if(PAncien.getMail()!=null){
					mailint = PAncien.getMail();				 
				}
				if(PAncien.getHomephone()!=null){
					numberfixe=PAncien.getHomephone();
				}
				if(PAncien.getMobile()!=null){
					numbermob=PAncien.getMobile();
				}
				if(PAncien.getA_mailexterne()!=null){
					mailext = PAncien.getA_mailexterne();
				}
				if(PAncien.getADR()!=null){
					ADR=PAncien.getADR();
				}
				if(PAncien.getADR2()!=null){
					ADR2=PAncien.getADR2();
				}
				if(PAncien.getADR3()!=null){
					ADR3=PAncien.getADR3();
				}
				if(PAncien.getCPO()!=null){
					CPO=PAncien.getCPO();
				}
				if(PAncien.getVIL()!=null){
					ville=PAncien.getVIL();
				}
				if(PAncien.getState()!=null){
					etat=PAncien.getState();
				}
				if (PAncien.getChez()!=null){
					roomNumber= PAncien.getChez();
				}
				try {
					bMap2= m.getPhoto(PAncien.getUid());
				} catch (ClientProtocolException e) {
					System.out.println(e);
				} catch (IOException e) {
					System.out.println(e);
				} catch (JSONException e) {
					System.out.println(e);
				}
				System.out.println(bMap2==null);

				
			

			return "";
		
		}
		
		protected void onPostExecute(String f){
			
			
			SurnomEdit.setText(Surnom);
			promoEdit.setText(promo);
			int index = mailint.indexOf("@");
			mailintEdit.setText(mailint.substring(0, index)+"\n"+mailint.substring(index));
			mailextEdit.setText(mailext);
			fixeEdit.setText(numberfixe);
			mobileEdit.setText(numbermob);
			AdrEdit.setText(ADR);
			Adr2Edit.setText(ADR2);
			Adr3Edit.setText(ADR3);
			CpoEdit.setText(CPO);
			villeEdit.setText(ville);
			etatEdit.setText(etat);
			roomNumberEdit.setText(roomNumber);
			if(bMap2!=null){
				photo.setImageBitmap(Bitmap.createScaledBitmap(bMap2, 180, 240, false));
			}
			
			nomEdit.setText(nom);
			nomEdit.setTextSize(20);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mailintEdit.getWindowToken(), 0);
			
			editer.setOnClickListener(new OnClickListener(){
				public void onClick(View v){
					PActualise.setA_nickname(SurnomEdit.getText().toString());
					PActualise.setA_promo(promoEdit.getText().toString());
					PActualise.setA_mailexterne(mailextEdit.getText().toString());
					PActualise.setHomephone(fixeEdit.getText().toString());
					PActualise.setMobile(mobileEdit.getText().toString());
					PActualise.setADR(AdrEdit.getText().toString());
					PActualise.setADR2(Adr2Edit.getText().toString());
					PActualise.setADR3(Adr3Edit.getText().toString());
					PActualise.setCPO(CpoEdit.getText().toString());
					PActualise.setVIL(villeEdit.getText().toString());
					PActualise.setState(etatEdit.getText().toString());
					PActualise.setChez(roomNumberEdit.getText().toString());
					taskEcrase task = new taskEcrase(PActualise);
					task.execute();
					Toast.makeText(EditprofilMulot.this, "Profil actualis�", Toast.LENGTH_LONG).show();
				}
			});
		}
		
	}
	
	private class taskEcrase extends AsyncTask<Void,Void,String>{
		
		private PersonneMulot p;
		
		public taskEcrase( PersonneMulot p){
			this.p=p;
		}

		@Override
		protected String doInBackground(Void... params) {
			try {
				m.editProfil(this.p, strong);
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}
			return "";
		}
		protected void onPostExecute(String f){
			Toast.makeText(EditprofilMulot.this, "Profil actualis�", Toast.LENGTH_LONG).show();
			
		}
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		System.out.println("premier appel de onNavigationItemSelected");
		if(itemPosition==2){
			Toast.makeText(EditprofilMulot.this, "Prochainement...", Toast.LENGTH_LONG).show();
		}
		if(itemPosition==1){
			startActivity(new Intent(EditprofilMulot.this,SodexoConnexion.class));
		}
		if(itemPosition==0){
			startActivity(new Intent(EditprofilMulot.this,SearchActivity.class));
		}
		return true;
	}

}
