package gui;


import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import main.Mulot;
import main.PersonneMulot;
import main.Sodexo;
import fr.vitamines.annuaire.android.R;
import adapters.NavigationDropDownRowAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class SodexoConnexion extends Activity implements ActionBar.OnNavigationListener{

	private boolean allumage=true;

	private EditText edit;


	private Builder dialog;

	private EditText sodexoidentifiant;
	private EditText sodexopassword;
	private Button sodexovalider;
	private CheckBox sodexoRemember;
	private TextView sodexopresentation;

	private SharedPreferences preferences;

	public static Sodexo compteUtilisateur;

	private ActionBar bar;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sodexoconnexion);

		preferences = PreferenceManager.getDefaultSharedPreferences(this);

		bar=getActionBar();
		bar.setTitle("Sodexo");
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		SpinnerAdapter mSpinnerAdapter = new NavigationDropDownRowAdapter(this);
		bar.setListNavigationCallbacks(mSpinnerAdapter, this);
		bar.setSelectedNavigationItem(1);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);

		sodexoidentifiant=(EditText) findViewById(R.id.sodexoidentifiant);
		sodexopassword=(EditText) findViewById(R.id.sodexopassword);
		sodexovalider=(Button) findViewById(R.id.sodexovalider);
		sodexoRemember=(CheckBox) findViewById(R.id.sodexoremember);
		sodexopresentation=(TextView) findViewById(R.id.sodexopresentation);

		sodexopresentation.setText("Si vous utilisez le restaurant et/ou la"
				+ "\ncafeteria de l'�cole, vous pouvez consulter"
				+ "\nici le montant restant sur votre carte ainsi"
				+ "\n que vos tickets de caisse."
				+ "\n"
				+ "\nPour obtenir vos identifiants Sodexo,"
				+ "\ndemandez � votre passage en caisse ou "
				+ "\nconsultez la borne � l'entr�e du restaurant.");

		String regId = preferences.getString("sodexousername", null);
		String regMdp = preferences.getString("sodexomdp", null);




		sodexovalider.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {


				if(Connexion.isDeviceConnected(SodexoConnexion.this)){


					String identifiant = sodexoidentifiant.getText().toString();
					String password = sodexopassword.getText().toString();
					try {
						compteUtilisateur = new CreateSodexo().execute(identifiant,password).get();
					} catch (InterruptedException e) {
						System.out.println(e);
					} catch (ExecutionException e) {
						System.out.println("ExecutionException: "+e);
					} 
					if(sodexoRemember.isChecked()){
						preferences.edit().putString("sodexousername", identifiant).putString("sodexomdp", password ).commit();
					}
					if(compteUtilisateur!=null){
						if(compteUtilisateur.getMontantRestant()!=-3.14){
							Intent intent = new Intent(SodexoConnexion.this,SodexoActivity.class);
							startActivity(intent);
						}

						else{

							Builder dialo= new  AlertDialog.Builder(SodexoConnexion.this);
							dialo.setCancelable(true);
							dialo.setTitle("Identifiant ou mot de passe erron�!");
							dialo.show();
						}
					}

				}
				else{
					Builder notConnected= new AlertDialog.Builder(SodexoConnexion.this);
					notConnected.setCancelable(true);
					/*notConnected.setPositiveButton("OK", new DialogInterface.OnClickListener(){
						@Override
						public void onClick(DialogInterface dialog,
								int which) {
										
						}
					});*/
					LayoutInflater notConnectedLayoutInflater = LayoutInflater.from(SodexoConnexion.this);
					final View notconnectedDialog = notConnectedLayoutInflater.inflate(R.layout.device_not_connected_dialog, null);
					notConnected.setView(notconnectedDialog);
					notConnected.setTitle("Non connect�");
					notConnected.show();
				}
			}
		});
		if(regId!=null && regMdp!=null){
			sodexoidentifiant.setText(regId);
			sodexopassword.setText(regMdp);
			sodexoRemember.setChecked(true);
			sodexovalider.performClick();
		}
	}

	private class CreateSodexo extends AsyncTask<String,Void,Sodexo>{

		@Override
		protected Sodexo doInBackground(String... params) {
			Sodexo result=null;

			try {
				result= new Sodexo(params[0],params[1]);
			} catch (ClientProtocolException e) {
				System.out.println(e);
			} catch (IOException e) {
				System.out.println(e);
			}
			return result;
		}

	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(allumage){
			allumage=false;
			return true;
		}
		if(itemPosition==0){
			startActivity(new Intent(SodexoConnexion.this,SearchActivity.class));
			return true;
		}
		if(itemPosition==2){
			Toast.makeText(SodexoConnexion.this, "Prochainement...", Toast.LENGTH_LONG).show();
			return true;
		}
		/*if(itemPosition==1){
			startActivity(new Intent(SodexoConnexion.this,SodexoConnexion.class));
			return true;
		}*/
		if(itemPosition==3){
			Builder dialo= new  AlertDialog.Builder(SodexoConnexion.this);
			dialo.setCancelable(true);
			LayoutInflater factory = LayoutInflater.from(SodexoConnexion.this);
			final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
			dialo.setView(alertDialogView);
			TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
			demande.setText("Le mot de passe fort est n�cessaire pour �diter son profil: ");
			edit=(EditText) alertDialogView.findViewById(R.id.editpws);
			dialo.setTitle("Mot de passe fort");
			dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					String strongpassword=edit.getText().toString();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
					try {
						if(new GetProfil().execute(strongpassword).get().getCn()!="echec"){
							Intent intent = new Intent(SodexoConnexion.this,EditprofilMulot.class);
							Bundle extras = new Bundle();
							extras.putString("strongpassword", strongpassword);
							intent.putExtras(extras);
							startActivity(intent);
						}
						else{
							Builder erreur= new  AlertDialog.Builder(SodexoConnexion.this);
							erreur.setCancelable(true);
							erreur.setTitle("Mot de passe erron�!");
							erreur.show();
						}
					} catch (InterruptedException e) {
						System.out.println(e);
					} catch (ExecutionException e) {
						System.out.println(e);
					}
				}
			});
			dialo.setOnCancelListener(new OnCancelListener(){
				public void onCancel(DialogInterface dialog) {
					getActionBar().setSelectedNavigationItem(1);
				}

			});
			dialo.show();
			return true;
		}
		if(itemPosition==4){
			startActivity(new Intent(SodexoConnexion.this,A_propos.class));
			return true;
		}
		else{
			return false;
		}

	}

	private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			Mulot m = SearchActivity.m;

			try {
				if(m!=null){
					SearchActivity.profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(SodexoConnexion.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}
			return SearchActivity.profil;
		}
	}



}
