package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import main.Sodexo;
import fr.vitamines.annuaire.android.R;
import android.app.ActionBar;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.webkit.WebView;

public class AffichageTicket extends Activity {
	
	private Sodexo compteUtilisateur = SodexoConnexion.compteUtilisateur;
	private WebView affichageticket;
	
	private ActionBar bar;
	
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.affichageticket);
		
		bar=getActionBar();
		bar.setTitle("Ticket");
		
		affichageticket=(WebView) findViewById(R.id.affichageticket);
		
		Bundle args = getIntent().getExtras();
		String num = args.getString("id");
		System.out.println(num);
		String codehtml=null;
		try {
			codehtml=new ChargerTicket().execute(num).get();
		} catch (InterruptedException e) {
			System.out.println(e);
		} catch (ExecutionException e) {
			System.out.println(e);
		}
		affichageticket.loadDataWithBaseURL(null, codehtml, "text/html", "utf-8", null);
		
	}

	private class ChargerTicket extends AsyncTask<String,Void,String>{

		@Override
		protected String doInBackground(String... params) {
			String result=null;
			try {
				result= compteUtilisateur.getTicket(params[0]);
			} catch (ClientProtocolException e) {
				System.out.println(e);
			} catch (IOException e) {
				System.out.println(e);
			} catch (JSONException e) {
				System.out.println(e);
			}
			return result;
		}
		
	}
}
