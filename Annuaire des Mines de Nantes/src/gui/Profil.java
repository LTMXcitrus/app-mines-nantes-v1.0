package gui;

import fr.vitamines.annuaire.android.R;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import main.Mulot;
import main.PersonneMulot;

import org.apache.http.ParseException;
import org.json.JSONException;

import adapters.NavigationDropDownRowAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class Profil extends Activity implements ActionBar.OnNavigationListener{
	
	/*private boolean allumage = true;
	private EditText edit;*/
	
	
	PersonneMulot PAncien;
	public String nom;
	public static Mulot m=null;
	
	private String strong;
	
	private TextView nomProfil;

	private Bitmap bMap2;
	private ImageView photoProfil;
	
	private Button editerProfil;

	private String numberfixe="";
	private String numbermob="";
	private EditText fixeProfil;
	private EditText mobileProfil;
	

	
	private String mailint="";
	private String mailext="";
	private TextView mailintProfil;
	private TextView mailextProfil;

	private String ADR="";
	private TextView AdrProfil;

	private String ADR2="";
	private TextView Adr2Profil;

	private String ADR3="";
	private TextView Adr3Profil;

	private String CPO="";
	private TextView CpoProfil;

	private String promo="";
	private TextView promoProfil;

	private String ville="";
	private TextView villeProfil;

	private String etat="";
	private TextView etatProfil;

	private String Surnom="";
	private TextView SurnomProfil;

	private String roomNumber="";
	private TextView roomNumberProfil;

	public static String result;
	
	private String identifiant;
	private String weakpassword;
	
	
	public void onCreate(Bundle savedInstanceState)  {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.editprofilmulot);
		
		ActionBar bar = getActionBar();
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		SpinnerAdapter mSpinnerAdapter = new NavigationDropDownRowAdapter(this);
		bar.setListNavigationCallbacks(mSpinnerAdapter, this);
		bar.setSelectedNavigationItem(0);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);
		
		Bundle bundle=this.getIntent().getExtras();
		weakpassword=bundle.getString("weakpassword");
		identifiant=bundle.getString("identifiant");
		
		photoProfil = (ImageView) findViewById(R.id.photoprofil);
		nomProfil= (TextView) findViewById(R.id.nomprofil);
		editerProfil=(Button) findViewById(R.id.editerprofil);
		fixeProfil=(EditText)findViewById(R.id.fixeprofil);
		mobileProfil=(EditText)findViewById(R.id.mobileprofil);
		roomNumberProfil=(EditText) findViewById(R.id.roomNumberprofil);
		mailintProfil=(EditText) findViewById(R.id.mailintprofil);
		mailextProfil=(EditText) findViewById(R.id.mailextprofil);
		SurnomProfil=(EditText) findViewById(R.id.Surnomprofil);
		promoProfil=(EditText) findViewById(R.id.promoprofil);
		AdrProfil=(EditText) findViewById(R.id.adrprofil);
		Adr2Profil=(EditText) findViewById(R.id.Adr2profil);
		Adr3Profil=(EditText) findViewById(R.id.Adr3profil);
		CpoProfil=(EditText) findViewById(R.id.Cpoprofil);
		villeProfil=(EditText) findViewById(R.id.Villeprofil);
		etatProfil=(EditText) findViewById(R.id.etatprofil);
		InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(mailintProfil.getWindowToken(), 0);
		
		
		taskProfil tsk = new taskProfil();
		tsk.execute();

	}
	
	private class taskProfil extends AsyncTask<Void,Void,String>{
		
		public taskProfil(){
		}

		@Override
		protected String doInBackground(Void... params) {
			PersonneMulot PAncien= new PersonneMulot();
			try {
				try {
					m = new Mulot(identifiant,weakpassword);
				} catch (UnsupportedEncodingException e1) {
					System.out.println(e1);
				}
				
				try {
					if(m!=null){
						PAncien = m.getProfil(strong);
					}
					else{
						Toast.makeText(Profil.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
					}
				} catch (IOException e) {
					System.out.println(e);
				}
				catch(JSONException e){
					System.out.println(e);
				}
				
				if(PAncien.getCn()!=null){
					nom=PAncien.getCn();
				}
				if(PAncien.getA_nickname()!=null){
					Surnom=PAncien.getA_nickname();
					System.out.println(Surnom);
				}
				if(PAncien.getA_promo()!=null){
					promo =PAncien.getA_promo();
					System.out.println(promo);
				}
				if(PAncien.getMail()!=null){
					mailint = PAncien.getMail();				 
				}
				if(PAncien.getHomephone()!=null){
					numberfixe=PAncien.getHomephone();
				}
				if(PAncien.getMobile()!=null){
					numbermob=PAncien.getMobile();
				}
				if(PAncien.getA_mailexterne()!=null){
					mailext = PAncien.getA_mailexterne();
				}
				if(PAncien.getADR()!=null){
					ADR=PAncien.getADR();
				}
				if(PAncien.getADR2()!=null){
					ADR2=PAncien.getADR2();
				}
				if(PAncien.getADR3()!=null){
					ADR3=PAncien.getADR3();
				}
				if(PAncien.getCPO()!=null){
					CPO=PAncien.getCPO();
				}
				if(PAncien.getVIL()!=null){
					ville=PAncien.getVIL();
				}
				if(PAncien.getState()!=null){
					etat=PAncien.getState();
				}
				if (PAncien.getRoomNumber()!=null){
					roomNumber= PAncien.getRoomNumber();
				}
				bMap2= m.getPhoto(PAncien.getUid());
				System.out.println(bMap2==null);

				
			} catch (ParseException e) {
				System.out.println(e);;
			} catch (IOException e) {
				System.out.println(e);
			} catch (JSONException e) {
				System.out.println(e);
			}

			return "";
		
		}
		
		protected void onPostExecute(String f){
			
			
			SurnomProfil.setText(Surnom);
			promoProfil.setText(promo);
			mailintProfil.setText(mailint);
			mailextProfil.setText(mailext);
			fixeProfil.setText(numberfixe);
			mobileProfil.setText(numbermob);
			AdrProfil.setText(ADR);
			Adr2Profil.setText(ADR2);
			Adr3Profil.setText(ADR3);
			CpoProfil.setText(CPO);
			villeProfil.setText(ville);
			etatProfil.setText(etat);
			roomNumberProfil.setText(roomNumber);
			if(bMap2!=null){
				photoProfil.setImageBitmap(Bitmap.createScaledBitmap(bMap2, 180, 240, false));
			}
			
			nomProfil.setText(nom);
			nomProfil.setTextSize(20);
			InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
			imm.hideSoftInputFromWindow(mailintProfil.getWindowToken(), 0);
			
			editerProfil.setOnClickListener(new OnClickListener(){
				public void onClick(View v){
					
				}
			});
		}
		
	}

	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(itemPosition==2){
			Toast.makeText(Profil.this, "Prochainement...", Toast.LENGTH_LONG).show();
		}
		if(itemPosition==1){
			startActivity(new Intent(Profil.this,SodexoConnexion.class));
		}
		/*if(itemPosition==3){
			Builder dialo= new  AlertDialog.Builder(Profil.this);
			dialo.setCancelable(true);
			LayoutInflater factory = LayoutInflater.from(Profil.this);
			final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
			dialo.setView(alertDialogView);
			TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
			demande.setText("Le mot de passe fort est n�cessaire pour �diter son profil: ");
			edit=(EditText) alertDialogView.findViewById(R.id.editpws);
			dialo.setTitle("Mot de passe fort");
			dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					strongpassword=edit.getText().toString();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
					try {
						if(new GetProfil().execute(strongpassword).get().getCn()!="echec"){
							Intent intent = new Intent(SearchActivity.this,EditprofilMulot.class);
							Bundle extras = new Bundle();
							extras.putString("strongpassword", strongpassword);
							intent.putExtras(extras);
							startActivity(intent);
						}
						else{
							Builder erreur= new  AlertDialog.Builder(SearchActivity.this);
							erreur.setCancelable(true);
							erreur.setTitle("Mot de passe erron�!");
							erreur.show();
						}
					} catch (InterruptedException e) {
						System.out.println(e);
					} catch (ExecutionException e) {
						System.out.println(e);
					}
				}
			});
			dialo.show();
			return false;
		}*/
		return false;


	}
	/*private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			try {
				m = new Mulot(identifiant,weakpassword);
			} catch (UnsupportedEncodingException e1) {
				System.out.println("Editprofil ligne 141: "+e1);
			}

			try {
				if(m!=null){
					profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(Profil.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}

			return profil;

		}

	}*/

}
