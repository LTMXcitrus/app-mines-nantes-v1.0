package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import main.Mulot;
import main.PersonneMulot;
import main.Sodexo;
import fr.vitamines.annuaire.android.R;
import adapters.NavigationDropDownRowAdapter;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.DialogInterface.OnCancelListener;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

public class SodexoActivity extends Activity implements ActionBar.OnNavigationListener{
	
	private boolean allumage=true;
	
	private EditText edit;

	private TextView utilisateur;
	private TextView sodexosolde;
	private ToggleButton impressiontickets;

	private SharedPreferences preferences;

	public Sodexo compteUtilisateur;

	private ActionBar bar;


	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.sodexoactivity);

		bar=getActionBar();
		bar.setTitle("Sodexo");
		bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		SpinnerAdapter mSpinnerAdapter = new NavigationDropDownRowAdapter(this);
		bar.setListNavigationCallbacks(mSpinnerAdapter, this);
		bar.setSelectedNavigationItem(1);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);

		compteUtilisateur = SodexoConnexion.compteUtilisateur;

		if(compteUtilisateur!=null){
			utilisateur=(TextView) findViewById(R.id.utilisateur);
			sodexosolde=(TextView) findViewById(R.id.sodexosolde);
			impressiontickets=(ToggleButton) findViewById(R.id.impressiontickets);

			preferences=PreferenceManager.getDefaultSharedPreferences(this);

			if(preferences.contains("impressiontickets")){
				impressiontickets.setChecked(preferences.getBoolean("impressiontickets", true));
			}
			else{		
				try {
					impressiontickets.setChecked(new getPrintingState().execute().get());
				} catch (InterruptedException e) {
					System.out.println(e);
				} catch (ExecutionException e) {
					System.out.println(e);
				}
			}

			impressiontickets.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					new setPrintingState().execute(isChecked);
				}
			});


			utilisateur.setText(compteUtilisateur.getName());
			utilisateur.setTextColor(getResources().getColor(R.color.bleu));
			sodexosolde.setText(compteUtilisateur.getMontantRestant()+" �");
			sodexosolde.setTextSize(70);
			System.out.println(compteUtilisateur.getTickets().length);
			System.out.println(compteUtilisateur.getTickets()[0].length);
		}
		else{
			Intent intent = new Intent(SodexoActivity.this,Accueil.class);
			startActivity(intent);
		}
	}

	private class getPrintingState extends AsyncTask<Void,Void,Boolean>{

		@Override
		protected Boolean doInBackground(Void... params) {
			boolean result=true;
			try {
				System.out.println(compteUtilisateur==null);
				result = compteUtilisateur.isPrintingEnabled();
			} catch (ClientProtocolException e) {
				System.out.println(e);
			} catch (Exception e) {
				System.out.println(e);
			}
			return result;
		}
	}
	
	public boolean onCreateOptionsMenu(Menu menu){
		getMenuInflater().inflate(R.menu.sodexoactivity_menu, menu);
		return true;
	}
	
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if(id == R.id.go_to_ticket_list){
			startActivity(new Intent(SodexoActivity.this,SodexoListeTickets.class));
		}
		return true;
	}
	
	private class setPrintingState extends AsyncTask<Boolean,Void,Void>{

		@Override
		protected Void doInBackground(Boolean... params) {
			try {
				compteUtilisateur.setPrintingEnabled(params[0]);
			} catch (ClientProtocolException e) {
				System.out.println(e);
			} catch (IOException e) {
				System.out.println(e);
			}
			return null;
		}

	}
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(allumage){
			allumage=false;
			return true;
		}
		if(itemPosition==0){
			startActivity(new Intent(SodexoActivity.this,SearchActivity.class));
			return true;
		}
		if(itemPosition==2){
			Toast.makeText(SodexoActivity.this, "Prochainement...", Toast.LENGTH_LONG).show();
			return true;
		}
		/*if(itemPosition==1){
			startActivity(new Intent(SodexoConnexion.this,SodexoConnexion.class));
			return true;
		}*/
		if(itemPosition==3){
			Builder dialo= new  AlertDialog.Builder(SodexoActivity.this);
			dialo.setCancelable(true);
			LayoutInflater factory = LayoutInflater.from(SodexoActivity.this);
			final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
			dialo.setView(alertDialogView);
			TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
			demande.setText("Le mot de passe fort est n�cessaire pour �diter son profil: ");
			edit=(EditText) alertDialogView.findViewById(R.id.editpws);
			dialo.setTitle("Mot de passe fort");
			dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					String strongpassword=edit.getText().toString();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
					try {
						if(new GetProfil().execute(strongpassword).get().getCn()!="echec"){
							Intent intent = new Intent(SodexoActivity.this,EditprofilMulot.class);
							Bundle extras = new Bundle();
							extras.putString("strongpassword", strongpassword);
							intent.putExtras(extras);
							startActivity(intent);
						}
						else{
							Builder erreur= new  AlertDialog.Builder(SodexoActivity.this);
							erreur.setCancelable(true);
							erreur.setTitle("Mot de passe erron�!");
							erreur.show();
						}
					} catch (InterruptedException e) {
						System.out.println(e);
					} catch (ExecutionException e) {
						System.out.println(e);
					}
				}
			});
			dialo.setOnCancelListener(new OnCancelListener(){
				public void onCancel(DialogInterface dialog) {
					getActionBar().setSelectedNavigationItem(1);
				}
				
			});
			dialo.show();
			return true;
		}
		if(itemPosition==4){
			startActivity(new Intent(SodexoActivity.this,A_propos.class));
			return true;
		}
		else{
			return false;
		}
	}
	
	private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			Mulot m = SearchActivity.m;

			try {
				if(m!=null){
					SearchActivity.profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(SodexoActivity.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}
			return SearchActivity.profil;
		}
	}
}
