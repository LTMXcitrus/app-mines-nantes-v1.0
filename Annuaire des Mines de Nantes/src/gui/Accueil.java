package gui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

import main.Mulot;
import main.PersonneMulot;

import org.json.JSONException;

import fr.vitamines.annuaire.android.R;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.TextView;
import android.widget.Toast;

public class Accueil extends Activity {

	private EditText edit;
	private ActionBar bar;
	public static Mulot m=null;

	private SharedPreferences preferences;

	private String identifiant;
	private String weakpassword;
	private String strongpassword;

	public static PersonneMulot profil;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.accueil);

		preferences=PreferenceManager.getDefaultSharedPreferences(this);
		identifiant = preferences.getString("username", null);
		weakpassword=preferences.getString("mdp", null);

		bar= getActionBar();
		bar.setTitle("Application des Mines");

		GridView gridview = (GridView) findViewById(R.id.gridview);
		gridview.setAdapter(new ImageAdapter(this));
		gridview.setNumColumns(2);


		gridview.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
				if(position==0){
					Intent intent = new Intent(Accueil.this,SearchActivity.class);
					startActivity(intent);
				}
				if(position==1){
					Toast.makeText(Accueil.this, "Prochainement...", Toast.LENGTH_LONG).show();
				}
				if(position==2){
					Intent intent = new Intent(Accueil.this,SodexoConnexion.class);
					startActivity(intent);
				}
				if(position==3){
					Builder dialo= new  AlertDialog.Builder(Accueil.this);
					dialo.setCancelable(true);
					LayoutInflater factory = LayoutInflater.from(Accueil.this);
					final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
					dialo.setView(alertDialogView);
					TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
					demande.setText("Le mot de passe fort est n�cessaire pour �diter son profil: ");
					edit=(EditText) alertDialogView.findViewById(R.id.editpws);
					dialo.setTitle("Mot de passe fort");
					dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
						public void onClick(DialogInterface dialog, int which) {
							strongpassword=edit.getText().toString();
							InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
							try {
								if(new GetProfil().execute(strongpassword).get().getCn()!="echec"){
									Intent intent = new Intent(Accueil.this,EditprofilMulot.class);
									Bundle extras = new Bundle();
									extras.putString("strongpassword", strongpassword);
									intent.putExtras(extras);
									startActivity(intent);
								}
								else{
									Builder erreur= new  AlertDialog.Builder(Accueil.this);
									erreur.setCancelable(true);
									erreur.setTitle("Mot de passe erron�!");
									erreur.show();
								}
							} catch (InterruptedException e) {
								System.out.println(e);
							} catch (ExecutionException e) {
								System.out.println(e);
							}
						}
					});
					dialo.show();
				}
			}
		});
	}

	private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			try {
				m = new Mulot(identifiant,weakpassword);
			} catch (UnsupportedEncodingException e1) {
				System.out.println("Editprofil ligne 141: "+e1);
			}

			try {
				if(m!=null){
					profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(Accueil.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println("Editprofil ligne 152: "+e);
			}
			catch(JSONException e){
				System.out.println("Editprofil ligne 155: "+e);
			}

			return profil;

		}

	}
}
