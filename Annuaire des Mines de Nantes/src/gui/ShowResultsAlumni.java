package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.ParseException;
import org.json.JSONException;

import main.Mulot;
import main.PersonneAlumni;
import main.PersonneMulot;
import fr.vitamines.annuaire.android.R;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Intents;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;



public class ShowResultsAlumni extends Activity {
	
	private boolean allumage=true;

	private TextView nom;
	private String stringNom;

	private TextView identifiant;
	private String Ident=" ";

	private TextView mailecoleV;
	private TextView mailecoleRespV;
	private Button mailecole;
	private String mailec=" ";

	private TextView classe;
	private String clas=" ";

	private TextView option;
	private String opt=" ";

	private TextView infospersosTV;
	private LinearLayout infospersos;

	private TextView genderV;
	private TextView gender;
	private String gend=" ";

	private TextView nationaliteV;
	private TextView nationalite;
	private String nationalit=" ";

	private TextView titreV;
	private TextView Titre;
	private String tit=" ";

	private TextView nomusageV;
	private TextView nomusage;
	private String nomusa=" ";

	private TextView surnomV;
	private TextView surnom;
	private String surn=" ";

	private TextView situationproTV;
	private LinearLayout situationpro;

	private TextView situationV;
	private TextView situation;
	private String situa=" ";

	private TextView entrepriseV;
	private TextView entreprise;
	private String entre=" ";

	private TextView AdrEntreV;
	private TextView adrEntreprise;
	private String adrEntre=" ";

	private TextView VilEntreV;
	private TextView VilEntreprise;
	private String VilEntre=" ";

	private TextView CPOEntreV;
	private TextView CPEntreprise;
	private String CPEntre=" ";

	private TextView PaysEntreV;
	private TextView PaysEntreprise;
	private String PaysEntre=" ";

	private TextView serviceV;
	private TextView Service;
	private String serv=" ";

	private TextView posteEntreV;
	private TextView postEntreprise;
	private String postEntre=" ";

	private TextView coordonneesTV;
	private LinearLayout coordonnees;

	private TextView mailproV;
	private TextView mailproRespV;
	private Button envoyermailpro;
	private String mailproS=" ";

	private TextView mailpersoV;
	private TextView mailpersoRespV;
	private Button envoyermailperso;
	private String mailpers=" ";

	private TextView telproV;
	private TextView telproRespV;
	private Button telephonerpro;
	private String telproS=" ";

	private TextView telpersV;
	private TextView telpersRespV;
	private Button telephonerperso;
	private String telpers=" ";

	private TextView telmobV;
	private TextView telmobRespV;
	private Button telephonermobile;
	private String mob=" ";

	private TextView siteWebV;
	private Button siteweb;
	private String siteW=" ";

	private TextView adressepostaleV;
	private TextView Adresse;
	private Button navigationdomicile;
	private String adres=" ";

	int position;
	
	private EditText edit;
	
	private ActionBar bar; 
	



	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.resultsalumni);
		
		bar = getActionBar();
		bar.setTitle("Annuaire");
		//bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		//bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);
		
		nom=(TextView) findViewById(R.id.nom);

		identifiant=(TextView) findViewById(R.id.identifiantRespV);
		
		mailecoleV=(TextView) findViewById(R.id.mailecoleV);
		mailecoleRespV =(TextView) findViewById(R.id.mailecoleRespV);
		mailecole=(Button) findViewById(R.id.mailecole);
		
		
		classe=(TextView) findViewById(R.id.classeRespV);
		option=(TextView) findViewById(R.id.optionRespV);
		
		infospersosTV=(TextView) findViewById(R.id.infospersosTV);
		
		infospersos=(LinearLayout) findViewById(R.id.infospersos);
		genderV=(TextView) findViewById(R.id.genderV);
		gender=(TextView) findViewById(R.id.genderRespV);
		nationaliteV=(TextView) findViewById(R.id.nationaliteV);
		nationalite=(TextView) findViewById(R.id.nationaliteRespV);
		titreV=(TextView)findViewById(R.id.titreV);
		Titre=(TextView) findViewById(R.id.titreRespV);
		nomusageV=(TextView) findViewById(R.id.nomusageV);
		nomusage=(TextView) findViewById(R.id.nomusageRespV);
		surnomV=(TextView)findViewById(R.id.SurnomV);
		surnom=(TextView) findViewById(R.id.SurnomRespV);
		
		situationproTV=(TextView) findViewById(R.id.situationproV);
		situationpro=(LinearLayout) findViewById(R.id.situationpro);
		situationV=(TextView) findViewById(R.id.SituationV);
		situation=(TextView) findViewById(R.id.SituationRespV);
		entrepriseV=(TextView)findViewById(R.id.entrepriseV);
		entreprise=(TextView) findViewById(R.id.entrepriseRespV);
		AdrEntreV=(TextView) findViewById(R.id.AdrEntreV);
		adrEntreprise=(TextView) findViewById(R.id.AdrEntreRespV);
		VilEntreV=(TextView)findViewById(R.id.VilEntreV);
		VilEntreprise=(TextView) findViewById(R.id.VilEntreRespV);
		CPOEntreV=(TextView)findViewById(R.id.CPOEntreV);
		CPEntreprise=(TextView) findViewById(R.id.CPOEntreRespV);
		PaysEntreV=(TextView) findViewById(R.id.PaysEntreV);
		PaysEntreprise=(TextView) findViewById(R.id.PaysEntreRespV);
		serviceV=(TextView)findViewById(R.id.serviceV);
		Service=(TextView) findViewById(R.id.serviceRespV);
		posteEntreV=(TextView)findViewById(R.id.posteEntreV);
		postEntreprise=(TextView) findViewById(R.id.posteEntreRespV);
		
		coordonneesTV=(TextView) findViewById(R.id.coordonneesTV);
		coordonnees=(LinearLayout)findViewById(R.id.coordonnees);
		
		mailproV=(TextView)findViewById(R.id.mailproV);
		mailproRespV=(TextView) findViewById(R.id.mailproRespV);
		envoyermailpro=(Button) findViewById(R.id.envoyermailpro);
		
		mailpersoV=(TextView)findViewById(R.id.mailpersoV);
		mailpersoRespV=(TextView) findViewById(R.id.mailpersoRespV);
		envoyermailperso=(Button) findViewById(R.id.envoyermailperso);
		
		telproV=(TextView)findViewById(R.id.telproV);
		telproRespV=(TextView) findViewById(R.id.telproRespV);
		telephonerpro=(Button) findViewById(R.id.telephonerpro);
		
		telpersV=(TextView) findViewById(R.id.telpersV);
		telpersRespV=(TextView) findViewById(R.id.telpersRespV);
		telephonerperso=(Button) findViewById(R.id.telephonerperso);
		
		telmobV=(TextView) findViewById(R.id.telmobV);
		telephonermobile=(Button) findViewById(R.id.telephonermobile);
		telmobRespV=(TextView) findViewById(R.id.telmobRespV);
		
		siteWebV=(TextView) findViewById(R.id.siteWebV);
		siteweb=(Button) findViewById(R.id.sitewebRespV);
		
		adressepostaleV=(TextView) findViewById(R.id.adressepostaleV);
		Adresse=(TextView) findViewById(R.id.adressepostaleRespV);
		navigationdomicile=(Button) findViewById(R.id.navigationdomicile);

		nationaliteV =(TextView) findViewById(R.id.nationaliteV);

		Bundle bundle  = this.getIntent().getExtras();
		position = bundle.getInt("position");

		Taskalumni tsk=new Taskalumni();
		tsk.execute();
	}

	private class Taskalumni extends AsyncTask<Void,Void,String>{
		

		

		@Override
		protected String doInBackground(Void... params) {
			PersonneAlumni pers =new PersonneAlumni();
			try {
				pers =	SearchActivity.a.getPersonne(SearchActivity.pa[position].getUid());
				System.out.println(pers.getA_login());
				stringNom=pers.getCn();
				if(pers.getA_login()!=null ){
					Ident=pers.getA_login();
				}
				if(pers.getMail()!=null ){
					mailec=pers.getMail();
				}
				if(pers.getA_classe()!=null ){
					clas=pers.getA_classe();
				}
				if(pers.getA_option()!=null ){
					opt=pers.getA_option();
				}
				if(pers.getA_gender()!=null ){
					gend=pers.getA_gender();
				}
				if(pers.getA_nationality()!=null ){
					nationalit=pers.getA_nationality();
				}
				if(pers.getPersonalTitle()!=null ){
					tit=pers.getPersonalTitle();
				}
				if(pers.getGivenName()!=null ){
					nomusa=pers.getGivenName();
				}
				if(pers.getA_nickname()!=null ){
					surn=pers.getA_nickname();
				}
				if(pers.getA_situation()!=null ){
					situa=pers.getA_situation();
				}
				if(pers.getO()!=null ){
					entre=pers.getO();
				}
				if(pers.getA_oaddress()!=null ){
					adrEntre=pers.getA_oaddress();
				}
				if(pers.getPostalAddress()!=null ){
					VilEntre=pers.getPostalAddress();
				}
				if(pers.getPostalCode()!=null ){
					CPEntre=pers.getPostalCode();
				}
				if(pers.getCo()!=null ){
					PaysEntre=pers.getCo();
				}
				if(pers.getOu()!=null ){
					serv=pers.getOu();
				}
				if(pers.getOrganizationalStatus()!=null ){
					postEntre=pers.getOrganizationalStatus();
				}
				if(pers.getMailpro()!=null ){
					mailproS=pers.getMailpro();
				}
				if(pers.getMailperso()!=null ){
					mailpers=pers.getMailperso();
				}
				if(pers.getTelephoneNumber()!=null ){
					telproS=pers.getTelephoneNumber();
				}
				if(pers.getHomephone()!=null){
					telpers=pers.getHomephone();
				}
				System.out.println("pers.getMobile()= "+pers.getMobile());
				if(pers.getMobile()!=null){
					System.out.println("mob= "+mob);
					mob=pers.getMobile();
					System.out.println("mob= "+mob);
				}
				if(pers.getA_homepage()!=null ){
					siteW=pers.getA_homepage();
				}
				if(pers.getHpaMonobloc()!=null ){
					System.out.println("je suis rentr� dans HpaMonobloc");
					adres=pers.getADR()+"\n"+pers.getADR2()+"\n"+pers.getADR3()+"\n"+ pers.getVIL();
				}
			} catch (ParseException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (JSONException e) {
				e.printStackTrace();
			}
			return "";
		}

		protected void onPostExecute(String f){
			//nom.setText(stringNom);
			bar.setTitle(stringNom);
			identifiant.setText(Ident);
			if(Ident==null){
				identifiant.setVisibility(View.GONE);
			}
			
			if(mailec==null || !mailec.contains("@")){
				mailecole.setVisibility(View.GONE);
				mailecoleRespV.setVisibility(View.GONE);
				mailecoleV.setVisibility(View.GONE);
			}
			else{
				int arobase = mailec.indexOf("@");
				mailecoleRespV.setText(mailec.substring(0,arobase)+"\n"+mailec.substring(arobase));
			}
			classe.setText(clas);
			if(clas==null){
				classe.setVisibility(View.GONE);
			}
			
			if(opt==null){
				option.setVisibility(View.GONE);
			}
			else{
				option.setText(opt);
			}
			
			if(gend=="null" || gend==null){
				gender.setVisibility(View.GONE);
				genderV.setVisibility(View.GONE);
			}
			else{
				gender.setText(gend);
			}
			
			if(nationalit=="null"){
				nationalite.setVisibility(View.GONE);
				nationaliteV.setVisibility(View.GONE);
			}
			else{
				nationalite.setText(nationalit);
			}
			
			if(tit==null || tit=="null"){
				Titre.setVisibility(View.GONE);
				titreV.setVisibility(View.GONE);
			}
			else{
				Titre.setText(tit);
			}
			
			if(nomusa==null || nomusa=="null"){
				nomusage.setVisibility(View.GONE);
				nomusageV.setVisibility(View.GONE);
			}
			else{
				nomusage.setText(nomusa);
			}
			
			if(surn==null || surn=="null"){
				surnom.setVisibility(View.GONE);
				surnomV.setVisibility(View.GONE);
			}
			else{
				surnom.setText(surn);
			}
			
			if(situa==null || situa=="null"){
				situation.setVisibility(View.GONE);
				situationV.setVisibility(View.GONE);
			}
			else{
				situation.setText(situa);
			}
			
			if(entre==null || entre=="null"){
				entreprise.setVisibility(View.GONE);
				entrepriseV.setVisibility(View.GONE);
			}
			else{
				entreprise.setText(entre);
			}
			
			if(adrEntre==null || adrEntre=="null"){
				adrEntreprise.setVisibility(View.GONE);
				AdrEntreV.setVisibility(View.GONE);
			}
			else{
				adrEntreprise.setText(adrEntre);
			}
			
			if(VilEntre==null || VilEntre=="null"){
				VilEntreprise.setVisibility(View.GONE);
				VilEntreV.setVisibility(View.GONE);
			}
			else{
				VilEntreprise.setText(VilEntre);
			}
			
			if(CPEntre==null || CPEntre=="null"){
				CPEntreprise.setVisibility(View.GONE);
				CPOEntreV.setVisibility(View.GONE);
			}
			else{
				CPEntreprise.setText(CPEntre);
			}
			
			if(PaysEntre==null || PaysEntre=="null"){
				PaysEntreprise.setVisibility(View.GONE);
				PaysEntreV.setVisibility(View.GONE);
			}
			else{
				PaysEntreprise.setText(PaysEntre);
			}
			
			if(serv==null || serv=="null"){
				Service.setVisibility(View.GONE);
				serviceV.setVisibility(View.GONE);
			}
			else{
				Service.setText(serv);
			}
			
			if(postEntre==null || postEntre=="null"){
				postEntreprise.setVisibility(View.GONE);
				posteEntreV.setVisibility(View.GONE);
			}
			else{
				postEntreprise.setText(postEntre);
			}
			
			if(mailproS==null || mailproS=="null" || !mailproS.contains("@")){
				mailproRespV.setVisibility(View.GONE);
				mailproV.setVisibility(View.GONE);
				envoyermailpro.setVisibility(View.GONE);
			}
			else{
				int arobase = mailproS.indexOf("@");
				mailproRespV.setText(mailproS.substring(0, arobase)+"\n"+mailproS.substring(arobase));
			}
			
			if(mailpers==null || mailpers=="null" || !mailproS.contains("@")){
				mailpersoRespV.setVisibility(View.GONE);
				mailpersoV.setVisibility(View.GONE);
				envoyermailperso.setVisibility(View.GONE);
			}
			else{
				int arobase = mailpers.indexOf("@");
				mailpersoRespV.setText(mailpers.substring(0,arobase)+"\n"+mailpers.substring(arobase));
			}
			telproRespV.setText(telproS);
			if(telproS==null || telproS=="null"){
				telproRespV.setVisibility(View.GONE);
				telproV.setVisibility(View.GONE);
				telephonerpro.setVisibility(View.GONE);
			}
			telpersRespV.setText(telpers);
			if(telpers==null || telpers=="null"){
				telpersRespV.setVisibility(View.GONE);
				telpersV.setVisibility(View.GONE);
				telephonerperso.setVisibility(View.GONE);
			}
			telmobRespV.setText(mob);
			if(mob==null || mob=="null"){
				telmobRespV.setVisibility(View.GONE);
				telmobV.setVisibility(View.GONE);
				telephonermobile.setVisibility(View.GONE);
			}
			siteweb.setText(siteW);
			if(siteW==null || siteW=="null"){
				siteweb.setVisibility(View.GONE);
				siteWebV.setVisibility(View.GONE);
			}
			Adresse.setText(adres);
			if(adres==null || adres=="null" || adres=="" || adres==" "){
				Adresse.setVisibility(View.GONE);
				adressepostaleV.setVisibility(View.GONE);
				navigationdomicile.setVisibility(View.GONE);
			}

			if(mob!=null && mob!="null"){
				telephonermobile.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent appel = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+mob));
						startActivity(appel);
					}
				});
			}
			if(telproS!=null && telproS!="null"){
				telephonerpro.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent appel = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+telproS));
						startActivity(appel);
					}
				});
			}if(telpers!=null && telpers!="null"){
				telephonerperso.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent appel = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+telpers));
						startActivity(appel);
					}
				});
			}
			if(mailec!=null && mailec!="null"){
				mailecole.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent intent = new Intent(Intent.ACTION_SENDTO);
						intent.setData(Uri.parse("mailto:"+mailec)); 
						intent.putExtra(Intent.EXTRA_EMAIL, mailec);
						if (intent.resolveActivity(getPackageManager()) != null) {
							startActivity(intent);
						}
					}
				});
			}
			if(mailproS!=null && mailproS!="null"){
				envoyermailpro.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent intent = new Intent(Intent.ACTION_SENDTO);
						intent.setData(Uri.parse("mailto:"+mailproS)); // only email apps should handle this
						intent.putExtra(Intent.EXTRA_EMAIL, mailproS);
						if (intent.resolveActivity(getPackageManager()) != null) {
							startActivity(intent);
						}
					}
				});
			}
			if(mailpers!=null && mailpers!="null"){
				envoyermailperso.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent intent = new Intent(Intent.ACTION_SENDTO);
						intent.setData(Uri.parse("mailto:"+mailpers)); // only email apps should handle this
						intent.putExtra(Intent.EXTRA_EMAIL, mailpers);
						if (intent.resolveActivity(getPackageManager()) != null) {
							startActivity(intent);
						}
					}
				});
			}
			if(siteW!=null && siteW!="null"){
				siteweb.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent i = new Intent(Intent.ACTION_VIEW);
						i.setData(Uri.parse("http://"+siteW));
						startActivity(i);
					}
				});
			}
			if(adres!=null && adres!=""){
				navigationdomicile.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
							Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("geo:0,0?q="+adres));
							startActivity(intent);
					}
				});
			}

		}

	}
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.showresultsalumni, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.add_user_alumni) {
			Intent addContact= new Intent(Intent.ACTION_INSERT);
			addContact.setType(Contacts.CONTENT_TYPE);
			addContact.putExtra(Intents.Insert.NAME, stringNom);
			if(mob!=null){
				addContact.putExtra(Intents.Insert.PHONE, mob);
			}
			if(mailpers!=null){
				addContact.putExtra(Intents.Insert.EMAIL, mailpers);
			}
			if(mailec!=null){
				addContact.putExtra(Intents.Insert.SECONDARY_EMAIL, mailec);
			}
			if(telpers!=null){
				addContact.putExtra(Intents.Insert.SECONDARY_PHONE, telpers);
			}
			if(adres!=null){
				addContact.putExtra(Intents.Insert.POSTAL, adres);
			}

			startActivity(addContact);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onDestroy(){
		super.onDestroy();
	}

	
	
	
	private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			Mulot m = SearchActivity.m;

			try {
				if(m!=null){
					SearchActivity.profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(ShowResultsAlumni.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}
			return SearchActivity.profil;
		}
	}

}
