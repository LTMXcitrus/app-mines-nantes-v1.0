package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import main.Mulot;
import main.PersonneMulot;

import org.apache.http.ParseException;
import org.json.JSONException;

import fr.vitamines.annuaire.android.R;
import android.app.AlertDialog.Builder;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract.Contacts;
import android.provider.ContactsContract.Intents;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class ShowResultsMulot extends Activity implements ActionBar.OnNavigationListener{
	
	private boolean allumage=true;

	private Dialog dialoattente;
	private EditText edit;
	private String weakpassword="wpw";
	private String identifiant="id";
	
	private boolean bureau;

	private TextView nomV;
	private String nom;

	private Bitmap bMap2;
	private ImageView photo;
	private String numberfixe="";
	private String numbermob="";
	private TextView fixeV;
	private TextView fixeRespV;
	private Button fixeRespB;
	private TextView mobileRespV;
	private Button mobileRespB;
	private Button mobileSendSms;
	private TextView mobileV;



	private String mailint="";
	private String mailext="";
	private TextView mailintRespV;
	private Button envoyermailint;
	private TextView mailintV;
	private TextView mailextRespV;
	private TextView mailextV;
	private Button envoyermailext;


	private String ADR="";
	private TextView AdrRespV;
	private TextView AdrV;

	private String ADR2="";
	private String ADR3="";
	private String CPO="";
	private String promo="";
	private TextView promoRespV;
	private TextView promoV;

	private String ville="";
	private String etat="";
	private String Surnom="";
	private TextView SurnomRespV;
	private TextView SurnomV;

	private String roomNumber="";
	private TextView roomNumberRespV;
	private TextView roomNumberV;

	public static String result;
	private int position=-1;

	private String adresse = "";
	private Button carte;




	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

		Bundle bundle=this.getIntent().getExtras();
		position = bundle.getInt("position");
		identifiant=bundle.getString("identifiant");
		weakpassword=bundle.getString("weakpassword");

		ActionBar bar = getActionBar();
		bar.setTitle("Annuaire");
		//bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		//SpinnerAdapter mSpinnerAdapter = new NavigationDropDownRowAdapter(this);
		//bar.setListNavigationCallbacks(mSpinnerAdapter, this);
		//bar.setSelectedNavigationItem(0);
		//bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
		//bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);


		if(position!=-1 & identifiant!="id" & weakpassword!="wpw" & SearchActivity.pm!=null){

			setContentView(R.layout.showresults);


			photo = (ImageView) findViewById(R.id.photo);
			nomV= (TextView) findViewById(R.id.nom);

			fixeRespV=(TextView)findViewById(R.id.fixeRespV);
			fixeRespB=(Button) findViewById(R.id.fixeRespB);
			fixeV=(TextView)findViewById(R.id.fixeV);



			mobileRespV=(TextView)findViewById(R.id.mobileRespV);
			mobileRespB=(Button) findViewById(R.id.mobileRespB);
			mobileSendSms=(Button) findViewById(R.id.envoyersms);
			mobileV =(TextView) findViewById(R.id.mobileV);

			roomNumberRespV=(TextView) findViewById(R.id.roomNumberRespV);
			roomNumberV=(TextView) findViewById(R.id.roomNumberV);

			mailintRespV=(TextView) findViewById(R.id.mailintRespV);
			mailintV=(TextView) findViewById(R.id.mailintV);
			envoyermailint=(Button) findViewById(R.id.envoyermailint);

			mailextRespV=(TextView) findViewById(R.id.mailextRespV);
			mailextV=(TextView) findViewById(R.id.mailextV);
			envoyermailext=(Button) findViewById(R.id.envoyermailext);

			SurnomRespV=(TextView) findViewById(R.id.SurnomRespV);
			SurnomV=(TextView) findViewById(R.id.SurnomV);

			promoRespV=(TextView) findViewById(R.id.promoRespV);
			promoV=(TextView) findViewById(R.id.promoV);

			AdrRespV=(TextView) findViewById(R.id.adrRespV);
			AdrV=(TextView) findViewById(R.id.AdrV);

			carte=(Button) findViewById(R.id.carte);






			nom = SearchActivity.pm[position].getCn();

			bar.setTitle(nom);
			nomV.setText(nom);
			nomV.setTextSize(20);
			nomV.setVisibility(View.GONE);
			//System.out.println(result);

			Taskback task = new Taskback(this);
			task.execute();

			//System.out.println(result);





		}
		else{
			Intent intent = new Intent(ShowResultsMulot.this,SearchActivity.class);
			Bundle extras = new Bundle();
			extras.putString("weakpassword", weakpassword);
			extras.putString("identifiant", identifiant);
			intent.putExtras(extras);
			startActivity(intent);

		}
	}

	private class Taskback extends AsyncTask<Void,Void,String>{
		private Context context;

		private Taskback(Context context){
			this.context=context;
		}

		protected void onPreExecute() {
			super.onPreExecute();
			dialoattente=new Dialog(ShowResultsMulot.this);
			LayoutInflater factory = LayoutInflater.from(ShowResultsMulot.this);
			final View dialochargement = factory.inflate(R.layout.chargement, null);
			dialoattente.setContentView(dialochargement);
			dialoattente.setTitle("Chargement...");
			dialoattente.show();

			Toast.makeText(context, "Chargement...", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected String doInBackground(Void... params) {
			PersonneMulot pers= new PersonneMulot();
			try {
				pers = SearchActivity.m.getPersonne(SearchActivity.pm[position].getUid());
				if(pers.getA_nickname()!=null){
					Surnom=pers.getA_nickname();
					System.out.println(Surnom);
				}
				if(pers.getA_promo()!=null){
					promo =pers.getA_promo();
					System.out.println(promo);
				}
				if(pers.getMail()!=null){
					mailint = pers.getMail();				 
				}
				if(pers.getHomephone()!=null){
					numberfixe=pers.getHomephone();
				}
				if(pers.getMobile()!=null){
					numbermob=pers.getMobile();
				}
				if(pers.getA_mailexterne()!=null){
					mailext = pers.getA_mailexterne();
				}
				if(pers.getADR()!=null){
					ADR=pers.getADR();
					System.out.println("ADR= "+ADR);
				}
				if(pers.getADR2()!=null){
					ADR2=pers.getADR2();
					System.out.println("ADR2= "+ADR2);
				}
				if(pers.getADR3()!=null){
					ADR3=pers.getADR3();
					System.out.println("ADR3= "+ADR3);
				}
				if(pers.getCPO()!=null){
					CPO=pers.getCPO();
				}
				if(pers.getVIL()!=null){
					ville=pers.getVIL();
				}
				if(pers.getState()!=null){
					etat=pers.getState();
				}
				if (pers.getChez()!=null){
					bureau=false;
					roomNumber= pers.getChez();
				}
				else{
					bureau=true;
					if(pers.getRoomNumber()!=null){
						roomNumber=pers.getRoomNumber();
					}
					
				}
				bMap2= SearchActivity.m.getPhoto(SearchActivity.pm[position].getUid());
				System.out.println(bMap2==null);
				adresse = ADR2+"\n"+CPO+"\n"+ville+"\n";
				System.out.println("adresse= "+adresse);


			} catch (ParseException e) {
				System.out.println(e);;
			} catch (IOException e) {
				System.out.println(e);
			} catch (JSONException e) {
				System.out.println(e);
			}


			return result;
		}
		protected void onPostExecute(String f){
			dialoattente.dismiss();
			
			if(bureau){
				roomNumberV.setText("Bureau");
			}
			else{
				roomNumberV.setText("Appartement");
			}
			SurnomRespV.setText(Surnom);
			System.out.println("Surnom==\"\" -> "+(Surnom==""));
			if(Surnom.length()==0){
				SurnomRespV.setVisibility(View.GONE);
				SurnomV.setVisibility(View.GONE);
			}
			promoRespV.setText(promo);
			System.out.println("promo= "+promo);
			if(promo.length()==0){
				promoV.setVisibility(View.GONE);
				promoRespV.setVisibility(View.GONE);
			}
			int arobase = mailint.indexOf("@");
			if(mailint.length()!=0){
				mailintRespV.setText(mailint.substring(0, arobase)+"\n"+mailint.substring(arobase));
			}
			if(mailint.length()==0){
				mailintV.setVisibility(View.GONE);
				mailintRespV.setVisibility(View.GONE);
				envoyermailint.setVisibility(View.GONE);
			}
			int arobase2 = mailext.indexOf("@");
			if(mailext.length()!=0){
				mailextRespV.setText(mailext.substring(0,arobase2)+"\n"+mailext.substring(arobase2));
			}
			if(mailext.length()==0){
				mailextV.setVisibility(View.GONE);
				mailextRespV.setVisibility(View.GONE);
				envoyermailext.setVisibility(View.GONE);
			}
			fixeRespV.setText(numberfixe);
			if(numberfixe.length()==0){
				fixeV.setVisibility(View.GONE);
				fixeRespV.setVisibility(View.GONE);
				fixeRespB.setVisibility(View.GONE);
			}
			mobileRespV.setText(numbermob);
			if(numbermob.length()==0){
				mobileV.setVisibility(View.GONE);
				mobileRespV.setVisibility(View.GONE);
				mobileRespB.setVisibility(View.GONE);
				mobileSendSms.setVisibility(View.GONE);
			}
			AdrRespV.setText(adresse+etat);

			if(ADR.length()==0){

				AdrV.setVisibility(View.GONE);
				AdrRespV.setVisibility(View.GONE);
				carte.setVisibility(View.GONE);
			}
			roomNumberRespV.setText(roomNumber);
			if(roomNumber.length()==0){
				roomNumberV.setVisibility(View.GONE);
				roomNumberRespV.setVisibility(View.GONE);
			}
			if(bMap2!=null){
				photo.setImageBitmap(Bitmap.createScaledBitmap(bMap2, 180, 240, false));
			}
			if(numbermob!=null){
				mobileRespB.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent appel = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+numbermob));
						startActivity(appel);
					}
				});
				mobileSendSms.setOnClickListener(new OnClickListener(){
					public void onClick(View v){
						Intent sms = new Intent(Intent.ACTION_VIEW);
						sms.setData(Uri.parse("sms:"+numbermob));
						startActivity(sms);
					}
				});
			}
			if(numberfixe!=null){
				fixeRespB.setOnClickListener(new OnClickListener(){

					public void onClick(View view){
						Intent appel = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:"+numberfixe));
						startActivity(appel);
					}
				});
			}
			if(mailint!=null){
				envoyermailint.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Builder maili=new AlertDialog.Builder(ShowResultsMulot.this);						
						maili.setCancelable(true);					
						maili.setPositiveButton("Envoyer mail",new DialogInterface.OnClickListener(){
							public void onClick(DialogInterface dialog, int which){
								Intent intent = new Intent(Intent.ACTION_SENDTO);
								intent.setData(Uri.parse("mailto:"+mailint)); // only email apps should handle this
								intent.putExtra(Intent.EXTRA_EMAIL, mailint);
								if (intent.resolveActivity(getPackageManager()) != null) {
									startActivity(intent);
								}
							}
						}
								);
						maili.setTitle("Mail interne");
						maili.setMessage(mailint);
						maili.show();

					}
				});
			}
			if(mailext!=null){
				envoyermailext.setOnClickListener(new OnClickListener(){
					public void onClick(View view){
						Intent intent = new Intent(Intent.ACTION_SENDTO);
						intent.setData(Uri.parse("mailto:"+mailext)); // only email apps should handle this
						intent.putExtra(Intent.EXTRA_EMAIL, mailext);
						if (intent.resolveActivity(getPackageManager()) != null) {
							startActivity(intent);
						}
					}
				});
			}
			if(adresse!=null){
				carte.setOnClickListener(new OnClickListener(){
					public void onClick(View v){
						Intent intent = new Intent(Intent.ACTION_VIEW,Uri.parse("geo:0,0?q="+adresse));
						startActivity(intent);
					}
				});

			}

		}

	}
	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.showresultsmulot, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.adduser) {
			Intent addContact= new Intent(Intent.ACTION_INSERT);
			addContact.setType(Contacts.CONTENT_TYPE);
			addContact.putExtra(Intents.Insert.NAME, nom);
			if(numbermob!=null){
				addContact.putExtra(Intents.Insert.PHONE, numbermob);
			}
			if(mailext!=null){
				addContact.putExtra(Intents.Insert.EMAIL, mailext);
			}
			if(mailint!=null){
				addContact.putExtra(Intents.Insert.SECONDARY_EMAIL, mailint);
			}
			if(numberfixe!=null){
				addContact.putExtra(Intents.Insert.SECONDARY_PHONE, numberfixe);
			}
			if(ADR!=null){
				addContact.putExtra(Intents.Insert.POSTAL, ADR+"\n"+ADR2+"\n"+ADR3+"\n"+CPO+"\n"+ville);
			}

			startActivity(addContact);
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	public void onDestroy(){
		super.onDestroy();
	}
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(allumage){
			allumage=false;
			return true;
		}
		if(itemPosition==0){
			startActivity(new Intent(ShowResultsMulot.this,SearchActivity.class));
			return true;
		}
		if(itemPosition==2){
			Toast.makeText(ShowResultsMulot.this, "Prochainement...", Toast.LENGTH_LONG).show();
			return true;
		}
		if(itemPosition==1){
			startActivity(new Intent(ShowResultsMulot.this,SodexoConnexion.class));
			return true;
		}
		if(itemPosition==3){
			Builder dialo= new  AlertDialog.Builder(ShowResultsMulot.this);
			dialo.setCancelable(true);
			LayoutInflater factory = LayoutInflater.from(ShowResultsMulot.this);
			final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
			dialo.setView(alertDialogView);
			TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
			demande.setText("Le mot de passe fort est n�cessaire pour �diter son profil: ");
			edit=(EditText) alertDialogView.findViewById(R.id.editpws);
			dialo.setTitle("Mot de passe fort");
			dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
				public void onClick(DialogInterface dialog, int which) {
					String strongpassword=edit.getText().toString();
					InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
					imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
					try {
						if(new GetProfil().execute(strongpassword).get().getCn()!="echec"){
							Intent intent = new Intent(ShowResultsMulot.this,EditprofilMulot.class);
							Bundle extras = new Bundle();
							extras.putString("strongpassword", strongpassword);
							intent.putExtras(extras);
							startActivity(intent);
						}
						else{
							Builder erreur= new  AlertDialog.Builder(ShowResultsMulot.this);
							erreur.setCancelable(true);
							erreur.setTitle("Mot de passe erron�!");
							erreur.show();
						}
					} catch (InterruptedException e) {
						System.out.println(e);
					} catch (ExecutionException e) {
						System.out.println(e);
					}
				}
			});
			dialo.show();
			return true;
		}
		else{
			return false;
		}
	}
	
	
	private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			Mulot m = SearchActivity.m;

			try {
				if(m!=null){
					SearchActivity.profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(ShowResultsMulot.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}
			return SearchActivity.profil;
		}
	}
}
