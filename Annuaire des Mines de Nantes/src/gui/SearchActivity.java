package gui;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

import main.Alumni;
import main.Mulot;
import main.PersonneAlumni;
import main.PersonneMulot;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import adapters.NavigationDropDownRowAdapter;
import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.text.Html;
import android.text.SpannableString;
import android.text.style.ForegroundColorSpan;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.TextView.OnEditorActionListener;
import fr.vitamines.annuaire.android.R;

public class SearchActivity extends ListActivity implements ActionBar.OnNavigationListener{

	//d�claration des instances qui seront utilis�es pour �tre transmises aux futures activit�s (visibilit� = public)
	public static String query;
	private Dialog dialo;
	public static Mulot m;
	public static Alumni a;

	//public static Dialog sodexoattente;

	private String[] fieldValuesAlumni ={"CN","a-option","a-classe","a-gender","o","VIL","postalAdress"};
	private String[] fieldValuesMulot ={"cn", "a-promo","mobile","telephoneNumber"};

	private String field = fieldValuesMulot[0];
	private String ALUMNI = "alumni";
	private String MULOT="mulot";
	private String CURRENT_SEARCH_AREA=MULOT;

	private boolean avancee = false;

	public static PersonneMulot[] pm;
	public static PersonneAlumni[] pa;

	public static PersonneMulot profil;

	private static String identifiant;
	private static String weakpassword;
	private static String strongpassword=null;
	private EditText edit;
	private EditText recherche;
	private Button clicktosearch;
	private Spinner searchTypeSpinner;


	private RadioButton mulot;
	private SharedPreferences preferences;

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//on relie l'activit� � son layout
		setContentView(R.layout.searchactivity);

		preferences=PreferenceManager.getDefaultSharedPreferences(this);

		this.getIntent().getExtras();
		identifiant=preferences.getString("username", null);
		weakpassword=preferences.getString("mdp", null);
		
		(new CreateAlumni()).execute();

		if(identifiant!=null && weakpassword!=null){
			if(Connexion.isDeviceConnected(SearchActivity.this)){

				ActionBar bar=getActionBar();
				bar.setTitle(Html.fromHtml("<font color=\"red\">" + "Hello" + "</font>"));
				bar.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
				SpinnerAdapter mSpinnerAdapter = new NavigationDropDownRowAdapter(this);
				bar.setListNavigationCallbacks(mSpinnerAdapter, this);
				bar.setSelectedNavigationItem(0);
				bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_TITLE);
				bar.setDisplayOptions(0, ActionBar.DISPLAY_SHOW_HOME);

				mulot=(RadioButton) findViewById(R.id.mulot);
				mulot.setChecked(true);

				clicktosearch = (Button) findViewById(R.id.clicktosearch);
				recherche=(EditText) findViewById(R.id.rechercheMulot);



				searchTypeSpinner = (Spinner) findViewById(R.id.searchtypespinner);
				ArrayAdapter<String>  arrayadapter =new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.searchtypeMulot));
				arrayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				searchTypeSpinner.setAdapter(arrayadapter);
				searchTypeSpinner.setVisibility(View.GONE);

				searchTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

					@Override
					public void onItemSelected(AdapterView<?> parent, View view,
							int position, long id) {
						field=fieldValuesMulot[position];
					}

					@Override
					public void onNothingSelected(AdapterView<?> parent) {		}

				});			

				clicktosearch.setOnClickListener(new OnClickListener() {
					public void onClick(View v) {
						String champDeRecherche = recherche.getText().toString();
						if(Connexion.isDeviceConnected(SearchActivity.this)){
							if(champDeRecherche.length()>1){
								if(CURRENT_SEARCH_AREA.equals(MULOT)){
									BackTaskMulot bcktsk = new BackTaskMulot();
									PersonneMulot[] tabpm=null;
									System.out.println("champ de recherche: "+champDeRecherche);
									try {
										tabpm = bcktsk.execute(champDeRecherche).get();
									} catch (InterruptedException e) {
										System.out.println(e);
									} catch (ExecutionException e) {
										System.out.println(e);
									}
									String[] pms;
									if(tabpm!=null && tabpm.length!=0){
										pms = new String[tabpm.length];
										for(int i=0;i<tabpm.length;i++){
											pms[i]=tabpm[i].getCn();
										}
										setListAdapter(new ArrayAdapter < String > (SearchActivity.this,android.R.layout.simple_list_item_1, pms));
										getListView().setTextFilterEnabled(true);
									}
									else{
										Builder erreur= new  AlertDialog.Builder(SearchActivity.this);
										erreur.setCancelable(true);
										LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
										final View erreurDialog = factory.inflate(R.layout.dialoguerreur, null);
										erreur.setView(erreurDialog);
										erreur.setTitle("Aucun r�sultat !");
										erreur.show();
									}

								}
								else{
									BackTaskAlumni bcktsk = new BackTaskAlumni();
									PersonneAlumni[] tabpa =null;
									try {
										tabpa= bcktsk.execute(champDeRecherche).get();
									} catch (InterruptedException e) {
										System.out.println(e);
									} catch (ExecutionException e) {
										System.out.println(e);
									}
									if(tabpa!=null && tabpa.length!=0){
										String[] pas  = new String[tabpa.length];
										for(int i=0;i<tabpa.length;i++){
											pas[i]=tabpa[i].getCn();
										}
										setListAdapter(new ArrayAdapter < String > (SearchActivity.this,android.R.layout.simple_list_item_1, pas));
										getListView().setTextFilterEnabled(true);
									}
									else{
										Builder erreur= new  AlertDialog.Builder(SearchActivity.this);
										erreur.setCancelable(true);
										LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
										final View erreurDialog = factory.inflate(R.layout.dialoguerreur, null);
										erreur.setView(erreurDialog);
										erreur.setTitle("Aucun r�sultat !");
										erreur.show();
									}

								}
							}
							else{
								Builder erreur= new  AlertDialog.Builder(SearchActivity.this);
								erreur.setCancelable(true);
								LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
								final View erreurDialog = factory.inflate(R.layout.dialoguerreur, null);
								erreur.setView(erreurDialog);
								erreur.setTitle("Le nom recherch� est trop court");
								erreur.show();
							}

						}
						else{
							Builder notConnected= new AlertDialog.Builder(SearchActivity.this);
							notConnected.setCancelable(true);
							notConnected.setPositiveButton("OK", new DialogInterface.OnClickListener(){
								@Override
								public void onClick(DialogInterface dialog,
										int which) {
									recreate();				
								}
							});
							LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
							final View notconnectedDialog = factory.inflate(R.layout.device_not_connected_dialog, null);
							notConnected.setView(notconnectedDialog);
							notConnected.setTitle("Non connect�");
							notConnected.show();
						}

					}

				});

				//on relie les widgets � ceux d�clar�s dans le layout

				recherche.setOnEditorActionListener(new OnEditorActionListener() {
					public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {

						if (actionId == EditorInfo.IME_ACTION_SEARCH) {
							clicktosearch.performClick();
							InputMethodManager imm= (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
							imm.hideSoftInputFromWindow(recherche.getWindowToken(), 0);
							return true;
						}
						return false;
					}
				});
			}
			else{
				Builder notConnected= new AlertDialog.Builder(SearchActivity.this);
				notConnected.setCancelable(true);
				notConnected.setPositiveButton("OK", new DialogInterface.OnClickListener(){
					@Override
					public void onClick(DialogInterface dialog,
							int which) {
						recreate();				
					}
				});
				LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
				final View notconnectedDialog = factory.inflate(R.layout.device_not_connected_dialog, null);
				notConnected.setView(notconnectedDialog);
				notConnected.setTitle("Non connect�");
				notConnected.show();

			}
		}
		else{
			startActivity(new Intent(SearchActivity.this,Connexion.class));
		}

	}


	public void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		if(mulot.isChecked()){
			//On lance l'activit� affichant les r�sultats
			Intent intent = new Intent(SearchActivity.this,ShowResultsMulot.class);
			/**On a besoin de savoir quel est le num�ro de l'item cliqu� dans la liste, pour savoir quelles informations
			 * aller chercher dans le PersonneMulot[] "pm". On utilise donc la classe Bundle, qui permet de faire passer des
			 * informations d'une activit� � une autre
			 */
			//On indique qu'on veut faire passer des "extras"
			Bundle extras = new Bundle();
			//On ajoute dans l'extra, un entier, qu'on appelle "position", et qui a la valeur position
			extras.putInt("position", position);
			extras.putString("weakpassword", weakpassword);
			extras.putString("identifiant", identifiant);
			//on rajoute l'extra dans l'intent
			intent.putExtras(extras);
			//On lance l'activit�
			startActivity(intent);
		}
		else{
			Intent intent  = new Intent(SearchActivity.this,ShowResultsAlumni.class);
			Bundle extras = new Bundle();
			extras.putInt("position", position);
			extras.putString("weakpassword", weakpassword);
			extras.putString("identifiant", identifiant);
			intent.putExtras(extras);
			startActivity(intent);
		}
	}

	public void onRadButtonSearchClick(View v){
		if(v.getId()==R.id.alumni){
			if(a==null){
				Builder dialo= new  AlertDialog.Builder(SearchActivity.this);
				dialo.setCancelable(true);
				dialo.setOnCancelListener(new OnCancelListener() {
					public void onCancel(DialogInterface dialog) {
						if(strongpassword==null){
							mulot.setChecked(true);
						}
					}
				});

				LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
				final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
				dialo.setView(alertDialogView);
				TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
				demande.setText("Le mot de passe fort est n�cessaire pour acc�der � l'annuaire des Anciens: ");
				dialo.setTitle("Mot de passe fort");
				dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which) {
						EditText edit=(EditText) alertDialogView.findViewById(R.id.editpws);
						strongpassword=edit.getText().toString();
						try {
							if(new CreateAlumni().execute().get()){
								if(a.getKey()=="echec"){
									mulot.setChecked(true);
									strongpassword=null;
									Builder erreur= new  AlertDialog.Builder(SearchActivity.this);
									erreur.setCancelable(true);
									erreur.setTitle("Mot de passe erron�!");
									erreur.show();
								}
							}
						} catch (InterruptedException e) {
							System.out.println(e);
						} catch (ExecutionException e) {
							System.out.println(e);
						}
					}
				});
				dialo.show();
			}
			CURRENT_SEARCH_AREA=ALUMNI;
			field=fieldValuesAlumni[0];

			ArrayAdapter<String>  arrayadapter =new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.searchtypeAlumni));
			arrayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			searchTypeSpinner.setAdapter(arrayadapter);
			searchTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					field=fieldValuesAlumni[position];
				}
				@Override
				public void onNothingSelected(AdapterView<?> parent) {		}	
			});

		}
		if(v.getId()==R.id.mulot){
			CURRENT_SEARCH_AREA=MULOT;
			field=fieldValuesMulot[0];

			ArrayAdapter<String>  arrayadapter =new ArrayAdapter<String>(SearchActivity.this, android.R.layout.simple_spinner_item, getResources().getStringArray(R.array.searchtypeMulot));
			arrayadapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			searchTypeSpinner.setAdapter(arrayadapter);
			searchTypeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
				@Override
				public void onItemSelected(AdapterView<?> parent, View view,
						int position, long id) {
					field=fieldValuesMulot[position];
				}
				@Override
				public void onNothingSelected(AdapterView<?> parent) {		}
			});
		}
	}

	public boolean onCreateOptionsMenu(Menu menu) {

		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.searchactivity, menu);
		return true;
	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if(id==R.id.deconnection){
			Builder deconnection = new AlertDialog.Builder(SearchActivity.this);
			deconnection.setCancelable(true);
			deconnection.setMessage("Voulez-vous vraiment vous d�connecter ?");
			deconnection.setNegativeButton("Non",new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					dialog.dismiss();
				}

			});
			deconnection.setPositiveButton("Oui",new DialogInterface.OnClickListener(){
				@Override
				public void onClick(DialogInterface dialog, int which) {
					preferences=PreferenceManager.getDefaultSharedPreferences(SearchActivity.this);
					preferences.edit().remove("username").remove("mdp").commit();
					startActivity(new Intent(SearchActivity.this,Connexion.class));
				}

			});
			deconnection.show();
		}
		if(id==R.id.rechercheavancee){
			if(avancee){
				item.setIcon(getResources().getDrawable(R.drawable.plus_bleu));
				searchTypeSpinner.setVisibility(View.GONE);
				if(CURRENT_SEARCH_AREA.equals(MULOT)){
					field=fieldValuesMulot[0];
				}
				if(CURRENT_SEARCH_AREA.equals(ALUMNI)){
					field=fieldValuesAlumni[0];
				}
				avancee=false;
			}
			else{
				item.setIcon(getResources().getDrawable(R.drawable.minus_bleu));
				searchTypeSpinner.setVisibility(View.VISIBLE);
				avancee=true;
			}
		}

		return super.onOptionsItemSelected(item);
	}

	public void onDestroy(){
		super.onDestroy();
	}


	private class BackTaskMulot extends AsyncTask<String, Integer, PersonneMulot[]>{

		protected void onPreExecute() {
			super.onPreExecute();
			dialo=new Dialog(SearchActivity.this);
			LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
			final View dialochargement = factory.inflate(R.layout.chargement, null);
			dialo.setContentView(dialochargement);
			dialo.setTitle("Chargement...");
			dialo.show();
		}

		protected PersonneMulot[] doInBackground(String... arg0) {

			try {
				m = new Mulot(identifiant,weakpassword);
			} catch (UnsupportedEncodingException e1) {
				System.out.println(e1);
			}
			PersonneMulot[] tabpm=null;
			try {
				tabpm = m.search(arg0[0],field,"contains","people");
				pm=tabpm;

			} catch (IOException e) {
				System.out.println(e);
			} catch (JSONException e) {
				System.out.println(e);
			}

			return pm;
		}

		protected void onPostExecute(PersonneMulot[] result) {
			dialo.dismiss();

			if(pm==null){
				Builder erreur= new  AlertDialog.Builder(SearchActivity.this);
				erreur.setCancelable(true);
				LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
				final View erreurDialog = factory.inflate(R.layout.dialoguerreur, null);
				erreur.setView(erreurDialog);
				erreur.setTitle("Aucun r�sultat");
				erreur.show();
			}


		}
	}

	private class BackTaskAlumni extends AsyncTask<String,Void,PersonneAlumni[]>{

		protected void onPreExecute() {
			super.onPreExecute();
			dialo=new Dialog(SearchActivity.this);
			LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
			final View dialochargement = factory.inflate(R.layout.chargement, null);
			dialo.setContentView(dialochargement);
			dialo.setTitle("Chargement...");
			dialo.show();
		}

		@Override
		protected PersonneAlumni[] doInBackground(String... params) {



			PersonneAlumni[] p = null;
			try {
				p = a.search(params[0],field,"SNGN");
				pa=p;
			} catch (IOException e) {
				System.out.println(e);
			} catch (JSONException e) {
				System.out.println(e);
			}

			return p;
		}
		protected void onPostExecute(PersonneAlumni[] result){
			dialo.dismiss();
		}
	}
	private class CreateAlumni extends AsyncTask<Void,Void,Boolean>{

		protected void onPreExecute() {
			super.onPreExecute();
			dialo=new Dialog(SearchActivity.this);
			LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
			final View dialochargement = factory.inflate(R.layout.chargement, null);
			dialo.setContentView(dialochargement);
			dialo.setTitle("Chargement...");
			dialo.show();
		}

		@Override
		protected Boolean doInBackground(Void... params) {
			try {
				a = new Alumni("mlemon13","LTMX45");
			} catch (ClientProtocolException e1) {
				System.out.println(e1);
			} catch (IOException e1) {
				System.out.println(e1);
			} catch (JSONException e1) {
				System.out.println(e1);
			}
			return true;
		}

		protected void onPostExecute(Boolean result){
			dialo.dismiss();
		}

	}
	@Override
	public boolean onNavigationItemSelected(int itemPosition, long itemId) {
		if(itemPosition==2){
			Toast.makeText(SearchActivity.this, "Prochainement...", Toast.LENGTH_LONG).show();
		}
		if(itemPosition==1){
			startActivity(new Intent(SearchActivity.this,SodexoConnexion.class));
		}
		if(itemPosition==3){
			if(SearchActivity.strongpassword==null){
				Builder dialo= new  AlertDialog.Builder(SearchActivity.this);
				dialo.setCancelable(true);
				LayoutInflater factory = LayoutInflater.from(SearchActivity.this);
				final View alertDialogView = factory.inflate(R.layout.alertdialopws, null);
				dialo.setView(alertDialogView);
				TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
				demande.setText("Le mot de passe fort est n�cessaire pour �diter son profil: ");
				edit=(EditText) alertDialogView.findViewById(R.id.editpws);
				dialo.setTitle("Mot de passe fort");
				dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
					public void onClick(DialogInterface dialog, int which) {
						strongpassword=edit.getText().toString();
						InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
						imm.hideSoftInputFromWindow(edit.getWindowToken(), 0);
						try {
							if(new GetProfil().execute(strongpassword).get().getCn()!="echec"){
								Intent intent = new Intent(SearchActivity.this,EditprofilMulot.class);
								Bundle extras = new Bundle();
								extras.putString("strongpassword", strongpassword);
								intent.putExtras(extras);
								startActivity(intent);
								System.out.println("EditprofilMulot est lanc�e");
							}
							else{
								Builder erreur= new  AlertDialog.Builder(SearchActivity.this);
								erreur.setCancelable(true);
								erreur.setTitle("Mot de passe erron�!");
								erreur.show();
								strongpassword=null;
							}
						} catch (InterruptedException e) {
							System.out.println(e);
						} catch (ExecutionException e) {
							System.out.println(e);
						}
					}
				});
				dialo.setOnCancelListener(new OnCancelListener(){
					public void onCancel(DialogInterface dialog) {
						getActionBar().setSelectedNavigationItem(0);
						strongpassword=null;
					}

				});
				dialo.show();
				return true;
			}
			else{
				Intent intent = new Intent(SearchActivity.this,EditprofilMulot.class);
				Bundle extras = new Bundle();
				extras.putString("strongpassword", strongpassword);
				intent.putExtras(extras);
				startActivity(intent);
				System.out.println("EditprofilMulot est lanc�e");
			}
		}
		if(itemPosition==4){
			startActivity(new Intent(SearchActivity.this,A_propos.class));
		}
		return true;


	}
	private class GetProfil extends AsyncTask<String,Void,PersonneMulot>{
		protected PersonneMulot doInBackground(String... params) {
			try {
				m = new Mulot(identifiant,weakpassword);
			} catch (UnsupportedEncodingException e1) {
				System.out.println(e1);
			}

			try {
				if(m!=null){
					profil = m.getProfil(params[0]);
				}
				else{
					Toast.makeText(SearchActivity.this, "Probl�me de connexion!", Toast.LENGTH_LONG).show();
				}
			} catch (IOException e) {
				System.out.println(e);
			}
			catch(JSONException e){
				System.out.println(e);
			}

			return profil;

		}

	}
}
