package gui;

import fr.vitamines.annuaire.android.R;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

public class ImageAdapter extends BaseAdapter {
	private Context mContext;

	public ImageAdapter(Context c) {
		mContext = c;
	}

	public int getCount() {
		return mThumbIds.length;
	}

	public Object getItem(int position) {
		return null;
	}

	public long getItemId(int position) {
		return 0;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		ImageView imageView;
		TextView imageTitle;

		View row;

		LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
		row = inflater.inflate(R.layout.gridview_custom_row, parent, false);
		imageView = (ImageView) row.findViewById(R.id.image_accueil);
		imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
		imageView.setPadding(8, 8, 8, 8);
		imageTitle = (TextView) row.findViewById(R.id.text_accueil_gridview);
		if(position==0){
			imageTitle.setText("Annuaire");
		}
		if(position==1){
			imageTitle.setText("Oasis");
		}
		if(position==2){
			imageTitle.setText("Sodexo");
		}
		if(position==3){
			imageTitle.setText("Profil");
		}
		imageTitle.setTextColor(Color.parseColor("#FFFFFF"));
		imageView.setImageResource(mThumbIds[position]);
		return row;
	}

	private Integer[] mThumbIds = {
			R.drawable.find_user, R.drawable.oasis,
			R.drawable.sodexo, R.drawable.profile,

	};
}