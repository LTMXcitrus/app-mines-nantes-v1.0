package main;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class Alumni {

	private String key ;
	
	/** field = {"nom", "surnom", "promotion", "option", "entreprise", "CP entreprise", "filtre LDAP"} */
	public final static String[] field = {"nom", "surnom", "promotion", "option", "entreprise", "CP entreprise", "filtre LDAP"} ;
	/** sortby = {"nom", "option/promotion", "promotion/option", "année"} */
	public final static String[] sortby = {"nom", "option/promotion", "promotion/option", "année"} ;
			
	/** fieldTypes = {"CN", "NICK", "PROMO", "OPTION", "ENTREPRISE", "CPENTREPRISE", "LDAP"} */
	public final static String[] fieldTypes = {"CN", "NICK", "PROMO", "OPTION", "ENTREPRISE", "CPENTREPRISE", "LDAP"} ;
	/** sortbyTypes = {"SNGN", "OPTCLASS", "CLASSOPT", "YEAR"} */
	public final static String[] sortbyTypes = {"SNGN", "OPTCLASS", "CLASSOPT", "YEAR"} ;
			
	/**
	 * Constructeur qui initialise la variable d'instance key grâce à une connexion aux serveurs
	 * avec identifiants pour paramètres.
	 * @param user est le login
	 * @param password est le mot de passe fort
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public Alumni(String user, String password) throws ClientProtocolException, IOException, JSONException {
		String content = "" ;
		String url = "https://www.emn.fr/z-dre/sia/WS/login.php";

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		
		List<NameValuePair> liste = new ArrayList<NameValuePair>(2);
		liste.add(new BasicNameValuePair("login", user));
		liste.add(new BasicNameValuePair("password", password));

		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));

		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}
		if (response.getStatusLine().getStatusCode() == 200) {

			JSONObject form = new JSONObject(content);

			if (form.getBoolean("yes")) {

				JSONObject answer = form.getJSONObject("answer");
				this.setKey(answer.getString("skey"));

			}
			else {
				System.out.println("Échec de la connexion : " + form.getString("message"));
				this.key="echec";
			}
		}
		else {
			System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
		}
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return this.key;
	}

	/**
	 * @param key the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * Méthode qui retourne l'ensemble des résultats d'une recherche avec pour paramètres value, le corps de la recherche.
	 * Les résultats sont sous forme d'un tableau de PersonneAlumni.
	 * @param value est le corps de la recherche
	 * @return un tableau de PersonneAlumni initialisés avec seulement leur nom et leur uid.
	 * @throws JSONException 
	 * @throws IOException 
	 */
	public PersonneAlumni[] search(String value) throws IOException, JSONException {
		return this.search(value, "CN", "SNGN") ;
	}
	
	/**
	 * Méthode qui retourne l'ensemble des résultats d'une recherche avec pour paramètres value, field, sortby.
	 * Les résultats sont sous forme d'un tableau de PersonneAlumni.
	 * @param value est le corps de la recherche (nom de la personne, par exemple)
	 * @param field est le critère de recherche (nom, prénom, entreprise, etc...)
	 * @param sortby est le critère de classement
	 * @return un tableau de PersonneAlumni initialisés avec seulement leur nom et leur uid.
	 * @throws IOException
	 * @throws JSONException
	 */
	public PersonneAlumni[] search(String value, String field, String sortby) throws IOException, JSONException {
		String content = "" ;
		String url = "https://www.emn.fr/z-dre/sia/WS/asearch.php";

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);

		List<NameValuePair> liste = new ArrayList<NameValuePair>(3);
		//liste.add(new BasicNameValuePair("value", value));
		liste.add(new BasicNameValuePair("select", "`"+field+"`"+" like "+"'%"+value+"%'"));
		liste.add(new BasicNameValuePair("attributes", "cn,uid"));
		liste.add(new BasicNameValuePair("sortby", sortby));

		post.addHeader("Cookie", "WSBAC=" + this.getKey());

		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));

		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}
		
		if (response.getStatusLine().getStatusCode() == 200) {

			JSONObject form = new JSONObject(content);

			if (form.getBoolean("yes")) {

				JSONArray answer = form.getJSONArray("answer") ;

				int n = answer.length() ;
				PersonneAlumni[] personnes = new PersonneAlumni[n] ;

				for (int i = 0 ; i < n ; i++) {
					JSONObject personne = answer.getJSONObject(i) ;

					personnes[i] = new PersonneAlumni() ;
					personnes[i].setCn(personne.getString("cn")) ;

					String s = personne.getString("dn") ;
					s = s.substring(s.indexOf("=") + 1) ;
					s = s.substring(0, s.indexOf(",")) ;
					personnes[i].setUid(Integer.parseInt(s));
					System.out.println(personnes[i].getCn() + " " + personnes[i].getUid());
				}
				return personnes ;
			}
			else {
				System.out.println("Échec de la recherche : " + form.getString("message"));
				return null ;
			}
		}
		else {
			System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
			return null ;
		}
	}

	/**
	 * Méthode qui retourne le profil d'une personne (avec null pour les champs non remplis).
	 * @param uid est l'uid de la personne dont on veut obtenir le profil
	 * @return le profil, sous forme d'un objet Personne
	 * @throws ParseException
	 * @throws IOException
	 * @throws JSONException
	 */
	public PersonneAlumni getPersonne(int uid) throws ParseException, IOException, JSONException {

		String content = "" ;
		String url = "https://www.emn.fr/z-dre/sia/WS/getdata.php";
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> liste = new ArrayList<NameValuePair>(1);
		liste.add(new BasicNameValuePair("dn", "uid=" + uid +",a-section=phantom,dc=emn,dc=fr"));

		post.addHeader("Cookie", "WSBAC=" + this.getKey());
		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));
		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}
		JSONObject form = new JSONObject(content);
		JSONObject answer = form.getJSONObject("answer");

		PersonneAlumni p = new PersonneAlumni(uid) ;

		if (answer.optString("cn") != null) {p.setCn(answer.optString("cn"));}
		if (answer.optString("sn") != null) {p.setSn(answer.optString("sn"));}
		if (answer.optString("givenName") != null) {p.setGivenName(answer.optString("givenName"));}
		if (answer.optString("homePhone") != null) {p.setHomephone(answer.optString("homePhone"));}
		if (answer.optString("mobile") != null) {p.setMobile(answer.optString("mobile"));}
		if (answer.optString("mail") != null) {p.setMail(answer.optString("mail"));}
		if (answer.optString("chez") != null) {p.setChez(answer.optString("chez"));}
		if (answer.optString("ADR") != null) {p.setADR(answer.optString("ADR"));}
		if (answer.optString("ADR2") != null) {p.setADR2(answer.optString("ADR2"));}
		if (answer.optString("ADR3") != null) {p.setADR3(answer.optString("ADR3"));}
		if (answer.optString("CPO") != null) {p.setCPO(answer.optString("CPO"));}
		if (answer.optString("VIL") != null) {p.setVIL(answer.optString("VIL"));}
		if (answer.optString("state") != null) {p.setState(answer.optString("state"));}
		if (answer.optString("PAY") != null) {p.setPAY(answer.optString("PAY"));}
		if (answer.optString("personalTitle") != null) {p.setPersonalTitle(answer.optString("personalTitle"));}
		if (answer.optString("o") != null) {p.setO(answer.optString("o"));}
		if (answer.optString("ou") != null) {p.setOu(answer.optString("ou"));}
		if (answer.optString("organizationalStatus") != null) {p.setOrganizationalStatus(answer.optString("organizationalStatus"));}
		if (answer.optString("telephoneNumber") != null) {p.setTelephoneNumber(answer.optString("telephoneNumber"));}
		if (answer.optString("a-login") != null) {p.setA_login(answer.optString("a-login"));}
		if (answer.optString("a-homepage") != null) {p.setA_homepage(answer.optString("a-homepage"));}
		if (answer.optString("a-nickname") != null) {p.setA_nickname(answer.optString("a-nickname"));}
		if (answer.optString("a-gender") != null) {p.setA_gender(answer.optString("a-gender"));}
		if (answer.optString("postalAddress") != null) {p.setPostalAddress(answer.optString("postalAddress"));}
		if (answer.optString("postalCode") != null) {p.setPostalCode(answer.optString("postalCode"));}
		if (answer.optString("a-oaddress") != null) {p.setA_oaddress(answer.optString("a-oaddress"));}
		if (answer.optString("co") != null) {p.setCo(answer.optString("co"));}
		if (answer.optString("a-classe") != null) {p.setA_classe(answer.optString("a-classe"));}
		if (answer.optString("a-option") != null) {p.setA_option(answer.optString("a-option"));}
		if (answer.optString("a-nationality") != null) {p.setA_nationality(answer.optString("a-nationality"));}
		if (answer.optString("a-situation") != null) {p.setA_situation(answer.optString("a-situation"));}
		if (answer.optString("mailperso") != null) {p.setMailperso(answer.optString("mailperso"));}
		if (answer.optString("mailpro") != null) {p.setMailpro(answer.optString("mailpro"));}	

		System.out.println("Profil de " + p.getCn() + " (" + p.getUid() + ") récupéré.");

		return p ;

	}

	public static void main(String[] args) throws ClientProtocolException, IOException, JSONException {
		Alumni a = new Alumni("pmesur13", "tostaky5") ;
		a.search("florent", "CN", "SNGN") ;
		System.out.println(a.getPersonne(3362).getEverything()) ;
	}
}
