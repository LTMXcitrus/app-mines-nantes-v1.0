package main;

import java.io.IOException;
import java.io.InputStream ;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;

import org.apache.http.client.methods.HttpGet ;


import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity ;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.graphics.Bitmap ;
import android.graphics.BitmapFactory ;
import android.util.Base64 ;

public class Mulot {
	private String key ;

	/** matchfield = {"nom", "surnom", "nom de famille", "service", "téléphone", "promotion", "position", "select!"} */
	public final static String[] matchfield = {"nom", "surnom", "nom de famille", "service", "téléphone", "promotion", "position", "select!"} ;
	/** matchmode = {"contient", "commence par", "est égal à", "prédicat LDAP"} */
	public final static String[] matchmode = {"contient", "commence par", "est égal à", "prédicat LDAP"} ;
	/** scope = {"tout le monde", "le personnel", "les étudiants", "les contacts", "tout"} */
	public final static String[] scope = {"tout le monde", "le personnel", "les étudiants", "les contacts", "tout"} ;

	/** matchfieldTypes = {"cn", "a-nickname", "sn", "ou", "telephoneNumber", "a-promo", "employeeType", "a-select"} */
	public final static String[] matchfieldTypes = {"cn", "a-nickname", "sn", "ou", "telephoneNumber", "a-promo", "employeeType", "a-select"} ;
	/** matchmodeTypes = {"contains", "startswith", "exactmatch", "predicate"} */
	public final static String[] matchmodeTypes = {"contains", "startswith", "exactmatch", "predicate"} ;
	/** scopeTypes = {"people", "employee", "students", "contacts", "whole"} */
	public final static String[] scopeTypes = {"people", "employee", "students", "contacts", "whole"} ;

	/**
	 * Constructeur qui initialise la clé de connexion "key" à partir des login et mot de passe de l'utilisateur
	 * @param user est le login
	 * @param password est le mot de passe
	 * @throws UnsupportedEncodingException 
	 */
	public Mulot(String user, String password) throws UnsupportedEncodingException {
		String orig = user + ":" + password ;
		this.key = new String(Base64.encodeToString(orig.getBytes("UTF-8"), Base64.NO_WRAP));
	}

	/**
	 * Constructeur vide qui crée une clé vide.
	 */
	public Mulot() {
		this.key = "";
	}

	/**
	 * Accesseur de la clé en lecture
	 * @return key, la clé
	 */
	public String getKey() {
		return this.key ;
	}

	/**
	 * Accesseur de la clé en écriture
	 * @param key est la clé de connexion
	 */
	public void setKey(String key) {
		this.key = key ;
	}

	/**
	 * Accesseur du login en lecture
	 * @return le login de la personne
	 */
	public String getUser() {
		String s = new String(Base64.decode(this.getKey().getBytes(), Base64.NO_WRAP));
		s = s.substring(0, s.indexOf(":")) ;
		return s ;
	}

	/**
	 * Accesseur du mot de passe en lecture
	 * @return le mot de passe de la personne
	 */
	public String getPassword() {
		String s = new String(Base64.decode(this.getKey().getBytes(), Base64.NO_WRAP));
		s = s.substring(s.indexOf(":") + 1) ;
		return s ;
	}

	/**
	 * Méthode qui retourne un profil incomplet de l'utilisateur contenant son nom (cn) et son uid.
	 * @return un profil incomplet de l'utilisateur.
	 * @throws IOException
	 * @throws JSONException
	 */
	public PersonneMulot getLightProfil() throws IOException, JSONException {
		String content = "" ;
		String url = "https://nsa.emn.fr/z-sic/aie/WS/basic-search.php";
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> liste = new ArrayList<NameValuePair>(6);
		liste.add(new BasicNameValuePair("searchmode", "basic"));
		liste.add(new BasicNameValuePair("reqattrs", "cn,uid"));
		liste.add(new BasicNameValuePair("criteria", this.getUser()));
		liste.add(new BasicNameValuePair("matchfield", "a-login"));
		liste.add(new BasicNameValuePair("matchmode", "contains"));
		liste.add(new BasicNameValuePair("scope", "people"));
		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));

		post.addHeader("Authorization", "Basic " + this.getKey());

		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}

		if (response.getStatusLine().getStatusCode() == 200) {

			JSONObject form = new JSONObject(content);

			if (form.getBoolean("yes")) {

				JSONObject answer = form.getJSONObject("answer");
				int count = answer.getInt("count");

				if (count == 0) {
					System.out.println("Aucun résultat");
					return null ;
				}

				PersonneMulot profil ;

				JSONObject entries = answer.getJSONObject("entries") ;
				Iterator<?> keys = entries.keys() ;

				while (keys.hasNext()) {
					String key = (String)keys.next();
					JSONObject personne = entries.getJSONObject(key) ;
					profil = new PersonneMulot() ;
					profil.setCn(personne.getJSONArray("cn").getString(0)) ;
					profil.setUid(Integer.parseInt(personne.getJSONArray("uid").getString(0)));
					System.out.println("Connecté en tant que " + profil.getCn() + ".");
				}
			}
			else {
				System.out.println("Échec de la récupération du profil : " + form.getString("message"));
				return null ;
			}
		}
		else {
			System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
			return null ;
		}
		return null ;
	}



	
	public boolean connect(String key) throws ClientProtocolException, IOException{
		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet("https://nsa.emn.fr/z-sic/aie/public/search.php");
		get.addHeader("Authorization", "Basic " + key);
		
		HttpResponse response = client.execute(get);
		return response.getStatusLine().getStatusCode()==200;
	 
	}


	/**
	 * Méthode qui effectue une recherche simple ("le nom contient query, parmi les personnes de l'école")
	 * @param query est le texte de la recherche
	 * @return un tableau de personnes, dont seuls le nom (cn) et l'uid sont initialisés.
	 * @throws IOException 
	 * @throws JSONException 
	 */
	public PersonneMulot[] search(String query) throws IOException, JSONException {

		return this.search(query, "cn", "contains", "people") ;
	}

	/**
	 * Méthode pour effectuer une recherche avancée. Il est recommandé d'utiliser uniquement des options issues
	 * des tableaux matchfieldTypes, matchmodeTypes et scopeTypes (dont on peut avoir un aperçu dans leur JavaDoc).
	 * On peut aussi récupérer l'équivalent en bon français de ces options dans les tableaux matchfield, matchmode et scope.
	 * @param query est le corps de la recherche.
	 * @param matchfield est le champ des profils de l'annuaire dans lesquels le corps sera recherché.
	 * @param matchmode est le type de recherche (contient, est égal, etc...).
	 * @param scope est la portée de la recherche.
	 * @return un tableau de personnes, dont seuls le nom (cn) et l'uid sont initialisés.
	 * @throws JSONException
	 * @throws IOException
	 */
	public PersonneMulot[] search(String query, String matchfield, String matchmode, String scope) throws JSONException, IOException {

		String content = "" ;
		String url = "https://nsa.emn.fr/z-sic/aie/WS/basic-search.php";
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> liste = new ArrayList<NameValuePair>(6);
		liste.add(new BasicNameValuePair("searchmode", "basic"));
		liste.add(new BasicNameValuePair("reqattrs", "cn,uid"));
		liste.add(new BasicNameValuePair("criteria", query));
		liste.add(new BasicNameValuePair("matchfield", matchfield));
		liste.add(new BasicNameValuePair("matchmode", matchmode));
		liste.add(new BasicNameValuePair("scope", scope));
		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));

		post.addHeader("Authorization", "Basic " + this.getKey());

		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}

		if (response.getStatusLine().getStatusCode() == 200) {

			JSONObject form = new JSONObject(content);

			if (form.getBoolean("yes")) {

				JSONObject answer = form.getJSONObject("answer");
				int count = answer.getInt("count");

				if (count == 0) {
					System.out.println("Aucun résultat");
					return null ;
				}

				PersonneMulot[] personnes = new PersonneMulot[count] ;

				JSONObject entries = answer.getJSONObject("entries") ;
				Iterator<?> keys = entries.keys() ;
				int i = 0 ;
				while (keys.hasNext()) {
					String key = (String)keys.next();
					JSONObject personne = entries.getJSONObject(key) ;
					personnes[i] = new PersonneMulot() ;
					personnes[i].setCn(personne.getJSONArray("cn").getString(0)) ;
					personnes[i].setUid(Integer.parseInt(personne.getJSONArray("uid").getString(0)));
					System.out.println(personnes[i].getCn() + " " + personnes[i].getUid());
					i++ ;
				}

				return personnes ;
			}
			else {
				System.out.println("Échec de la recherche : " + form.getString("message"));
				return null ;
			}
		}
		else {
			System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
			return null ;
		}
	}

	/**
	 * Méthode qui retourne le profil d'une personne (avec null pour les champs non remplis).
	 * @param uid est l'uid de la personne dont on veut obtenir le profil
	 * @return le profil, sous forme d'un objet Personne
	 * @throws IOException
	 * @throws JSONException
	 */
	public PersonneMulot getPersonne(int uid) throws IOException, JSONException {

		String content = "" ;
		String url = "https://nsa.emn.fr/z-sic/aie/WS/entry-getdata.php";
		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> liste = new ArrayList<NameValuePair>(2);
		liste.add(new BasicNameValuePair("isdn", "uid=" + uid + ",a-section=people,dc=emn,dc=fr"));
		liste.add(new BasicNameValuePair("attributes", "cn,sn,givenName,homePhone,mobile,mail,homePostalAddress,"
				+ "personalTitle,o,ou,organizationalStatus,telephoneNumber,a-login,a-homepage,a-nickname,a-gender,"
				+ "photoURL,a-promo,l,employeeType,roomNumber,a-mailexterne, a-select"));
		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));

		post.addHeader("Authorization", "Basic " + this.getKey());
		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}

		//System.out.println(content) ;
		if (response.getStatusLine().getStatusCode() == 200) {

			JSONObject form = new JSONObject(content);


			if (form.getBoolean("yes")) {

				JSONObject answer = form.getJSONObject("answer");

				PersonneMulot p = new PersonneMulot(uid) ;

				if (answer.optJSONArray("cn") != null) {p.setCn(answer.getJSONArray("cn").getString(0));}
				if (answer.optJSONArray("sn") != null) {p.setSn(answer.getJSONArray("sn").getString(0));}
				if (answer.optJSONArray("givenName") != null) {p.setGivenName(answer.getJSONArray("givenName").getString(0));}
				if (answer.optJSONArray("homePhone") != null) {p.setHomephone(answer.getJSONArray("homePhone").getString(0));}
				if (answer.optJSONArray("mobile") != null) {p.setMobile(answer.getJSONArray("mobile").getString(0));}
				if (answer.optJSONArray("mail") != null) {p.setMail(answer.getJSONArray("mail").getString(0));}
				if (answer.optJSONArray("chez") != null) {p.setChez(answer.getJSONArray("chez").getString(0));}
				if (answer.optJSONArray("ADR") != null) {p.setADR(answer.getJSONArray("ADR").getString(0));}
				if (answer.optJSONArray("ADR2") != null) {p.setADR2(answer.getJSONArray("ADR2").getString(0));}
				if (answer.optJSONArray("ADR3") != null) {p.setADR3(answer.getJSONArray("ADR3").getString(0));}
				if (answer.optJSONArray("CPO") != null) {p.setCPO(answer.getJSONArray("CPO").getString(0));}
				if (answer.optJSONArray("VIL") != null) {p.setVIL(answer.getJSONArray("VIL").getString(0));}
				if (answer.optJSONArray("state") != null) {p.setState(answer.getJSONArray("state").getString(0));}
				if (answer.optJSONArray("PAY") != null) {p.setPAY(answer.getJSONArray("PAY").getString(0));}
				if (answer.optJSONArray("personalTitle") != null) {p.setPersonalTitle(answer.getJSONArray("personalTitle").getString(0));}
				if (answer.optJSONArray("o") != null) {p.setO(answer.getJSONArray("o").getString(0));}
				if (answer.optJSONArray("ou") != null) {p.setOu(answer.getJSONArray("ou").getString(0));}
				if (answer.optJSONArray("organizationalStatus") != null) {p.setOrganizationalStatus(answer.getJSONArray("organizationalStatus").getString(0));}
				if (answer.optJSONArray("telephoneNumber") != null) {p.setTelephoneNumber(answer.getJSONArray("telephoneNumber").getString(0));}
				if (answer.optJSONArray("a-login") != null) {p.setA_login(answer.getJSONArray("a-login").getString(0));}
				if (answer.optJSONArray("a-homepage") != null) {p.setA_homepage(answer.getJSONArray("a-homepage").getString(0));}
				if (answer.optJSONArray("a-nickname") != null) {p.setA_nickname(answer.getJSONArray("a-nickname").getString(0));}
				if (answer.optJSONArray("a-gender") != null) {p.setA_gender(answer.getJSONArray("a-gender").getString(0));}
				if (answer.optJSONArray("photoURL") != null) {p.setPhotoURL(answer.getJSONArray("photoURL").getString(0));}
				if (answer.optJSONArray("a-promo") != null) {p.setA_promo(answer.getJSONArray("a-promo").getString(0));}
				if (answer.optJSONArray("l") != null) {p.setL(answer.getJSONArray("l").getString(0));}
				if (answer.optJSONArray("employeeType") != null) {p.setEmployeeType(answer.getJSONArray("employeeType").getString(0));}
				if (answer.optJSONArray("roomNumber") != null) {p.setRoomNumber(answer.getJSONArray("roomNumber").getString(0));}
				if (answer.optJSONArray("a-mailexterne") != null) {p.setA_mailexterne(answer.getJSONArray("a-mailexterne").getString(0));}
				if (answer.optJSONArray("a-mailexterne") != null) {p.setA_mailexterne(answer.getJSONArray("a-mailexterne").getString(0));}
				if (answer.optJSONArray("a-select") != null) {
					JSONArray a_select = answer.getJSONArray("a-select") ;
					String[] array = new String[a_select.length()] ;
					for (int i = 0 ; i < a_select.length() ; i ++) {
						array[i] = a_select.getString(i) ;
					}
					p.setA_select(array);
				}

				System.out.println("Profil de " + p.getCn() + " (" + p.getUid() + ") récupéré.");

				return p ;
			}
			else {
				System.out.println("Échec de la récupération du profil : " + form.getString("message"));
				return null ;
			}
		}
		else {
			System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
			return null ;
		}
	}

	/**
	 * Retourne le profil de la personne connectée sous la forme d'un objet PersonneMulot.
	 * @param strong est le mot de passe fort, nécessaire pour cette opération
	 * @return le profil de la personne connectée
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public PersonneMulot getProfil(String strong) throws ClientProtocolException, IOException, JSONException {

		String content = "" ;
		String url = "https://nsa.emn.fr/z-sic/aie/WS/user-login.php";

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> liste = new ArrayList<NameValuePair>(2);
		liste.add(new BasicNameValuePair("a-login", this.getUser()));
		liste.add(new BasicNameValuePair("password", strong));
		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));
		post.addHeader("Authorization", "Basic " + this.getKey());

		HttpResponse response = client.execute(post);
		response.getEntity().consumeContent();

		url = "https://nsa.emn.fr/z-sic/aie/WS/entry-getdata.php";

		post = new HttpPost(url);
		liste = new ArrayList<NameValuePair>(1);
		liste.add(new BasicNameValuePair("attributes", "uid, cn, sn, givenName, homePhone, mobile, mail, homePostalAddress, personalTitle, o, ou, "
				+ "organizationalStatus, a-login, a-homepage, a-nickname, a-gender,"
				+ "photoURL, a-promo, l, employeeType, roomNumber, a-mailexterne"));
		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));
		post.addHeader("Authorization", "Basic " + this.getKey());

		response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}
		PersonneMulot p = new PersonneMulot();

		if (response.getStatusLine().getStatusCode() == 200) {

			JSONObject form = new JSONObject(content);

			if (form.getBoolean("yes")) {

				JSONObject answer = form.getJSONObject("answer");

				String s = answer.getString("dn") ;
				s = s.substring(s.indexOf("=") + 1) ;
				s = s.substring(0, s.indexOf(",")) ;

				p = new PersonneMulot(Integer.parseInt(s)) ;

				if (answer.optJSONArray("cn") != null) {p.setCn(answer.getJSONArray("cn").getString(0));}
				if (answer.optJSONArray("sn") != null) {p.setSn(answer.getJSONArray("sn").getString(0));}
				if (answer.optJSONArray("givenName") != null) {p.setGivenName(answer.getJSONArray("givenName").getString(0));}
				if (answer.optJSONArray("homePhone") != null) {p.setHomephone(answer.getJSONArray("homePhone").getString(0));}
				if (answer.optJSONArray("mobile") != null) {p.setMobile(answer.getJSONArray("mobile").getString(0));}
				if (answer.optJSONArray("mail") != null) {p.setMail(answer.getJSONArray("mail").getString(0));}
				if (answer.optJSONArray("chez") != null) {p.setChez(answer.getJSONArray("chez").getString(0));}
				if (answer.optJSONArray("ADR") != null) {p.setADR(answer.getJSONArray("ADR").getString(0));}
				if (answer.optJSONArray("ADR2") != null) {p.setADR2(answer.getJSONArray("ADR2").getString(0));}
				if (answer.optJSONArray("ADR3") != null) {p.setADR3(answer.getJSONArray("ADR3").getString(0));}
				if (answer.optJSONArray("CPO") != null) {p.setCPO(answer.getJSONArray("CPO").getString(0));}
				if (answer.optJSONArray("VIL") != null) {p.setVIL(answer.getJSONArray("VIL").getString(0));}
				if (answer.optJSONArray("state") != null) {p.setState(answer.getJSONArray("state").getString(0));}
				if (answer.optJSONArray("PAY") != null) {p.setPAY(answer.getJSONArray("PAY").getString(0));}
				if (answer.optJSONArray("personalTitle") != null) {p.setPersonalTitle(answer.getJSONArray("personalTitle").getString(0));}
				if (answer.optJSONArray("o") != null) {p.setO(answer.getJSONArray("o").getString(0));}
				if (answer.optJSONArray("ou") != null) {p.setOu(answer.getJSONArray("ou").getString(0));}
				if (answer.optJSONArray("organizationalStatus") != null) {p.setOrganizationalStatus(answer.getJSONArray("organizationalStatus").getString(0));}
				if (answer.optJSONArray("a-login") != null) {p.setA_login(answer.getJSONArray("a-login").getString(0));}
				if (answer.optJSONArray("a-homepage") != null) {p.setA_homepage(answer.getJSONArray("a-homepage").getString(0));}
				if (answer.optJSONArray("a-nickname") != null) {p.setA_nickname(answer.getJSONArray("a-nickname").getString(0));}
				if (answer.optJSONArray("a-gender") != null) {p.setA_gender(answer.getJSONArray("a-gender").getString(0));}
				if (answer.optJSONArray("photoURL") != null) {p.setPhotoURL(answer.getJSONArray("photoURL").getString(0));}
				if (answer.optJSONArray("a-promo") != null) {p.setA_promo(answer.getJSONArray("a-promo").getString(0));}
				if (answer.optJSONArray("l") != null) {p.setL(answer.getJSONArray("l").getString(0));}
				if (answer.optJSONArray("employeeType") != null) {p.setEmployeeType(answer.getJSONArray("employeeType").getString(0));}
				if (answer.optJSONArray("roomNumber") != null) {p.setRoomNumber(answer.getJSONArray("roomNumber").getString(0));}
				if (answer.optJSONArray("a-mailexterne") != null) {p.setA_mailexterne(answer.getJSONArray("a-mailexterne").getString(0));}
				if (answer.optJSONArray("a-select") != null) {
					JSONArray a_select = answer.getJSONArray("a-select") ;
					String[] array = new String[a_select.length()] ;
					for (int i = 0 ; i < a_select.length() ; i ++) {
						array[i] = a_select.getString(i) ;
					}
					p.setA_select(array);
				}

				System.out.println("Profil récupéré avec succès.");

				return p ;
			}
			else {
				System.out.println("Échec de la récupération du profil : " + form.getString("message"));
				
				return p ;
			}
		}
		else {
			System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
			return p ;
		}

	}

	/**
	 * Édite le profil de la personne connectée à partir d'un profil le plus complet possible.
	 * @param p est le profil que va utiliser le Mulot pour modifier le profil de la personne connectée
	 * @param strong est le mot de passe fort, nécessaire pour cette opération
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public void editProfil(PersonneMulot p, String strong) throws ClientProtocolException, IOException, JSONException {

		String content = "" ;
		String url = "https://nsa.emn.fr/z-sic/aie/WS/user-login.php";

		HttpClient client = new DefaultHttpClient();
		HttpPost post = new HttpPost(url);
		List<NameValuePair> liste = new ArrayList<NameValuePair>(2);
		liste.add(new BasicNameValuePair("a-login", this.getUser()));
		liste.add(new BasicNameValuePair("password", strong));
		post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));
		post.addHeader("Authorization", "Basic " + this.getKey());

		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}

		if (response.getStatusLine().getStatusCode() == 200) {

			url = "https://nsa.emn.fr/z-sic/aie/WS/entry-setdata.php";

			post = new HttpPost(url);
			liste = new ArrayList<NameValuePair>(29);
			liste.add(new BasicNameValuePair("uid", "" + p.getUid()));
			liste.add(new BasicNameValuePair("cn", p.getCn()));
			liste.add(new BasicNameValuePair("sn", p.getSn()));
			liste.add(new BasicNameValuePair("givenName", p.getGivenName()));
			liste.add(new BasicNameValuePair("homePhone", p.getHomephone()));
			liste.add(new BasicNameValuePair("mobile", p.getMobile()));
			liste.add(new BasicNameValuePair("mail", p.getMail()));
			liste.add(new BasicNameValuePair("chez", p.getChez()));
			liste.add(new BasicNameValuePair("ADR", p.getADR()));
			liste.add(new BasicNameValuePair("ADR2", p.getADR2()));
			liste.add(new BasicNameValuePair("ADR3", p.getADR3()));
			liste.add(new BasicNameValuePair("CPO", p.getCPO()));
			liste.add(new BasicNameValuePair("VIL", p.getVIL()));
			liste.add(new BasicNameValuePair("state", p.getState()));
			liste.add(new BasicNameValuePair("PAY", p.getPAY()));
			liste.add(new BasicNameValuePair("personalTitle", p.getPersonalTitle()));
			liste.add(new BasicNameValuePair("o", p.getO()));
			liste.add(new BasicNameValuePair("ou", p.getOu()));
			liste.add(new BasicNameValuePair("organizationalStatus", p.getOrganizationalStatus()));
			liste.add(new BasicNameValuePair("a-login", p.getA_login()));
			liste.add(new BasicNameValuePair("a-homepage", p.getA_homepage()));
			liste.add(new BasicNameValuePair("a-nickname", p.getA_nickname()));
			liste.add(new BasicNameValuePair("a-gender", p.getA_gender()));
			liste.add(new BasicNameValuePair("photoURL", p.getPhotoURL()));
			//liste.add(new BasicNameValuePair("a-promo", p.getA_promo()));
			liste.add(new BasicNameValuePair("l", p.getL()));
			liste.add(new BasicNameValuePair("employeeType", p.getEmployeeType()));
			liste.add(new BasicNameValuePair("roomNumber", p.getRoomNumber()));
			liste.add(new BasicNameValuePair("a-mailexterne", p.getA_mailexterne()));
			post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));

			post.addHeader("Authorization", "Basic " + this.getKey());

			response = client.execute(post);
			respEntity = response.getEntity();
			if (respEntity != null) {
				content =  EntityUtils.toString(respEntity);
			}

			JSONObject form = new JSONObject(content);
			//TODO
			System.out.println(form);

			if (form.getBoolean("yes")) {
				System.out.println("Profil édité avec succès.");
			}
			else {
				System.out.println("Échec de la modification du profil : " + form.getString("message"));
			}
		}
		else {
			System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
		}
	}

	/**
	 * Modifie les mots de passe fort et faible. La méthode modifie également le mot de passe faible de l'instance de Mulot en cours,
	 * mais pas le mot de passe fort stocké dans l'app. Il faut le faire à part.
	 * @param current est le mot de passe fort actuel
	 * @param strong est le nouveau mot de passe fort
	 * @param weak est le nouveau mot de passe faible
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public void editPasswords(String current, String strong, String weak) throws ClientProtocolException, IOException, JSONException {

		boolean weakContainsNumber = weak.matches(".*\\d.*") ;
		boolean weakLength = weak.length() >= 5 && weak.length() <= 8 ;
		boolean strongLength = strong.length() >= 4 && strong.length() <= 8 ;

		if (weakContainsNumber && weakLength && strongLength) {

			String content = "" ;
			String url = "https://nsa.emn.fr/z-sic/aie/WS/user-login.php";

			HttpClient client = new DefaultHttpClient();
			HttpPost post = new HttpPost(url);
			List<NameValuePair> liste = new ArrayList<NameValuePair>(2);
			liste.add(new BasicNameValuePair("a-login", this.getUser()));
			liste.add(new BasicNameValuePair("password", current));
			post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));
			post.addHeader("Authorization", "Basic " + this.getKey());

			HttpResponse response = client.execute(post);
			HttpEntity respEntity = response.getEntity();
			if (respEntity != null) {
				content =  EntityUtils.toString(respEntity);
			}

			if (response.getStatusLine().getStatusCode() == 200) {

				url = "https://nsa.emn.fr/z-sic/aie/WS/change-password.php";

				post = new HttpPost(url);
				liste = new ArrayList<NameValuePair>(5);
				liste.add(new BasicNameValuePair("current", current));
				liste.add(new BasicNameValuePair("userPassword", strong));
				liste.add(new BasicNameValuePair("userPasswordRetype", strong));
				liste.add(new BasicNameValuePair("a-weak", weak));
				liste.add(new BasicNameValuePair("a-weakRetype", weak));
				liste.add(new BasicNameValuePair("strong", "x"));
				liste.add(new BasicNameValuePair("canalh", "x"));
				liste.add(new BasicNameValuePair("eleve", "x"));
				liste.add(new BasicNameValuePair("weak", "x"));
				liste.add(new BasicNameValuePair("gev", "x"));
				post.setEntity(new UrlEncodedFormEntity(liste, "UTF-8"));
				post.addHeader("Authorization", "Basic " + this.getKey());

				response = client.execute(post);
				respEntity = response.getEntity();
				if (respEntity != null) {
					content =  EntityUtils.toString(respEntity);
				}

				JSONObject form = new JSONObject(content) ;

				if (form.getBoolean("yes")) {
					String s = this.getUser() + ":" + weak ;
					s = new String(Base64.encodeToString(s.getBytes("UTF-8"), Base64.NO_WRAP));
					this.setKey(s);

					System.out.println("Mots de passes modifiés avec succès.");
				}
				else {
					System.out.println("Échec de la modification des mots de passe : " + form.getString("message"));
				}
			}
			else {
				System.out.println("Problème de connexion de type " + response.getStatusLine().getStatusCode() + ".");
			}
		}
		else {
			System.out.println("Les mots de passe ne sont pas conformes.");
		}
	}

	public boolean photoEnabled(PersonneMulot p) {
		boolean b = false ;
		for (int i = 0 ; i < p.getA_select().length ; i++) {
			b = (p.getA_select()[i].equals("DI-AIE")) || b ;
		}
		return b ;
	}

	/**
	 * Methode qui retourne la photo d'une personne, sous forme de Bitmap, à partir de son uid.
	 * Pour des raisons aisées à comprendre, la photo ne doit être récupérée qu'après un test positif avec la fonction photoEnabled().
	 * @param uid est l'uid de la personne de la photo
	 * @return la photo sous forme de Bitmap
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public Bitmap getPhoto(int uid) throws ClientProtocolException, IOException, JSONException {

		String url = "https://nsa.emn.fr/z-sic/aie/photo-id/" + uid + ".jpg" ;

		HttpClient client = new DefaultHttpClient();
		HttpGet get = new HttpGet(url);

		get.addHeader("Authorization", "Basic " + this.getKey());

		HttpResponse response = client.execute(get);
		BufferedHttpEntity entity = new BufferedHttpEntity(response.getEntity());
		InputStream stream = entity.getContent();

		return BitmapFactory.decodeStream(stream);
	}
}
