package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import main.Alumni;
import main.Mulot;
import main.PersonneAlumni;
import main.PersonneMulot;

import org.json.JSONException;

import tools.AdvancedSearchSpinnerAdapter;

import fr.vitamines.annuaire.android.BuildConfig;
import fr.vitamines.annuaire.android.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.ListFragment;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TextView.OnEditorActionListener;

public class AnnuaireFragment extends ListFragment {

	//Variables utiles pour tout
	private int searchType=Accueil.MULOT_SEARCH_TYPE;
	private String title="Mulot";
	private String identifiant;
	private String weakpassword;
	public static String field = "cn";
	public static Mulot user;
	public static Alumni userAlumni;
	public static PersonneMulot[] resultatsMulot;
	public static PersonneAlumni[] resultatsAlumni;

	//Variables pour la recherche avancée
	public static boolean advancedSearchOn = false;
	private String[] fieldValuesAlumni ={"CN","a-option","a-classe","a-gender","o","VIL","postalAdress"};
	private String[] fieldValuesMulot ={"cn", "a-promo","mobile","telephoneNumber"};

	//widgets
	private EditText champDeRecherche;
	public static Spinner advancedSearchSpinner;


	/**
	 * Returns a new instance of this fragment for the given section number.
	 */
	public static AnnuaireFragment newInstance(int searchType, String title) {
		AnnuaireFragment fragment = new AnnuaireFragment();
		fragment.searchType=searchType;
		fragment.title=title;

		return fragment;
	}

	public AnnuaireFragment() {
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View rootView = inflater.inflate(R.layout.annuaire_fragment,
				container, false);
		champDeRecherche =(EditText) rootView.findViewById(R.id.champ_de_recherche);
		champDeRecherche.setOnEditorActionListener(new OnEditorActionListener() {
			@Override
			public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
				if(actionId == EditorInfo.IME_ACTION_SEARCH){
					if(Connexion.isDeviceConnected(getActivity())){
						if(searchType==Accueil.MULOT_SEARCH_TYPE){
							SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
							identifiant = preferences.getString("identifiant", null);
							weakpassword = preferences.getString("weakpassword", null); 
							displayMulotResults();
						}
						else{
							displayAlumniResults();
						}
					}
					else{
						notConnectedDialog(getActivity());
					}
				}
				return false;
			}
		});
		advancedSearchSpinner=(Spinner) rootView.findViewById(R.id.advanced_search_spinner);

		setSpinner();

		advancedSearchSpinner.setVisibility(View.GONE);


		return rootView;
	}
	
	/**
	 * Boite de dialogue qui apparait si l'appareil n'est pas connecté à internet, et qui propose de réessayer, dès que l'utilisateur a connecté l'appareil.
	 * @param context
	 */
	public void notConnectedDialog(Context context){
		Builder notConnected= new AlertDialog.Builder(context);
		notConnected.setCancelable(true);
		notConnected.setPositiveButton("Réessayer", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				getActivity().recreate();				
			}
		});
		LayoutInflater factory = LayoutInflater.from(context);
		final View notconnectedDialog = factory.inflate(R.layout.device_not_connected_dialog, null);
		notConnected.setView(notconnectedDialog);
		notConnected.setTitle("Non connecté");
		notConnected.show();
	}

	@Override
	public void onListItemClick(ListView l, View v, int position, long id){
		super.onListItemClick(l, v, position, id);
		if(searchType==Accueil.MULOT_SEARCH_TYPE){
			Intent intent = new Intent(getActivity(),ShowResultsMulot.class);
			Bundle extras = new Bundle();
			extras.putInt("position", position);
			extras.putString("weakpassword", weakpassword);
			extras.putString("identifiant", identifiant);
			intent.putExtras(extras);
			startActivity(intent);
		}
		else{
			Intent intent = new Intent(getActivity(),ShowResultsAlumni.class);
			Bundle extras = new Bundle();
			extras.putInt("position", position);
			intent.putExtras(extras);
			startActivity(intent);
		}
	}

	public void setSpinner(){
		AdvancedSearchSpinnerAdapter spinnerAdapter;

		spinnerAdapter = new AdvancedSearchSpinnerAdapter(getActivity(), searchType);	

		advancedSearchSpinner.setAdapter(spinnerAdapter);
		advancedSearchSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int position, long id) {
				if(searchType==Accueil.MULOT_SEARCH_TYPE){
					field=fieldValuesMulot[position];
				}
				else{
					field=fieldValuesAlumni[position];
				}				
			}
			@Override
			public void onNothingSelected(AdapterView<?> parent) {			}
		});
	}

	/**
	 * traite les résultats de la recherche sur l'annuaire mulot, pour n'afficher que la liste des prénoms et noms
	 * @param resultats
	 * @return
	 */
	public static String[] getListeNoms(PersonneMulot[] resultats){
		String[] listeNoms = new String[resultats.length];
		for(int i=0;i<resultats.length;i++){
			listeNoms[i]=resultats[i].getCn();
		}
		return listeNoms;
	}

	/**
	 * traite les résultats de la recherche sur l'annuaire Alumni, pour n'afficher que la liste des prénoms et noms
	 * @param resultats
	 * @return
	 */
	public static String[] getListeNoms(PersonneAlumni[] resultats){
		String[] listeNoms = new String[resultats.length];
		for(int i=0;i<resultats.length;i++){
			listeNoms[i]=resultats[i].getCn();
		}
		return listeNoms;
	}

	/**
	 * affiche la liste des résultats de la recherche sur l'annuaire Mulot
	 */
	public void displayMulotResults(){
		PersonneMulot[] resultats=null; 
		try {
			resultats = (new BackTaskMulot().execute(champDeRecherche.getText().toString())).get();
		} catch (InterruptedException | ExecutionException e) {
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		}
		if(resultats!=null){
			String[] listeNoms = getListeNoms(resultats);
			setListAdapter(new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,listeNoms));
		}
		else{
			noResultsDialog();
		}
	}

	/**
	 * affiche la liste des résultats de la recherche sur l'annuaire Alumni
	 */
	public void displayAlumniResults(){
		PersonneAlumni[] resultats=null;
		try{
			resultats=(new BackTaskAlumni().execute(champDeRecherche.getText().toString())).get();
		} catch(Exception e){
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		}
		if(resultats!=null){
			String[] listeNoms = getListeNoms(resultats);
			setListAdapter(new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_1,listeNoms));
		}
		else{
			noResultsDialog();
		}
	}

	/**
	 * Crée et affiche une boite de dialogue si la recherche n'a pas donné de résulat
	 */
	public void noResultsDialog(){
		Builder erreur= new  AlertDialog.Builder(getActivity());
		erreur.setCancelable(true);
		LayoutInflater factory = LayoutInflater.from(getActivity());
		final View erreurDialog = factory.inflate(R.layout.no_result_dialog, null);
		erreur.setView(erreurDialog);
		erreur.setTitle("Aucun résultat");
		erreur.show();
	}

	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((Accueil) activity).onSectionAttached(title);
	}

	private class BackTaskMulot extends AsyncTask<String,Void, PersonneMulot[]>{
		@Override
		protected PersonneMulot[] doInBackground(String... arg0) {
			resultatsMulot=null;
			try {
				user = new Mulot(identifiant,weakpassword);
				resultatsMulot = user.search(arg0[0],field,"contains","people");
			} catch (JSONException | IOException e1) {
				if(BuildConfig.DEBUG){
					System.out.println(e1);
				}
			}
			return resultatsMulot;
		}
	}

	private class BackTaskAlumni extends AsyncTask<String,Void,PersonneAlumni[]>{
		@Override
		protected PersonneAlumni[] doInBackground(String... params) {
			resultatsAlumni = null;
			try{
				userAlumni = new Alumni("mlemon13", "LTMX45");
				resultatsAlumni =userAlumni.search(params[0], field, "SNGN");
			}
			catch(JSONException | IOException e){
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}	
			return resultatsAlumni;
		}

	}
}
