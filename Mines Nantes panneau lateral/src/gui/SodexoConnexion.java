package gui;


import java.util.concurrent.ExecutionException;

import main.Sodexo;
import tools.CreateSodexo;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import fr.vitamines.annuaire.android.R;

public class SodexoConnexion extends Fragment{

	private EditText sodexoidentifiant;
	private EditText sodexopassword;
	private Button sodexovalider;
	private CheckBox sodexoRemember;
	private TextView sodexopresentation;

	private static SharedPreferences preferences;

	public static Sodexo compteUtilisateur;

	public static Fragment newInstance(){
		return new SodexoConnexion();
	}
	
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.sodexoconnexion, container, false);

		preferences = PreferenceManager.getDefaultSharedPreferences(getActivity());

		sodexoidentifiant=(EditText) rootView.findViewById(R.id.sodexoidentifiant);
		sodexopassword=(EditText) rootView.findViewById(R.id.sodexopassword);
		sodexovalider=(Button) rootView.findViewById(R.id.sodexovalider);
		sodexoRemember=(CheckBox) rootView.findViewById(R.id.sodexoremember);
		sodexopresentation=(TextView) rootView.findViewById(R.id.sodexopresentation);

		sodexopresentation.setText("Si vous utilisez le restaurant et/ou la"
				+ "\ncafeteria de l'école, vous pouvez consulter"
				+ "\nici le montant restant sur votre carte ainsi"
				+ "\n que vos tickets de caisse."
				+ "\n"
				+ "\nPour obtenir vos identifiants Sodexo,"
				+ "\ndemandez à votre passage en caisse ou "
				+ "\nconsultez la borne à l'entrée du restaurant.");

		String regId = preferences.getString("sodexousername", null);
		String regMdp = preferences.getString("sodexomdp", null);




		sodexovalider.setOnClickListener(new OnClickListener(){
			public void onClick(View v) {


				if(Connexion.isDeviceConnected(getActivity())){


					String identifiant = sodexoidentifiant.getText().toString();
					String password = sodexopassword.getText().toString();
					try {
						compteUtilisateur = new CreateSodexo(getActivity()).execute(identifiant,password).get();
					} catch (InterruptedException e) {
						System.out.println(e);
					} catch (ExecutionException e) {
						System.out.println("ExecutionException: "+e);
					} 
					if(sodexoRemember.isChecked()){
						preferences.edit().putString("sodexousername", identifiant).putString("sodexomdp", password ).commit();
					}
					if(compteUtilisateur!=null){
						if(compteUtilisateur.getMontantRestant()!=-3.14){
							
							getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.container, SodexoSoldeFragment.newInstance(true)).commit();
						}

						else{

							Builder dialo= new  AlertDialog.Builder(getActivity());
							dialo.setCancelable(true);
							dialo.setTitle("Identifiant ou mot de passe erroné!");
							dialo.show();
						}
					}

				}
				else{
					Builder notConnected= new AlertDialog.Builder(getActivity());
					notConnected.setCancelable(true);
					LayoutInflater notConnectedLayoutInflater = LayoutInflater.from(getActivity());
					final View notconnectedDialog = notConnectedLayoutInflater.inflate(R.layout.device_not_connected_dialog, null);
					notConnected.setView(notconnectedDialog);
					notConnected.setTitle("Non connecté");
					notConnected.show();
				}
			}
		});
		if(regId!=null && regMdp!=null){
			sodexoidentifiant.setText(regId);
			sodexopassword.setText(regMdp);
			sodexoRemember.setChecked(true);
			sodexovalider.performClick();
		}
		
		return rootView;
	}
}
