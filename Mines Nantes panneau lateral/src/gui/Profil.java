package gui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

import main.Alumni;
import main.Mulot;
import main.PersonneMulot;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import fr.vitamines.annuaire.android.BuildConfig;
import fr.vitamines.annuaire.android.R;

public class Profil extends Activity {

	private Dialog dialoattente;

	private Mulot user;
	private Alumni userAlumni;
	private boolean bureau;
	private boolean isEditing = false;
	private String identifiant;
	private String weakpassword;
	private String strongpassword;

	private TextView nomV;
	private String nom;
	private int uid;

	private Bitmap bMap2;
	private ImageView photo;
	private String numberfixe="";
	private String numbermob="";
	private TextView fixeV;
	private TextView fixeRespV;
	private EditText fixeEdit;
	private TextView mobileRespV;
	private EditText mobileEdit;
	private TextView mobileV;

	private PersonneMulot mProfil=null;



	private String mailint="";
	private String mailext="";
	private TextView mailintRespV;
	private TextView mailintV;
	private TextView mailextRespV;
	private EditText mailextEdit;
	private TextView mailextV;
	private String ADR="";
	private TextView AdrRespV;
	private TextView AdrV;
	private TextView adrTextEdit;
	private EditText adrEdit;
	private TextView postalTextEdit;
	private EditText postalEdit;
	private TextView villeTextEdit;
	private EditText villeEdit;
	private TextView stateTextEdit;
	private EditText stateEdit;

	private String ADR2="";
	private String ADR3="";
	private String CPO="";
	private String promo="";
	private TextView promoRespV;
	private TextView promoV;

	private String ville="";
	private String etat="";
	private String Surnom="";
	private TextView SurnomRespV;
	private EditText surnomEdit;
	private TextView SurnomV;

	private String roomNumber="";
	private TextView roomNumberRespV;
	private EditText flatEdit;
	private TextView roomNumberV;

	public static String result;
	private String adresse = "";
	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);

//		ActionBar bar = getActionBar();
//		bar.setTitle("Annuaire");

		setContentView(R.layout.profil);

		photo = (ImageView) findViewById(R.id.photo_profil);
		nomV= (TextView) findViewById(R.id.nom_profil);

		fixeRespV=(TextView)findViewById(R.id.fixeRespV_profil);
		fixeEdit =(EditText) findViewById(R.id.fixe_edit);
		fixeV=(TextView)findViewById(R.id.fixeV_profil);

		mobileRespV=(TextView)findViewById(R.id.mobileRespV_profil);
		mobileEdit =(EditText) findViewById(R.id.mobile_edit);
		mobileV =(TextView) findViewById(R.id.mobileV_profil);

		roomNumberRespV=(TextView) findViewById(R.id.roomNumberRespV_profil);
		roomNumberV=(TextView) findViewById(R.id.roomNumberV_profil);
		flatEdit = (EditText) findViewById(R.id.flat_edit);

		mailintRespV=(TextView) findViewById(R.id.mailintRespV_profil);
		mailintV=(TextView) findViewById(R.id.mailintV_profil);


		mailextRespV=(TextView) findViewById(R.id.mailextRespV_profil);
		mailextV=(TextView) findViewById(R.id.mailextV_profil);
		mailextEdit=(EditText) findViewById(R.id.mailext_edit);

		SurnomRespV=(TextView) findViewById(R.id.SurnomRespV_profil);
		SurnomV=(TextView) findViewById(R.id.SurnomV_profil);
		surnomEdit =(EditText) findViewById(R.id.Surnom_edit);

		promoRespV=(TextView) findViewById(R.id.promoRespV_profil);
		promoV=(TextView) findViewById(R.id.promoV_profil);

		AdrRespV=(TextView) findViewById(R.id.adrRespV_profil);
		AdrV=(TextView) findViewById(R.id.AdrV_profil);
		adrTextEdit =(TextView) findViewById(R.id.adr_text_edit);
		adrEdit=(EditText)findViewById(R.id.adr_edit);
		postalTextEdit =(TextView) findViewById(R.id.postal_text_edit);
		postalEdit = (EditText)  findViewById(R.id.postal_edit);
		villeTextEdit= (TextView) findViewById(R.id.ville_text_edit);
		villeEdit  = (EditText) findViewById(R.id.ville_edit);
		stateTextEdit =(TextView) findViewById(R.id.state_text_edit);
		stateEdit = (EditText) findViewById(R.id.state_edit);

		SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
		identifiant = preferences.getString("identifiant", null);
		weakpassword = preferences.getString("weakpassword", null);

		try {
			user =new Mulot(identifiant,weakpassword);
		} catch (UnsupportedEncodingException e1) {
			if(BuildConfig.DEBUG){
				System.out.println(e1);
			}
		}

		try {
			String[] profil = (new GetLightProfil(this).execute()).get();
			nom = profil[0];
			uid= Integer.valueOf(profil[1]);
		} catch (InterruptedException | ExecutionException e) {
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		}

//		bar.setTitle(nom);
		nomV.setText(nom);
		nomV.setTextSize(20);
		nomV.setVisibility(View.GONE);

		Taskback task = new Taskback(this);
		task.execute();








	}

	private class Taskback extends AsyncTask<Void,Void,String>{
		private Context context;

		private Taskback(Context context){
			this.context=context;
		}

		protected void onPreExecute() {
			super.onPreExecute();
			dialoattente = Accueil.chargementDialog(context);
			dialoattente.show();

			Toast.makeText(context, "Chargement...", Toast.LENGTH_SHORT).show();
		}

		@Override
		protected String doInBackground(Void... params) {
			PersonneMulot pers= new PersonneMulot();
			try {
				pers = user.getPersonne(uid);
				if(pers.getA_nickname()!=null){
					Surnom=pers.getA_nickname();
				}
				if(pers.getA_promo()!=null){
					promo =pers.getA_promo();
				}
				if(pers.getMail()!=null){
					mailint = pers.getMail();				 
				}
				if(pers.getHomephone()!=null){
					numberfixe=pers.getHomephone();
				}
				if(pers.getMobile()!=null){
					numbermob=pers.getMobile();
				}
				if(pers.getA_mailexterne()!=null){
					mailext = pers.getA_mailexterne();
				}
				if(pers.getADR()!=null){
					ADR=pers.getADR();
				}
				if(pers.getADR2()!=null){
					ADR2=pers.getADR2();
				}
				if(pers.getADR3()!=null){
					ADR3=pers.getADR3();
				}
				if(pers.getCPO()!=null){
					CPO=pers.getCPO();
				}
				if(pers.getVIL()!=null){
					ville=pers.getVIL();
				}
				if(pers.getState()!=null){
					etat=pers.getState();
				}
				if (pers.getChez()!=null){
					bureau=false;
					roomNumber= pers.getChez();
				}
				else{
					bureau=true;
					if(pers.getRoomNumber()!=null){
						roomNumber=pers.getRoomNumber();
					}

				}
				bMap2= user.getPhoto(uid);
				adresse = ADR2+"\n"+CPO+"\n"+ville+"\n";


			} catch (ParseException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			} catch (IOException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			} catch (JSONException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}


			return result;
		}
		protected void onPostExecute(String f){
			dialoattente.dismiss();

			if(bureau){
				roomNumberV.setText("Bureau");
			}
			else{
				roomNumberV.setText("Appartement");
			}
			SurnomRespV.setText(Surnom);
			if(Surnom.length()==0){
				SurnomRespV.setVisibility(View.GONE);
				SurnomV.setVisibility(View.GONE);
			}
			promoRespV.setText(promo);
			if(promo.length()==0){
				promoV.setVisibility(View.GONE);
				promoRespV.setVisibility(View.GONE);
			}
			int arobase = mailint.indexOf("@");
			if(mailint.length()!=0){
				mailintRespV.setText(mailint.substring(0, arobase)+"\n"+mailint.substring(arobase));
			}
			if(mailint.length()==0){
				mailintV.setVisibility(View.GONE);
				mailintRespV.setVisibility(View.GONE);

			}
			int arobase2 = mailext.indexOf("@");
			if(mailext.length()!=0){
				mailextRespV.setText(mailext.substring(0,arobase2)+"\n"+mailext.substring(arobase2));
			}
			if(mailext.length()==0){
				mailextV.setVisibility(View.GONE);
				mailextRespV.setVisibility(View.GONE);

			}
			fixeRespV.setText(numberfixe);
			if(numberfixe.length()==0){
				fixeV.setVisibility(View.GONE);
				fixeRespV.setVisibility(View.GONE);

			}
			mobileRespV.setText(numbermob);
			if(numbermob.length()==0){
				mobileV.setVisibility(View.GONE);
				mobileRespV.setVisibility(View.GONE);

			}
			AdrRespV.setText(adresse+etat);

			if(ADR.length()==0){

				AdrV.setVisibility(View.GONE);
				AdrRespV.setVisibility(View.GONE);

			}
			roomNumberRespV.setText(roomNumber);
			if(roomNumber.length()==0){
				roomNumberV.setVisibility(View.GONE);
				roomNumberRespV.setVisibility(View.GONE);
			}
			if(bMap2!=null){
				photo.setImageBitmap(Bitmap.createScaledBitmap(bMap2, 180, 240, false));
			}


		}

	}

	private class GetLightProfil extends AsyncTask<Void, Void, String[]>{
		
		private Context context;
		private Dialog dialoattente;
		
		public GetLightProfil(Context context){
			this.context=context;
			dialoattente = Accueil.chargementDialog(this.context);
		}
		
		protected void onPreExecute(){
			dialoattente.show();
		}

		@Override
		protected String[] doInBackground(Void... params) {
			String[] result=null;
			try {
				result = new String[2];
				PersonneMulot profil= user.getLightProfil();
				result[0]= profil.getCn();
				result[1]=""+profil.getUid();
			} catch (IOException | JSONException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			return result;
		}
		
		protected void onPostExecute(String[] result){
			dialoattente.dismiss();
		}

	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu){
		MenuInflater inflater = new MenuInflater(this);
		inflater.inflate(R.menu.profil, menu);
		return super.onCreateOptionsMenu(menu);

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		item.getItemId();
		new GetProfil();
		if(isEditing && strongpassword==null){
			getStrongPasswordDialog();
		}

		if(!isEditing){			
			surnomEdit.setVisibility(View.VISIBLE);
			SurnomRespV.setVisibility(View.GONE);
			mailextRespV.setVisibility(View.GONE);
			mailextEdit.setVisibility(View.VISIBLE);
			fixeRespV.setVisibility(View.GONE);
			fixeEdit.setVisibility(View.VISIBLE);
			mobileRespV.setVisibility(View.GONE);
			mobileEdit.setVisibility(View.VISIBLE);
			flatEdit.setVisibility(View.VISIBLE);
			fixeV.setVisibility(View.VISIBLE);
			roomNumberRespV.setVisibility(View.GONE);
			AdrRespV.setVisibility(View.GONE);
			adrEdit.setVisibility(View.VISIBLE);
			adrTextEdit.setVisibility(View.VISIBLE);
			postalTextEdit.setVisibility(View.VISIBLE);
			postalEdit.setVisibility(View.VISIBLE);
			villeTextEdit.setVisibility(View.VISIBLE);
			villeEdit.setVisibility(View.VISIBLE);
			stateEdit.setVisibility(View.VISIBLE);
			stateTextEdit.setVisibility(View.VISIBLE);


			roomNumberV.setText("Appartement");
			surnomEdit.setText(Surnom);
			mailextEdit.setText(mailext);
			fixeEdit.setText(numberfixe);
			mobileEdit.setText(numbermob);
			if(!bureau){
				flatEdit.setText(roomNumber);
			}
			adrEdit.setText(ADR2);
			postalEdit.setText(CPO);
			villeEdit.setText(ville);
			stateEdit.setText(etat);

			item.setIcon(this.getResources().getDrawable(R.drawable.ok));
			isEditing=true;
		}

		return super.onOptionsItemSelected(item);

	}

	public void onDestroy(){
		super.onDestroy();
	}

	private class GetProfil extends AsyncTask<String, Void, Void>{
		@Override
		protected Void doInBackground(String... params) {

			try {
				mProfil = user.getProfil(params[0]);
			} catch (IOException | JSONException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			return null;

		}

	}

	public String getStrongPasswordDialog(){
		Builder dialo= new  AlertDialog.Builder(Profil.this);
		dialo.setCancelable(true);
		LayoutInflater factory = LayoutInflater.from(Profil.this);
		final View alertDialogView = factory.inflate(R.layout.get_strongpassword_dialog, null);
		dialo.setView(alertDialogView);
		TextView demande=(TextView) alertDialogView.findViewById(R.id.demandepws);
		demande.setText("Le mot de passe fort est nécessaire pour éditer son profil ");
		dialo.setTitle("Mot de passe fort");
		dialo.setPositiveButton("OK", new DialogInterface.OnClickListener(){
			public void onClick(DialogInterface dialog, int which) {
				EditText edit=(EditText) alertDialogView.findViewById(R.id.editpws);
				strongpassword=edit.getText().toString();
				try {
					if(new CreateAlumni().execute(identifiant,strongpassword).get()){
						if(userAlumni.getKey()=="echec"){
							strongpassword=null;
							Builder erreur= new  AlertDialog.Builder(Profil.this);
							erreur.setCancelable(true);
							erreur.setTitle("Mot de passe erroné!");
							erreur.show();
						}
						else{
							Dialog dialo = Accueil.chargementDialog(Profil.this);
							(new GetProfil()).execute(strongpassword).get();
							if(mProfil!=null){
								mProfil.setA_nickname(surnomEdit.getText().toString());
								mProfil.setA_mailexterne(mailextEdit.getText().toString());
								mProfil.setHomephone(fixeEdit.getText().toString());
								mProfil.setMobile(mobileEdit.getText().toString());
								mProfil.setChez(flatEdit.getText().toString());
								mProfil.setADR(adrEdit.getText().toString());
								mProfil.setCPO(postalEdit.getText().toString());
								mProfil.setVIL(villeEdit.getText().toString());
								mProfil.setState(stateEdit.getText().toString());

								isEditing=false;

								boolean done=false;
								try {
									done=(new SaveProfil()).execute(mProfil).get();
								} catch (InterruptedException | ExecutionException e) {
									if(BuildConfig.DEBUG){
										System.out.println(e);
									}
								}
								if(done){
									recreate();
								}
							}
							else{
								//TODO
							}
							dialo.dismiss();
						}
					}
				} catch (InterruptedException e) {
					if(BuildConfig.DEBUG){
						System.out.println(e);
					}
				} catch (ExecutionException e) {
					if(BuildConfig.DEBUG){
						System.out.println(e);
					}
				}
			}
		});
		dialo.show();
		return strongpassword;
	}

	private class CreateAlumni extends AsyncTask<String,Void,Boolean>{
		@Override
		protected Boolean doInBackground(String... params) {
			try {
				userAlumni = new Alumni(params[0],params[1]);
			} catch (ClientProtocolException e1) {
				if(BuildConfig.DEBUG){
					System.out.println(e1);
				}
			} catch (IOException e1) {
				if(BuildConfig.DEBUG){
					System.out.println(e1);
				}
			} catch (JSONException e1) {
				if(BuildConfig.DEBUG){
					System.out.println(e1);
				}
			}
			return true;
		}
	}

	private class SaveProfil extends AsyncTask<PersonneMulot, Void, Boolean>{
		@Override
		protected Boolean doInBackground(PersonneMulot... params) {
			try {
				user.editProfil(params[0], strongpassword);
				return true;
			} catch (IOException | JSONException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
				return false;
			}
		}
	}
}
