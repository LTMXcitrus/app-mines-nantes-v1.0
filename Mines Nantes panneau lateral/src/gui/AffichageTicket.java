package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import org.apache.http.client.ClientProtocolException;
import org.json.JSONException;

import fr.vitamines.annuaire.android.BuildConfig;
import fr.vitamines.annuaire.android.R;
import main.Sodexo;
import android.app.ActionBar;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;

public class AffichageTicket extends Fragment {

	private Sodexo compteUtilisateur = Accueil.userSodexo;
	private WebView affichageticket;
	
	public static AffichageTicket newInstance(String num){
		AffichageTicket affichage = new AffichageTicket();
		Bundle args = new Bundle();
	    args.putString("id", num);
	    affichage.setArguments(args);
		return affichage;
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
		View rootView  = inflater.inflate(R.layout.affichageticket, container, false);
		affichageticket=(WebView) rootView.findViewById(R.id.affichageticket);
		
		return rootView;
	}


	public void onActivityCreated(Bundle savedInstanceState){
		super.onActivityCreated(savedInstanceState);

		
		((Accueil) getActivity()).setTitle("Ticket");
		Bundle args = getArguments();
		String num = args.getString("id");
		String codehtml=null;
		try {
			codehtml=new ChargerTicket().execute(num).get();
		} catch (InterruptedException e) {
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		} catch (ExecutionException e) {
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		}
		affichageticket.loadDataWithBaseURL(null, codehtml, "text/html", "utf-8", null);

	}

	private class ChargerTicket extends AsyncTask<String,Void,String>{

		@Override
		protected String doInBackground(String... params) {
			String result=null;
			try {
				result= compteUtilisateur.getTicket(params[0]);
			} catch (ClientProtocolException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			} catch (IOException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			} catch (JSONException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			return result;
		}

	}
}
