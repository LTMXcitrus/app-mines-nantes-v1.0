package gui;

import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

import main.Mulot;
import main.Sodexo;
import tools.CreateSodexo;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import fr.vitamines.annuaire.android.BuildConfig;
import fr.vitamines.annuaire.android.R;

public class Accueil extends ActionBarActivity implements
NavigationDrawerFragment.NavigationDrawerCallbacks {

	/**
	 * Fragment managing the behaviors, interactions and presentation of the
	 * navigation drawer.
	 */
	private NavigationDrawerFragment mNavigationDrawerFragment;

	/**
	 * Used to know if we are on one of the directory's fragments.
	 */
	private boolean currentMenuIsDirectory = true;

	/**
	 * Used to store the last screen title. For use in
	 * {@link #restoreActionBar()}.
	 */
	private CharSequence mTitle;

	/**
	 * Used to make the difference between the two type of directories, Mulot
	 * and Alumni
	 */
	public static final int MULOT_SEARCH_TYPE = 0;
	public static final int ALUMNI_SEARCH_TYPE = 1;

	/**
	 * Used to know how to implement the back Button if the user has just logged
	 * in
	 */
	public static boolean hasJustLoggedIn = false;
	private boolean hasBackButtonBeenPressed = false;
	/**
	 * Utilisateur
	 */
	public static Mulot user;
	public static Sodexo userSodexo;
	/**
	 * Use to handle SharedPreferences
	 */
	private SharedPreferences preferences;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_accueil);
		preferences = PreferenceManager.getDefaultSharedPreferences(this);
		preferences.getString("sodexousername", null);
		preferences.getString("sodexomdp", null);

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);
		mTitle = getTitle();

		// Set up the drawer.
		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		if (!isLoggedIn()) {
			startActivity(new Intent(Accueil.this, Connexion.class));
		}
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		// update the main content by replacing fragments
		FragmentManager fragmentManager = getSupportFragmentManager();
		if (position == 1) {
			fragmentManager
			.beginTransaction()
			.replace(
					R.id.container,
					AnnuaireFragment.newInstance(MULOT_SEARCH_TYPE,
							"Mulot")).commit();
			currentMenuIsDirectory = true;
		}
		if (position == 2) {
			fragmentManager
			.beginTransaction()
			.replace(
					R.id.container,
					AnnuaireFragment.newInstance(ALUMNI_SEARCH_TYPE,
							"Alumni")).commit();
			currentMenuIsDirectory = true;
		}
		if (position == 3) {
			currentMenuIsDirectory = false;
			if(Connexion.isDeviceConnected(this)){
				//startActivity(new Intent(Accueil.this, Profil.class));
				fragmentManager.beginTransaction().replace(R.id.container, 
						ProfilFragment.newInstance(),
						"Profil").commit();
			}
			else{
				notConnectedDialog(this);
			}
		}
		if (position == 5) {
			if(Connexion.isDeviceConnected(this)){
				currentMenuIsDirectory = false;
				preferences = PreferenceManager.getDefaultSharedPreferences(this);

				if (preferences.getString("sodexousername", null) == null
						|| preferences.getString("sodexomdp", null) == null) {
					fragmentManager.beginTransaction()
					.replace(R.id.container, SodexoConnexion.newInstance())
					.commit();
				} else {
					if (userSodexo == null) {
						Dialog attente = chargementDialog(this);
						String identifiantsodexo = preferences.getString(
								"sodexousername", null);
						String sodexomdp = preferences.getString("sodexomdp", null);
						try {
							userSodexo = (new CreateSodexo(this)).execute(
									identifiantsodexo, sodexomdp).get();
							attente.dismiss();
							fragmentManager
							.beginTransaction()
							.replace(R.id.container,
									SodexoSoldeFragment.newInstance(false))
									.commit();
						} catch (InterruptedException | ExecutionException e) {
							if (BuildConfig.DEBUG) {
								System.out.println(e);
							}
						}
					} else {
						fragmentManager
						.beginTransaction()
						.replace(R.id.container,
								SodexoSoldeFragment.newInstance(false))
								.commit();
					}
				}
			}
			else{
				notConnectedDialog(this);
			}
		}
		if (position == 6) {
			if(Connexion.isDeviceConnected(this)){
				currentMenuIsDirectory = false;
				preferences = PreferenceManager.getDefaultSharedPreferences(this);

				if (preferences.getString("sodexousername", null) == null
						|| preferences.getString("sodexomdp", null) == null) {
					fragmentManager.beginTransaction()
					.replace(R.id.container, SodexoConnexion.newInstance())
					.commit();
				} else {
					if (userSodexo == null) {
						String identifiantsodexo = preferences.getString(
								"sodexousername", null);
						String sodexomdp = preferences.getString("sodexomdp", null);
						try {
							userSodexo = (new CreateSodexo(this)).execute(
									identifiantsodexo, sodexomdp).get();
							fragmentManager
							.beginTransaction()
							.replace(R.id.container,
									TicketListFragment.newInstance())
									.commit();
						} catch (InterruptedException | ExecutionException e) {
							if (BuildConfig.DEBUG) {
								System.out.println(e);
							}
						}
					} else {
						fragmentManager
						.beginTransaction()
						.replace(R.id.container,
								TicketListFragment.newInstance()).commit();
					}
				}
			}
			else{
				notConnectedDialog(this);
			}
		}
		if (position == 8) {
			currentMenuIsDirectory = false;
			fragmentManager.beginTransaction()
			.replace(R.id.container, APropos.newInstance()).commit();
		}
		if (position == 9) {
			currentMenuIsDirectory = false;
			deconnectionDialog();
		}
	}

	public void onSectionAttached(String title) {
		mTitle = title;
	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setTitle(mTitle);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (!mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.
			MenuInflater inflater = new MenuInflater(this);
			inflater.inflate(R.menu.accueil, menu);
			menu.findItem(R.id.rechercheavancee).setVisible(
					currentMenuIsDirectory);
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.rechercheavancee) {
			if (AnnuaireFragment.advancedSearchOn) {
				item.setIcon(R.drawable.plus_bleu);
				AnnuaireFragment.advancedSearchSpinner.setVisibility(View.GONE);
				AnnuaireFragment.field = "cn";
				AnnuaireFragment.advancedSearchOn = false;
			} else {
				item.setIcon(R.drawable.minus_bleu);
				AnnuaireFragment.advancedSearchSpinner
				.setVisibility(View.VISIBLE);
				AnnuaireFragment.advancedSearchOn = true;
			}
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	/**
	 * vérifie si l'utilisateur a rentré ses identifiants
	 * @return true si les identifiants sont enregistrés dans les préférences, false sinon
	 */
	public boolean isLoggedIn() {
		SharedPreferences preferences = PreferenceManager
				.getDefaultSharedPreferences(this);

		String identifiant = preferences.getString("identifiant", null);
		String weakpassword = preferences.getString("weakpassword", null);

		try {
			createMulot(identifiant, weakpassword);
		} catch (UnsupportedEncodingException e) {
			if (BuildConfig.DEBUG) {
				System.out.println(e);
			}
		}

		return identifiant != null && weakpassword != null;
	}

	/**
	 * crée l'utilisateur de l'annuaire (Mulot)
	 * @param identifiant
	 * @param weakpassword
	 * @throws UnsupportedEncodingException
	 */
	public void createMulot(String identifiant, String weakpassword)
			throws UnsupportedEncodingException {
		user = new Mulot(identifiant, weakpassword);
	}

	@Override
	public void onBackPressed() {
		if (hasJustLoggedIn) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			fragmentManager
			.beginTransaction()
			.replace(
					R.id.container,
					AnnuaireFragment.newInstance(MULOT_SEARCH_TYPE,
							"Mulot")).commit();
		} else {
			if (!hasBackButtonBeenPressed) {
				super.onBackPressed();
			} else {
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		}

	}

	/**
	 * Fenêtre de Dialogue de chargement
	 * @param context
	 * @return la fenêtre de dialogue
	 */
	public static Dialog chargementDialog(Context context) {
		Dialog dialoattente = new Dialog(context);
		LayoutInflater factory = LayoutInflater.from(context);
		final View dialochargement = factory.inflate(R.layout.chargement, null);
		dialoattente.setContentView(dialochargement);
		dialoattente.setTitle("Chargement...");
		return dialoattente;
	}

	/**
	 * supprime les identifiants enregistrés dans les préférences si l'utilisateur le souhaite
	 * @param mulot vaut true si l'utilisateur veut se déconnecter de l'annuaire, false sinon
	 * @param sodexo vaut true si l'utilisateur veut se déconnecter du compte Sodexo, false sinon
	 */
	public void removeIdentifiants(boolean mulot, boolean sodexo) {
		if (mulot) {
			preferences.edit().remove("identifiant").remove("weakpassword")
			.commit();
		}
		if (sodexo) {
			preferences.edit().remove("sodexousername").remove("sodexomdp")
			.commit();
		}
	}

	/**
	 * crée et affiche une boite de Dialogue permettant de se déconnecter de l'application
	 * il est possible de supprimer les identifiants enregistrés dans les préférences (via removeIdentifiants)
	 */
	public void deconnectionDialog() {
		Builder dialo = new AlertDialog.Builder(this);
		LayoutInflater factory = LayoutInflater.from(this);
		final View dialochargement = factory.inflate(
				R.layout.deconnection_dialog, null);
		final CheckBox mulot = (CheckBox) dialochargement
				.findViewById(R.id.deconnectmulot);
		final CheckBox sodexo = (CheckBox) dialochargement
				.findViewById(R.id.deconnectsodexo);
		dialo.setMessage("Voulez-vous vraiment vous déconnecter ?");
		dialo.setNegativeButton("Annuler",
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		dialo.setPositiveButton("Me déconnecter",
				new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeIdentifiants(mulot.isChecked(),
						sodexo.isChecked());
				Intent intent = new Intent(Intent.ACTION_MAIN);
				intent.addCategory(Intent.CATEGORY_HOME);
				intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
				startActivity(intent);
			}
		});
		dialo.setView(dialochargement);
		dialo.setTitle("Quitter");
		dialo.show();
	}

	/**
	 * Boite de dialogue qui apparait si l'appareil n'est pas connecté à internet, et qui propose de réessayer, dès que l'utilisateur a connecté l'appareil.
	 * @param context
	 */
	public void notConnectedDialog(Context context){
		Builder notConnected= new AlertDialog.Builder(context);
		notConnected.setCancelable(true);
		notConnected.setPositiveButton("Réessayer", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				recreate();				
			}
		});
		LayoutInflater factory = LayoutInflater.from(context);
		final View notconnectedDialog = factory.inflate(R.layout.device_not_connected_dialog, null);
		notConnected.setView(notconnectedDialog);
		notConnected.setTitle("Non connecté");
		notConnected.show();
	}

}
