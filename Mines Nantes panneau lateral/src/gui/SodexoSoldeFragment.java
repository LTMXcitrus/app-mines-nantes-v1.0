package gui;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import main.Sodexo;
import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.TextView;
import android.widget.ToggleButton;
import fr.vitamines.annuaire.android.BuildConfig;
import fr.vitamines.annuaire.android.R;

public class SodexoSoldeFragment extends Fragment{

	/**
	 * Used to know from where the user is from (SodexoConnexion or Accueil)
	 */
	private boolean fromConnexion;
	
	private String title="Solde";


	public static SodexoSoldeFragment newInstance(boolean fromConnexion) {
		SodexoSoldeFragment fragment = new SodexoSoldeFragment();
		fragment.fromConnexion=fromConnexion;
		return fragment;
	}

	public SodexoSoldeFragment() {	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.sodexoactivity,container, false);

		TextView utilisateur =(TextView) rootView.findViewById(R.id.utilisateur);
		TextView sodexosolde  = (TextView) rootView.findViewById(R.id.sodexosolde);
		ToggleButton impressiontickets = (ToggleButton) rootView.findViewById(R.id.impressiontickets);

		setInformation(utilisateur, sodexosolde, impressiontickets);
		return rootView;
	}

	public void setInformation(TextView utilisateur, TextView sodexosolde, ToggleButton impressiontickets){
		Sodexo user;
		if(fromConnexion){
			user=SodexoConnexion.compteUtilisateur;
		}
		else{
			user=Accueil.userSodexo;
		}
		double solde = user.getMontantRestant();
		String nomUtilisateur =user.getName();
		boolean impressionActive =false;
		try {
			impressionActive = (new GetPrintingStatus()).execute(user).get();
		} catch (InterruptedException | ExecutionException e) {
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		}
		utilisateur.setText(nomUtilisateur);
		utilisateur.setTextColor(getResources().getColor(R.color.bleu));
		sodexosolde.setText(String.format("%.2f",solde)+" €");
		sodexosolde.setTextSize(70);

		impressiontickets.setChecked(impressionActive);
		impressiontickets.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				new SetPrintingStatus().execute(isChecked);				
			}
		});
	}

	private class GetPrintingStatus extends AsyncTask<Sodexo, Void, Boolean>{
		@Override
		protected Boolean doInBackground(Sodexo... params) {
			Boolean result=null;
			try {
				result=params[0].isPrintingEnabled();
			} catch (IOException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			return result;
		}
	}

	private class SetPrintingStatus extends AsyncTask<Object, Void, Void>{
		@Override
		protected Void doInBackground(Object... params) {
			Sodexo user = (Sodexo) params[0];
			Boolean enabled = (Boolean) params[1];
			try {
				user.setPrintingEnabled(enabled);
			} catch (IOException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			return null;
		}

	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((Accueil) activity).onSectionAttached(title);
	}

}
