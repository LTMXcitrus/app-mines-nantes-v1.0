package gui;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.concurrent.ExecutionException;

import main.Mulot;
import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.ConnectivityManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import fr.vitamines.annuaire.android.BuildConfig;
import fr.vitamines.annuaire.android.R;

public class Connexion extends Activity{

	//widgets
	private EditText identifiantEntry;
	private EditText weakpasswordEntry;
	private Button valider;

	//OS elements
	private SharedPreferences preferences;

	//Variables
	private String identifiant;
	private String weakpassword;

	public void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		setContentView(R.layout.connexion);

		setActionBar();

		Accueil.hasJustLoggedIn=true;

		identifiantEntry = (EditText) findViewById(R.id.identifiant);
		weakpasswordEntry = (EditText) findViewById(R.id.password);
		valider = (Button) findViewById(R.id.valider);

		preferences = PreferenceManager.getDefaultSharedPreferences(this);

		valider.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				createMulot();
			}
		});
	}


	public void createMulot(){
		identifiant=identifiantEntry.getText().toString();
		weakpassword=weakpasswordEntry.getText().toString();

		Mulot user = null;
		try {
			user = new Mulot(identifiant,weakpassword);
		} catch (UnsupportedEncodingException e) {
			if(BuildConfig.DEBUG){
				System.out.println(e);
			}
		}
		preferences.edit().putString("identifiant", identifiant).putString("weakpassword", weakpassword ).commit();

		checkLogsAndConnection(user);

	}

	public void checkLogsAndConnection(Mulot user){
		if(isDeviceConnected(Connexion.this)){
			Connect connect = new Connect();
			boolean connected=false;
			try {
				connected = connect.execute(user).get();
			} catch (InterruptedException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			} catch (ExecutionException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			goToDirectoryIfConnected(connected);
		}
		else{
			notConnectedDialog(Connexion.this);
		}
	}
	public boolean isLoggedAndConnected(Mulot user){
		if(isDeviceConnected(Connexion.this)){
			Connect connect = new Connect();
			boolean connected=false;
			try {
				connected = connect.execute(user).get();
			} catch (InterruptedException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			} catch (ExecutionException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			return connected;
		}
		else{
			return false;
		}
	}

	public void setActionBar(){
		ActionBar actionBar = getActionBar();
		if(actionBar != null) actionBar.setTitle("Connexion");
	}

	private class Connect extends AsyncTask<Mulot,Void,Boolean>{

		protected Boolean doInBackground(Mulot... params) {
			Mulot mulot = params[0];
			boolean result = false;
			try {
				result= mulot.connect(mulot.getKey());
			} catch (IOException e) {
				if(BuildConfig.DEBUG){
					System.out.println(e);
				}
			}
			return result;
		}
	}
	/**
	 * Vérifie si l'appareil est connecté à internet
	 * @param context
	 * @return true si l'appareil a accès à internet, false sinon
	 */
	public static boolean isDeviceConnected(Context context) {
		ConnectivityManager cm = (ConnectivityManager)context.getSystemService(Context.CONNECTIVITY_SERVICE);
		return (cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isAvailable() && cm.getActiveNetworkInfo().isConnected());
	}

	public void goToDirectoryIfConnected(boolean connected){
		if(connected){
			startActivity(new Intent(Connexion.this,Accueil.class));
		}
		else{
			wrongLogsDialog(Connexion.this);
		}
	}

	public static void wrongLogsDialog(Context context){
		Builder dialo= new  AlertDialog.Builder(context);
		dialo.setCancelable(true);
		dialo.setTitle("Identifiant ou mot de passe erroné!");
		dialo.show();	
	}

	public void notConnectedDialog(Context context){
		Builder notConnected= new AlertDialog.Builder(context);
		notConnected.setCancelable(true);
		notConnected.setPositiveButton("Réessayer", new DialogInterface.OnClickListener(){
			@Override
			public void onClick(DialogInterface dialog,
					int which) {
				recreate();				
			}
		});
		LayoutInflater factory = LayoutInflater.from(context);
		final View notconnectedDialog = factory.inflate(R.layout.device_not_connected_dialog, null);
		notConnected.setView(notconnectedDialog);
		notConnected.setTitle("Non connecté");
		notConnected.show();
	}

	@Override
	public void onBackPressed(){
		Intent intent = new Intent(Intent.ACTION_MAIN);
		intent.addCategory(Intent.CATEGORY_HOME);
		intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		startActivity(intent);
	}

}
