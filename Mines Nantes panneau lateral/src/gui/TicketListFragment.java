package gui;

import java.io.IOException;

import main.Sodexo;

import org.apache.http.client.ClientProtocolException;

import tools.ArrayAdapterTicketSodexo;
import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.Mode;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshListView;

import fr.vitamines.annuaire.android.R;

public class TicketListFragment extends Fragment{

	ArrayAdapter mArrayAdapter;
	PullToRefreshListView mPullToRefreshListView;
	
	private String title = "Tickets de caisse";

	public static Fragment newInstance(){
		Fragment fragment = new TicketListFragment();
		return fragment;
	}


	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState){
		View rootView = inflater.inflate(R.layout.sodexolistetickets, container, false);


		final Sodexo compteUtilisateur= Accueil.userSodexo;
		
			
		
		if(compteUtilisateur!=null){
			mPullToRefreshListView = (PullToRefreshListView) rootView.findViewById(R.id.ticketlist);
			mArrayAdapter=new ArrayAdapterTicketSodexo(getActivity(), R.layout.itemticket, compteUtilisateur.getTickets());
			mPullToRefreshListView.setAdapter(mArrayAdapter);
			mPullToRefreshListView.setMode(Mode.PULL_FROM_END);
			mPullToRefreshListView.setShowIndicator(false);
			mPullToRefreshListView.setOnRefreshListener(new OnRefreshListener<ListView>() {
				@Override
				public void onRefresh(PullToRefreshBase<ListView> refreshView) {
					new RefreshList().execute(compteUtilisateur);
				}
			});
			mPullToRefreshListView.setOnItemClickListener(new OnItemClickListener() {

				@Override
				public void onItemClick(AdapterView<?> parent, View view,
						int position, long id) {
					FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
					fragmentManager.beginTransaction()
						.replace(R.id.container, AffichageTicket.newInstance(compteUtilisateur.getTickets()[position-1][0]), "Ticket")
						.commit();
					
//					String num = compteUtilisateur.getTickets()[position-1][0];
//					Bundle args = new Bundle();
//					args.putString("id", num);
//					Intent intent = new Intent(getActivity(),AffichageTicket.class);
//					intent.putExtras(args);
//					startActivity(intent);
				}
			});
		}
		return rootView;
	}
	
	private class RefreshList extends AsyncTask<Sodexo,Void,Sodexo>{

		protected Sodexo doInBackground(Sodexo... params) {
			try {
				params[0].getMoreTickets();
			} catch (ClientProtocolException e) {
				System.out.println(e);
			} catch (IOException e) {
				System.out.println(e);
			}

			return params[0];
		}
		@Override
		protected void onPostExecute(Sodexo result){
			super.onPostExecute(result);
			mArrayAdapter=new ArrayAdapterTicketSodexo(getActivity(),R.layout.itemticket,result.getTickets());
			mPullToRefreshListView.setAdapter(mArrayAdapter);
			mPullToRefreshListView.onRefreshComplete();
		}

	}
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		((Accueil) activity).onSectionAttached(title);
	}
}
