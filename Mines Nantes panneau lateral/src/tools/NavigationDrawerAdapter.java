package tools;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.vitamines.annuaire.android.R;

public class NavigationDrawerAdapter extends ArrayAdapter<String>{

	private Context context;
	private int layoutResourceId;
	private String[] objects;
	private int[] icons = {R.drawable.annuaire,R.drawable.find_user_bleu,R.drawable.find_user_bleu,R.drawable.profil_bleu,R.drawable.sodexo_bleu,
			R.drawable.solde,R.drawable.receipt,R.drawable.more,R.drawable.info,R.drawable.exit};
	private int selectedItem=1;

	public NavigationDrawerAdapter(Context context,
			int layoutResourceId, String[] objects) {
		super(context, layoutResourceId, objects);
		this.context=context;
		this.layoutResourceId=layoutResourceId;
		this.objects=objects;

	}

	public View getView(int position, View convertView, ViewGroup parent){
		if(position==0 || position==4 || position == 7){

			LayoutInflater inflater = ((Activity) this.context).getLayoutInflater();

			convertView = inflater.inflate(R.layout.navigation_drawer_section, parent, false);

			TextView title =(TextView) convertView.findViewById(R.id.navigation_drawer_section_title);
			//ImageView image=(ImageView) convertView.findViewById(R.id.navigation_section_image);
			//image.setImageResource(icons[position]);
			title.setText(objects[position]);
			convertView.setOnClickListener(null);
			convertView.setOnLongClickListener(null);
			convertView.setLongClickable(false);

			return convertView;	
		}
		else{
			LayoutInflater inflater = ((Activity) this.context).getLayoutInflater();

			convertView = inflater.inflate(layoutResourceId, parent, false);

			TextView title =(TextView) convertView.findViewById(R.id.navigation_drawer_row_title);
			ImageView image =(ImageView) convertView.findViewById(R.id.navigation_row_image);
			
			image.setImageResource(icons[position]);
			title.setText(objects[position]);
			
			if(position!=3 && position!=6){
				convertView.setBackgroundDrawable(context.getResources().getDrawable(R.drawable.drawer_row_background_pressed_unpressed));
			}
			if(position==selectedItem){
				convertView.setBackgroundColor(Color.LTGRAY);
			}
			return convertView;
		}
	}
	
	public int getSelectedItem(){
		return selectedItem;
	}
	public void setSelectedItem(int selectedItem){
		this.selectedItem=selectedItem;
	}
}
