package tools;

import gui.Accueil;

import java.io.IOException;

import main.Sodexo;

import org.apache.http.client.ClientProtocolException;

import android.app.Dialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.Toast;

public class CreateSodexo extends AsyncTask<String,Void,Sodexo>{
	
	private Context context;
	private Dialog dialoattente;
	
	public CreateSodexo(Context context){
		this.context=context;
	}
	
	protected void onPreExecute() {
		super.onPreExecute();
		dialoattente = Accueil.chargementDialog(context);
		dialoattente.show();

		Toast.makeText(context, "Chargement...", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected Sodexo doInBackground(String... params) {
		Sodexo result=null;
		try {
			result= new Sodexo(params[0],params[1]);
		} catch (ClientProtocolException e) {
			System.out.println(e);
		} catch (IOException e) {
			System.out.println(e);
		}
		return result;
	}
	@Override
	protected void onPostExecute(Sodexo f){
		dialoattente.dismiss();
	}
}