package tools;



import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import fr.vitamines.annuaire.android.R;

public class ArrayAdapterTicketSodexo extends ArrayAdapter<String[]> {

	Context mContext;
	int layoutResourceId;
	String[] data[] = null;
	private ImageView imagelieu;


	public ArrayAdapterTicketSodexo(Context mContext, int layoutResourceId, String[][] data){
		super(mContext, layoutResourceId, data);
		this.layoutResourceId = layoutResourceId;
		this.mContext = mContext;
		this.data = data;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		if(convertView==null){

			LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();

			convertView = inflater.inflate(layoutResourceId, parent, false);
		}

		

		TextView lieu = (TextView) convertView.findViewById(R.id.lieu);
		TextView date = (TextView) convertView.findViewById(R.id.date);
		TextView montant=(TextView) convertView.findViewById(R.id.montant);
		imagelieu=(ImageView) convertView.findViewById(R.id.imagelieu);
		
		String sLieu =data[position][2];
		String sDate = data[position][1];
		String sMontant = data[position][3];
		lieu.setTextSize(18);
		lieu.setText(sLieu);
		date.setTextColor(Color.BLUE);
		date.setTextSize(12);
		date.setText(sDate);
		montant.setTextSize(20);
		if(sMontant.charAt(0)=='+'){
			montant.setTextColor(Color.rgb(0, 143, 88));
		}
		else{
			montant.setTextColor(Color.RED);
		}
		montant.setText(sMontant);
		
		if(sLieu.charAt(0) =='C'){
			imagelieu.setBackgroundResource(R.drawable.hot_chocolate);
		}
		if(sLieu.charAt(0)=='P'){
			imagelieu.setBackgroundResource(R.drawable.card_inserting);
		}
		if(sLieu.charAt(0)=='S'){
			imagelieu.setBackgroundResource(R.drawable.restaurant);
		}
		if(sLieu.charAt(0)=='M'){
			imagelieu.setBackgroundResource(R.drawable.internet);
		}
		return convertView;

	}


}
