package tools;

import fr.vitamines.annuaire.android.R;
import gui.Accueil;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class AdvancedSearchSpinnerAdapter extends BaseAdapter{
	
	private Context context;
	private String[] objects;
	

	public AdvancedSearchSpinnerAdapter(Context context, int searchType) {
		this.context=context;
		if(searchType==Accueil.MULOT_SEARCH_TYPE){
			this.objects=context.getResources().getStringArray(R.array.searchtypeMulot);
		}
		else{
			this.objects=context.getResources().getStringArray(R.array.searchtypeAlumni);
		}
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
		LayoutInflater inflater = ((Activity) this.context).getLayoutInflater();
		convertView = inflater.inflate(R.layout.spinner_item_custom, parent, false);
		TextView spinnerOptionText = (TextView) convertView.findViewById(R.id.spinner_option_text);
		spinnerOptionText.setText(this.objects[position]);
		return convertView;
	}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return this.objects.length;
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return position;
	}

}
