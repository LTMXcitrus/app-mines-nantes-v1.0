package main;

public class Personne {

	/*
	 * Chaque variable d'instance correspond à une information commune aux
	 * profils Mulot et Alumni, et porte le même nom. Les infos propres à chaque
	 * annuaire sont conservées au sein des classes héritées PersonneAlumni et
	 * PersonneMulot.
	 */

	/** Identifiant unique de chaque élève, enseignant, alumni, etc... */
	private int uid;
	/** Complete name - Nom complet */
	private String cn;
	/** Surname - Nom de famille */
	private String sn;
	/** Prénom seulement */
	private String givenName;
	/** Numéro fixe */
	private String homePhone;
	/** Numéro de portable */
	private String mobile;
	/** Adresse email */
	private String mail;
	/** Numéro d'appartement, de maison */
	private String chez;
	/** Premier champ d'adresse */
	private String ADR;
	/** Second champ d'adresse */
	private String ADR2;
	/** Troisième champ d'adresse */
	private String ADR3;
	/** Code postal */
	private String CPO;
	/** Ville */
	private String VIL;
	/** État, inutilisé pour les Français */
	private String state;
	/** Pays */
	private String PAY;
	/** Monsieur, Mademoiselle, Madame, etc... */
	private String personalTitle;
	/** Entreprise pour les Alumni, "emn" pour les gens de l'école */
	private String o;
	/** Département dans l'entreprise pour les Alumni, Entité ("élève", "sic", etc...) */
	private String ou;
	/** Fonction dans l'entreprise pour les Alumni, à l'école pour le personnel, "" pour les élèves */
	private String organizationalStatus;
	/** Numéro de téléphone professionnel, uniquement pour les Alumni et les employés de l'école */
	private String telephoneNumber ;
	/** variable "a-login", le login de la personne ("pmesur13", par exemple) */
	private String a_login;
	/** variable "a-homepage", pas présente dans le JSON si pas remplie sur le Mulot */
	private String a_homepage;
	/** variable "a-nickname", surnom de l'alumni */
	private String a_nickname ;
	/** variable "a-gender", Monsieur, Madame, mais à choisir parmi une liste */
	private String a_gender ;
	
	public Personne(int uid) {
		this.uid = uid;
	}

	public Personne(String cn, int uid) {
		this(uid);
		this.cn = cn;
	}

	public Personne() {
		this(10323);
	}

	/**
	 * @return the uid
	 */
	public int getUid() {
		return uid;
	}

	/**
	 * @param uid
	 *            the uid to set
	 */
	public void setUid(int uid) {
		this.uid = uid;
	}

	/**
	 * @return the cn
	 */
	public String getCn() {
		return cn;
	}

	/**
	 * @param cn
	 *            the cn to set
	 */
	public void setCn(String cn) {
		this.cn = cn;
	}

	/**
	 * @return the sn
	 */
	public String getSn() {
		return sn;
	}

	/**
	 * @param sn
	 *            the sn to set
	 */
	public void setSn(String sn) {
		this.sn = sn;
	}

	/**
	 * @return the givenName
	 */
	public String getGivenName() {
		return givenName;
	}

	/**
	 * @param givenName the givenName to set
	 */
	public void setGivenName(String givenName) {
		this.givenName = givenName;
	}

	/**
	 * @return the homePhone
	 */
	public String getHomephone() {
		return homePhone;
	}

	/**
	 * @param homePhone the homePhone to set
	 */
	public void setHomephone(String homePhone) {
		this.homePhone = homePhone;
	}

	/**
	 * @return the mobile
	 */
	public String getMobile() {
		return mobile;
	}

	/**
	 * @param mobile
	 *            the mobile to set
	 */
	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	/**
	 * @return the mail
	 */
	public String getMail() {
		return mail;
	}

	/**
	 * @param mail
	 *            the mail to set
	 */
	public void setMail(String mail) {
		this.mail = mail;
	}

	/**
	 * @return the chez
	 */
	public String getChez() {
		return chez;
	}

	/**
	 * @param chez
	 *            the chez to set
	 */
	public void setChez(String chez) {
		this.chez = chez;
	}

	/**
	 * @return the aDR
	 */
	public String getADR() {
		return ADR;
	}

	/**
	 * @param aDR
	 *            the aDR to set
	 */
	public void setADR(String aDR) {
		ADR = aDR;
	}

	/**
	 * @return the aDR2
	 */
	public String getADR2() {
		return ADR2;
	}

	/**
	 * @param aDR2
	 *            the aDR2 to set
	 */
	public void setADR2(String aDR2) {
		ADR2 = aDR2;
	}

	/**
	 * @return the aDR3
	 */
	public String getADR3() {
		return ADR3;
	}

	/**
	 * @param aDR3
	 *            the aDR3 to set
	 */
	public void setADR3(String aDR3) {
		ADR3 = aDR3;
	}

	/**
	 * @return the cPO
	 */
	public String getCPO() {
		return CPO;
	}

	/**
	 * @param cPO
	 *            the cPO to set
	 */
	public void setCPO(String cPO) {
		CPO = cPO;
	}

	/**
	 * @return the vIL
	 */
	public String getVIL() {
		return VIL;
	}

	/**
	 * @param vIL
	 *            the vIL to set
	 */
	public void setVIL(String vIL) {
		VIL = vIL;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state
	 *            the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the pAY
	 */
	public String getPAY() {
		return PAY;
	}

	/**
	 * @param pAY
	 *            the pAY to set
	 */
	public void setPAY(String pAY) {
		PAY = pAY;
	}

	/**
	 * @return the personalTitle
	 */
	public String getPersonalTitle() {
		return personalTitle;
	}

	/**
	 * @param personalTitle
	 *            the personalTitle to set
	 */
	public void setPersonalTitle(String personalTitle) {
		this.personalTitle = personalTitle;
	}

	/**
	 * @return the o
	 */
	public String getO() {
		return o;
	}

	/**
	 * @param o
	 *            the o to set
	 */
	public void setO(String o) {
		this.o = o;
	}

	/**
	 * @return the ou
	 */
	public String getOu() {
		return ou;
	}

	/**
	 * @param ou
	 *            the ou to set
	 */
	public void setOu(String ou) {
		this.ou = ou;
	}

	/**
	 * @return the organizationalStatus
	 */
	public String getOrganizationalStatus() {
		return organizationalStatus;
	}

	/**
	 * @param organizationalStatus
	 *            the organizationalStatus to set
	 */
	public void setOrganizationalStatus(String organizationalStatus) {
		this.organizationalStatus = organizationalStatus;
	}

	/**
	 * @return the telephoneNumber
	 */
	public String getTelephoneNumber() {
		return telephoneNumber;
	}

	/**
	 * @param telephonenumber the telephonenumber to set
	 */
	public void setTelephoneNumber(String telephoneNumber) {
		this.telephoneNumber = telephoneNumber;
	}
	
	/**
	 * @return the a_login
	 */
	public String getA_login() {
		return a_login;
	}

	/**
	 * @param a_login
	 *            the a_login to set
	 */
	public void setA_login(String a_login) {
		this.a_login = a_login;
	}

	/**
	 * @return the a_homepage
	 */
	public String getA_homepage() {
		return a_homepage;
	}

	/**
	 * @param a_homepage
	 *            the a_homepage to set
	 */
	public void setA_homepage(String a_homepage) {
		this.a_homepage = a_homepage;
	}

	/**
	 * @return the a_nickname
	 */
	public String getA_nickname() {
		return a_nickname;
	}

	/**
	 * @param a_nickname the a_nickname to set
	 */
	public void setA_nickname(String a_nickname) {
		this.a_nickname = a_nickname;
	}
	
	/**
	 * @return the a_gender
	 */
	public String getA_gender() {
		return a_gender;
	}

	/**
	 * @param a_gender the a_gender to set
	 */
	public void setA_gender(String a_gender) {
		this.a_gender = a_gender;
	}
	
	/**
	 * @return un profil détaillé de la personne
	 */
	public String getEverything() {
		return "\n	== Profil de " + this.getCn() + " ==	\n"
			+ "| uid = " + this.getUid() + "\n"
			+ "| cn = " + this.getCn() + "\n"
			+ "| sn = " + this.getSn() + "\n"
			+ "| homePhone = " + this.getHomephone() + "\n"
			+ "| mobile = " + this.getMobile() + "\n"
			+ "| mail = " + this.getMail() + "\n"
			+ "| chez = " + this.getChez() + "\n"
			+ "| ADR = " + this.getADR() + "\n"
			+ "| ADR2 = " + this.getADR2() + "\n"
			+ "| ADR3 = " + this.getADR3() + "\n"
			+ "| CPO = " + this.getCPO() + "\n"
			+ "| VIL = " + this.getVIL() + "\n"
			+ "| state = " + this.getState() + "\n"
			+ "| PAY = " + this.getPAY() + "\n"
			+ "| personalTitle = " + this.getPersonalTitle() + "\n"
			+ "| o = " + this.getO() + "\n"
			+ "| ou = " + this.getOu() + "\n"
			+ "| organizationalStatus = " + this.getOrganizationalStatus() + "\n"
			+ "| telephonenumber = " + this.getTelephoneNumber() + "\n"
			+ "| a-login = " + this.getA_login() + "\n"
			+ "| a-homepage = " + this.getA_homepage() + "\n"
			+ "| a-nickname = " + this.getA_nickname() + "\n"
			+ "| a-gender = " + this.getA_gender() ;
			
	}

	public String toString() {
		String s = "";

		s += "Je m'appelle " + this.cn + ", code " + this.uid
				+ ", mon numéro est le " + this.mobile + " et mon mail est "
				+ this.mail + ".";

		return s;
	}
}
