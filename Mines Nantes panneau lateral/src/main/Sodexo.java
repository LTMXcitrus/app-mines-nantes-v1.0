package main;

import java.io.IOException ;
import java.util.Arrays ;

import org.apache.http.HttpEntity ;
import org.apache.http.HttpResponse ;
import org.apache.http.ParseException ;
import org.apache.http.client.ClientProtocolException ;
import org.apache.http.client.CookieStore ;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet ;
import org.apache.http.client.methods.HttpPost ;
import org.apache.http.entity.StringEntity ;
import org.apache.http.impl.client.AbstractHttpClient;
import org.apache.http.impl.client.BasicCookieStore ;
import org.apache.http.impl.client.DefaultHttpClient ;
import org.apache.http.util.EntityUtils ;
import org.json.JSONException ;
import org.json.JSONObject ;

public class Sodexo {

	/** Pseudo de l'utilisateur, probablement toujours NOM + Initiale du prénom */
	private String user ;
	/** Mot de passe */
	private String password ;
	/** Nom de l'utilisateur, à afficher sur l'écran d'accueil, afin d'éviter que quelqu'un se connecte sur le téléphone d'un autre et oublie de se déconnecter */
	private String name ;
	/** Montant restant sur la carte, en double (pour manipuler) */
	private double montantRestant ;
	/** Tableau contenant toutes les infos sur les tickets de caisse, rangées de la manière suivante : <br/><br/>
	 * 	
	 * - tickets[i][0] contient les numéros permettant de récupérer le code HTML avec getTicket(String id) du ticket n°i <br/>
	 * - tickets[i][1] contient la date d'émission du ticket n°i<br/>
	 * - tickets[i][2] contient le type/lieu de la transaction (Self, Cafeteria, Poste de gestion, etc...) du ticket n°i<br/>
	 * - tickets[i][3] contient le montant de la transaction (ex: -3,28€ ou +40,00€) du ticket n°i <br/><br/>
	 *  En outre, le tableau est automatiquement agrandi pour accueillir les nouveaux éléments chargés avec getMoreTickets.
	 */
	private String[][] tickets ;
	/** Cookies récupérés lors de la connexion, et nécessaires aux requêtes ultérieures */
	private CookieStore cookies ;
	
	/**
	 * Constructeur vide, ouvrant une session avec mes identifiants.
	 */
	public Sodexo() throws ClientProtocolException, ParseException, IOException {
		this("MESUREP", "5I2CPB2E") ;
	}

	/**
	 * Constructeur ouvrant un sessions sur MONEWEB avec les identifiants en paramètre. En outre, il récupère le nom de la personne,
	 * le montant restant dans le porte-monnaie virtuel, et toutes les informations nécessaires sur les tickets pour en afficher une liste.
	 */
	public Sodexo(String user, String password) throws ClientProtocolException, IOException{
		this.user = user ;
		this.password = password ;

		cookies = new BasicCookieStore() ;
		
		String content = "" ;
		String url = "https://sodexo-mines-nantes.moneweb.fr/default.aspx";
		
		HttpClient client = new DefaultHttpClient();
		((AbstractHttpClient) client).setCookieStore(cookies);
		
		HttpPost post = new HttpPost(url);

		StringEntity s = new StringEntity("__LASTFOCUS=&__EVENTTARGET=login%24ctl00%24btnConnexion&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMjA0NDc5MjAyMg9kFgICAQ8WAh4Kb25rZXlwcmVzcwVOamF2YXNjcmlwdDpyZXR1cm4gV2ViRm9ybV9GaXJlRGVmYXVsdEJ1dHRvbihldmVudCwgJ2xvZ2luX2N0bDAwX2J0bkNvbm5leGlvbicpFgJmDxYCHgVjbGFzcwUQZGl2Q29tbXVuaWNhdGlvbhYEAgEPFgIeB1Zpc2libGVoFgJmDxBkEBUBF0Vjb2xlcyBkZXMgTWluZXMgTmFudGVzFQEBMRQrAwFnFgFmZAIDDxYCHglpbm5lcmh0bWwFIjxkaXYgY2xhc3M9ImNsZWFyX2JvdGgiPg0KDQo8L2Rpdj5kGAEFHl9fQ29udHJvbHNSZXF1aXJlUG9zdEJhY2tLZXlfXxYBBRhsb2dpbiRjdGwwMCRjYlJlbWVtYmVyTWWwoe%2Faub2lpgapN24XkZJNhJXawA%3D%3D&__EVENTVALIDATION=%2FwEWBQLD0IGuAgKkvM0FAsP7tokJAtem9osIArDnr94Kp3DwkboI8de%2BKnREqhTRAEEgf%2FU%3D&login%24ctl00%24tbLogin=" + user + "&login%24ctl00%24tbPassword=" + password + "&login%24ctl00%24cbRememberMe=on") ;
		s.setContentType("application/x-www-form-urlencoded");
		post.setEntity(s) ;
		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		respEntity.consumeContent();
		
		HttpGet get = new HttpGet("https://sodexo-mines-nantes.moneweb.fr/convive/") ;
		
		response = client.execute(get) ;
		
		respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}
		
		
		tickets = new String[15][4] ;

		if (content.contains("Bienvenue")) {
			name = content.substring(content.indexOf("Bienvenue") + 57, content.indexOf("Mon compte") - 145) ;

			String mrs = content.substring(content.indexOf("colDataChiffre") + 14) ;
			mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
			mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
			mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
			mrs = mrs.substring(18, mrs.indexOf("ligneCliquable") - 42) ;
			mrs = mrs.replace(',', '.') ;

			if (mrs.equals("-")) {
				mrs = content.substring(content.indexOf("colDataChiffre") + 14) ;
				mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
				mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 32) ;
				String vString = mrs.substring(0, mrs.indexOf("</td>") - 16);
				vString = vString.replace(',', '.') ;
				double versement = Double.parseDouble(vString) ;
				mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
				mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
				mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
				mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 14) ;
				mrs = mrs.substring(mrs.indexOf("colDataChiffre") + 32) ;
				mrs = mrs.substring(0, mrs.indexOf("ligneCliquable") - 42) ;
				mrs = mrs.replace(',', '.') ;
				double initial = Double.parseDouble(mrs) ;
				
				this.montantRestant = initial + versement ;
				
			}
			else {
				this.montantRestant = Double.parseDouble(mrs) ;
			}

			String rnl = content, rdl, rll, rml ;
			int i = 0 ;

			while (rnl.contains("GetTicket('")) {
				
				tickets[i] = new String[4] ;
				
				rnl = rnl.substring(rnl.indexOf("GetTicket('") + 11) ;
				rdl = rnl.substring(rnl.indexOf("colDataDate\">") + 13) ;
				rll = rdl.substring(rdl.indexOf("colDataLibelle\">") + 16) ;
				rml = rll.substring(rll.indexOf("colDataChiffre\">") + 16) ;
				rml = "-" + rml.substring(rml.indexOf("colDataChiffre\">") + 32) ;

				tickets[i][0] = rnl.substring(0, rnl.indexOf("');\">)") + 17) ;
				tickets[i][1] = rdl.substring(0, rdl.indexOf("</td>")) ;
				tickets[i][2] = rll.substring(0, rll.indexOf("</td>")) ;

				if (rml.substring(0, rml.indexOf("</td>") - 14).equals("-0,00 €")) {
					rml = "+" + rml.substring(rml.indexOf("colDataChiffre\">") + 32) ;
				}

				tickets[i][3] = rml.substring(0, rml.indexOf("</td>") - 14) ;
				tickets[i][3] = tickets[i][3].replace("--", "+") ;
				

				i++ ;
			}
			
			if (i % 15 != 0) {
				tickets = Arrays.copyOf(tickets, i) ;
				
			}
		}
		else {
			System.out.println("Échec de la connexion.") ;
			montantRestant = -3.14 ;
			name = "echec" ;
		}
	}

	/**
	 * Accesseur qui retourne le pseudo de la personne connectée.
	 * @return le pseudo de la personne connectée.
	 */
	public String getUser(){
		return this.user;
	}

	/**
	 * Accesseur qui retourne le mot de passe de la personne connectée.
	 * @return le mot de passe de la personne connectée.
	 */
	public String getPassword(){
		return this.password;
	}
	
	/**
	 * Accesseur qui retourne le nom de la personne connectée.
	 * @return le nom de la personne connectée.
	 */
	public String getName() {
		return name ;
	}

	/**
	 * Accesseur qui retourne le montant restant sur la carte.
	 * @return le montant restant sur la carte.
	 */
	public double getMontantRestant() {
		return montantRestant ;
	}

	/**
	 * Accesseur qui renvoie un tableau contenant toutes les infos sur les tickets de caisse, rangées de la manière suivante : <br/><br/>
	 * 	
	 * - tickets[0] contient les numéros permettant de récupérer le code HTML avec getTicket(String id) <br/>
	 * - tickets[1] contient la date d'émission<br/>
	 * - tickets[2] contient le type/lieu de la transaction (Self, Cafeteria, Poste de gestion, etc...) <br/>
	 * - tickets[3] contient le montant de la transaction (ex: -3,28€ ou +40,00€) <br/><br/>
	 *  En outre, le tableau est automatiquement agrandi pour accueillir les nouveaux éléments chargés avec getMoreTickets. Chaque
	 *  appel de cette fonction l'augmente de 15, sauf le dernier (lorsqu'il n'y a plus d'autres tickets). On peut donc tester la taille
	 *  du tableau (%15) pour vérifier qu'il n'y a plus d'autres tickets.
	 */
	public String[][] getTickets() {
		return tickets ;
	}

	/**
	 * Méthode qui charge dans le tableau tickets quinze tickets supplémentaires (cf. getTickets() pour savoir sous quelle forme).
	 * MONEWEB ne garde probablement que 100 tickets, donc cette méthode peut ne charger que 10 tickets. Dans ce cas, il ne sert à rien de l'utiliser
	 * à nouveau.
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public void getMoreTickets() throws ClientProtocolException, IOException {
		
		if (tickets.length % 15 != 0) {
			return ;
		}
		
		int page = tickets.length / 15 ;
		
		tickets = Arrays.copyOf(tickets, (page + 1) * 15) ;
		
		String content = "" ;
		String url = "https://sodexo-mines-nantes.moneweb.fr/convive/default.aspx";

		HttpClient client = new DefaultHttpClient() ;
		((AbstractHttpClient) client).setCookieStore(cookies);
		
		HttpPost post = new HttpPost(url);
		
		StringEntity s = new StringEntity("__EVENTTARGET=m%24cphMain%24ctl00%24gvHistoTick%24ctl18%24ImgBtnNext&__EVENTARGUMENT=&__LASTFOCUS=&__VIEWSTATE=fINFu1v6bAVxOu9ENpQZspsL2JHCh4HAl5Tmb3FgXE%2BStKb53hP4duwhAgqkTDLC6UEAXv%2FUacye2%2BMOkwVMi5wZQg2VFJqHScHtGIwXZGJs9rep0Xld8KJxU%2FtToArZyPt56Bk2kuPNlk9b2ZQG0VBpTs7GWHt6ptvZZs%2BAPep6BdyxnKnPfRu6Yk7Jzfz3ZXF%2F%2F%2BUvO8s%2BCPzdc9vAbVHHe388UhssjjuhBC2EUyj1t8rHdFuKf6ToIMAPa1ni%2Bc4jD3KazmNOWXQqU0v%2FsvadiZdtWYg4d4JCunP9hZ%2B7kRQQ4u%2BwoZFe2CSIMWs9Htz7edoX0%2Fovowg0pzBeUqhwBhqecHc2%2F4X90AYF7pd%2BHIBM%2F623Dnpn3K47d2ZWua0H5VN4ya3jKl%2BncSD5JVXyJ5GZOx6HKvNnzVl6GIEIAfnxohU7yxfhtLDrLuTJa3PHSQ5N%2FoQj4DS8neJT5jyrCA%2BCqiJARO%2BgnXC3rqrimfC%2Fke8N8QDfcgqUIRhtillSDn%2F%2Bw1DwqV3btT8%2BwtRVJaec1o0x92uyzmL7etpAM5hsDqsm3lrrCcA5%2BAvQ%2FZJLZNu0nrYOVdQwSu%2BQJHelqQ7Bmyd7XiqtwT24j7OIwbm7MWx0aA%2BgNwklHDtILHsFGupiud83pynCEQ9p1%2FKaIP2ettg6BWLMjgzh9QSkWF7fWFpmskcwUNCWMGZSLde2neIWGoDKQenN99F8ihefxtIDtwkfCYI0No%2BI1Xd0FSvBEX70HXdVUqKXlIvrJucqFcWkVCIThyBlJktGY7IRmsAuweYfeEuLJfwZK9DslGUY&__VIEWSTATEENCRYPTED=&__EVENTVALIDATION=RWW%2FADpNa%2FVLfMoMziO9EYl4d80dV2VcF%2FFW%2BbwyvhNdsqqgaYXxwVgkZfBZ5k882rDrAjbwoGxFSVQbT5CULrlN%2Fp5LL2iiCQNlUCwA03rmLL6AtSugcri1ZLKApxqclkw6vSnjChXFc47nZPSA9ZavefMQS7WyN02a%2Fzb03gk0K4B29ZTQ%2FyaUpAwvAfpef8PDIA%3D%3D&m%24cphMain%24ctl00%24gvHistoTick%24ctl18%24ddlPages=" + page) ;
		s.setContentType("application/x-www-form-urlencoded");
		post.setEntity(s) ;
		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}
		
		if (content.contains("Bienvenue")) {

			String rnl = content, rdl, rll, rml ;
			int i = page * 15 ;

			while (rnl.contains("GetTicket('")) {
				
				tickets[i] = new String[4] ;
				
				rnl = rnl.substring(rnl.indexOf("GetTicket('") + 11) ;
				rdl = rnl.substring(rnl.indexOf("colDataDate\">") + 13) ;
				rll = rdl.substring(rdl.indexOf("colDataLibelle\">") + 16) ;
				rml = rll.substring(rll.indexOf("colDataChiffre\">") + 16) ;
				rml = "-" + rml.substring(rml.indexOf("colDataChiffre\">") + 32) ;

				tickets[i][0] = rnl.substring(0, rnl.indexOf("');\">)") + 17) ;
				tickets[i][1] = rdl.substring(0, rdl.indexOf("</td>")) ;
				tickets[i][2] = rll.substring(0, rll.indexOf("</td>")) ;

				if (rml.substring(0, rml.indexOf("</td>") - 14).equals("-0,00 €")) {
					rml = "+" + rml.substring(rml.indexOf("colDataChiffre\">") + 32) ;
				}

				tickets[i][3] = rml.substring(0, rml.indexOf("</td>") - 14) ;
				tickets[i][3] = tickets[i][3].replace("--", "+") ;
				
				i++ ;
			}
			
			if (i % 15 != 0) {
				tickets = Arrays.copyOf(tickets, i) ;
			}
			
		}
	}

	/**
	 * Méthode qui retourne le code HTML d'un ticket à partir de son numéro unique. Le code est préformaté de manière à ressembler le
	 * plus possible à un ticket papier. Pour personnaliser l'image d'arrière-plan, il faut modifier les fichiers image receipt.png,
	 * receipt_top.png et receipt.bottom.png.
	 * @param id est le numéro unique du ticket.
	 * @return le code HTML du ticket.
	 * @throws ClientProtocolException
	 * @throws IOException
	 * @throws JSONException
	 */
	public String getTicket(String id) throws ClientProtocolException, IOException, JSONException {
		
		String content = "" ;
		String url = "https://sodexo-mines-nantes.moneweb.fr/convive/default.aspx/GetTicket";

		HttpClient client = new DefaultHttpClient() ;
		((AbstractHttpClient) client).setCookieStore(cookies);
		
		HttpPost post = new HttpPost(url);
		
		StringEntity s = new StringEntity("{\"idTicketStr\":\"" + id + "\"}") ;
		s.setContentType("application/json;charset=utf-8");
		post.setEntity(s) ;
		
		HttpResponse response = client.execute(post);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}
		
		if (content != null && content.substring(0, 1).equals("{")) {
			
			if ((new JSONObject(content)).get("d") != null) {
				String ticket = new String(new JSONObject(content).getString("d")) ;
				ticket = ticket.replace("<?xml version=\"1.0\" encoding=\"utf-16\"?>", "") ;
				ticket = ticket.replace(">x<", "><") ;
				ticket = ticket.replace("170", "120") ;
				ticket = ticket.substring(0, ticket.indexOf("</tr><tr><td colspan=\"2\"><br /></td><td /></tr></table>")) ;
				ticket = ticket.concat("</table>") ;
				
				ticket =   "<html>"
			             + "\n<head>"
			             + "\n<meta name=\"viewport\" content=\"width=device-width; minimum-scale=1.0; maximum-scale=1.0; user-scalable=no\">"
			             + "\n<style>"
			             + "\ntable {"
			             +    "\nfont-family: Courier New;"
			             +    "\nfont-size: smaller ;"
			             +    "\nbackground-image: url(receipt_top.png), url(receipt_bottom.png), url(receipt_middle.png);"
			             +    "\nbackground-size: contain;"
			             +    "\nbackground-position: left top, left bottom, left center;"
			             +    "\nbackground-repeat: no-repeat, no-repeat, repeat-y;"
			             +    "\npadding: 15px;"
			             +    "\nposition: center;"
			             + "\n}"
			             + "\n</style>"
			             + "\n</head>"
			             + "\n<body>"
			             + "\n"
			             + ticket
			             + "\n</body>"
			             + "\n</html>" ;
				
				return ticket ;
			}
		}
		System.out.println("Échec de la récupération du ticket.") ;
		return id ;
	}
	
	/**
	 * Méthode qui teste si l'impression des tickets est activée ou non dans les préférences de l'utilisateur.
	 * Permet d'afficher cet état via une CheckBox ou autre.
	 * @return un booléen (true si l'impression est activée, false si elle est désactivée).
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public boolean isPrintingEnabled() throws ClientProtocolException, IOException {
		String content = "" ;
		String url = "https://sodexo-mines-nantes.moneweb.fr/Convive/profil.aspx";

		HttpClient client = new DefaultHttpClient() ;
		((AbstractHttpClient) client).setCookieStore(cookies);
		
		HttpGet get = new HttpGet(url);
		
		HttpResponse response = client.execute(get);
		HttpEntity respEntity = response.getEntity();
		if (respEntity != null) {
			content =  EntityUtils.toString(respEntity);
		}

		return content.contains("checked") ;
	}
	
	/**
	 * Méthode qui permet d'activer ou désactiver l'impression des tickets.
	 * @param enabled est un booléen (true si l'impression est activée, false si elle est désactivée).
	 * @throws ClientProtocolException
	 * @throws IOException
	 */
	public void setPrintingEnabled(boolean enabled) throws ClientProtocolException, IOException {

		String url = "https://sodexo-mines-nantes.moneweb.fr/Convive/profil.aspx";

		HttpClient client = new DefaultHttpClient() ;
		((AbstractHttpClient) client).setCookieStore(cookies);
		
		HttpPost post = new HttpPost(url);
		
		String body = new String("__EVENTTARGET=m%24cphMain%24ctl00%24btnEnregistrer&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUJNDM2MzEzNzExD2QWAmYPZBYCAgEPFgIeCm9ua2V5cHJlc3MFVGphdmFzY3JpcHQ6cmV0dXJuIFdlYkZvcm1fRmlyZURlZmF1bHRCdXR0b24oZXZlbnQsICdtX2NwaE1haW5fY3RsMDBfYnRuRW5yZWdpc3RyZXInKRYCAgQPZBYCAgEPZBYEAgEPZBYCAgEPFgIeB1Zpc2libGVoFgICAQ9kFgxmDxAPFgIeB0VuYWJsZWRoZBAVARdFY29sZXMgZGVzIE1pbmVzIE5hbnRlcxUBATEUKwMBZxYBZmQCAQ8QDxYEHgdDaGVja2VkaB8CaGRkZGQCAg8QDxYEHwNoHwJoZGRkZAIDDxAPFgQfA2gfAmhkZGRkAgQPEA8WBB8DaB8CaGRkZGQCBQ8QDxYEHwNoHwJoZGRkZAIDD2QWAgIDDw8WAh4RVXNlU3VibWl0QmVoYXZpb3JoZGQYAQUeX19Db250cm9sc1JlcXVpcmVQb3N0QmFja0tleV9fFgIFIm0kY3BoTWFpbiRjdGwwMCRjdGwwMiRjYkVkaXRUaWNrZXQFK20kY3BoTWFpbiRjdGwwMCRjdGwwMiRjYkdlc3Rpb25GaWRlbGlzYXRpb25RlbyyPutQuUU6QJo%2F8JSj2rzW7w%3D%3D&__EVENTVALIDATION=%2FwEWBgKxv8bCBgLl84bjCwKFwLnSCQLz5%2Bj3CQLr4oTWDgK1lbiLA%2BLtvfyPLhwKn%2BXdr2VIMSJA2x1n") ;
		
		if (enabled) {
			body += "&m%24cphMain%24ctl00%24ctl02%24cbEditTicket=on" ;
		}

		StringEntity s = new StringEntity(body) ;		
		s.setContentType("application/x-www-form-urlencoded");
		post.setEntity(s) ;
		
		client.execute(post);
		
	}
}