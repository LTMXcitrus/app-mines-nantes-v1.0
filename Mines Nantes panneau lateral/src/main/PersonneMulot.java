package main;

public class PersonneMulot extends Personne {
	
	
	/** Adresse de la photo */
	private String photoURL ;
	/** Promo (variable "a-promo") */
	private String a_promo ;
	/** Lieu de travail */
	private String l ;
	/** "eleve" pour les étudiants, "personnel" et d'autres trucs ("cdd" ?) */
	private String employeeType ;
	/** Numéro de bureau, surtout pour le personnel */
	private String roomNumber ;
	/** Mail personnel (pas celui de l'école), le personnel n'en a apparemment pas. */
	private String a_mailexterne ;
	/** Préférences du Mulot */
	private String[] a_select ;

	public PersonneMulot(int uid) {
		super(uid);
	}

	public PersonneMulot() {
		super.setCn("echec");
	}
	/**
	 * @return the photoURL
	 */
	public String getPhotoURL() {
		return photoURL;
	}


	/**
	 * @param photoURL the photoURL to set
	 */
	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}


	/**
	 * @return the a_promo
	 */
	public String getA_promo() {
		return a_promo;
	}


	/**
	 * @param a_promo the a_promo to set
	 */
	public void setA_promo(String a_promo) {
		this.a_promo = a_promo;
	}


	/**
	 * @return the l
	 */
	public String getL() {
		return l;
	}

	/**
	 * @param l the l to set
	 */
	public void setL(String l) {
		this.l = l;
	}

	/**
	 * @return the employeeType
	 */
	public String getEmployeeType() {
		return employeeType;
	}


	/**
	 * @param employeeType the employeeType to set
	 */
	public void setEmployeeType(String employeeType) {
		this.employeeType = employeeType;
	}


	/**
	 * @return the roomNumber
	 */
	public String getRoomNumber() {
		return roomNumber;
	}


	/**
	 * @param roomNumber the roomNumber to set
	 */
	public void setRoomNumber(String roomNumber) {
		this.roomNumber = roomNumber;
	}


	/**
	 * @return the a_mailexterne
	 */
	public String getA_mailexterne() {
		return a_mailexterne;
	}


	/**
	 * @param a_mailexterne the a_mailexterne to set
	 */
	public void setA_mailexterne(String a_mailexterne) {
		this.a_mailexterne = a_mailexterne;
	}

	/**
	 * @return the a_select
	 */
	public String[] getA_select() {
		return a_select ;
	}

	/**
	 * @param a_select the a_select to set
	 */
	public void setA_select(String[] a_select) {
		this.a_select = a_select ;
	}

	public String getEverything() {
		return super.getEverything() + "\n"
				+ "| photoURL = " + this.getPhotoURL() + "\n"
				+ "| a-promo = " + this.getA_promo() + "\n"
				+ "| l = " + this.getL() + "\n"
				+ "| employeeType = " + this.getEmployeeType() + "\n"
				+ "| roomNumber = " + this.getRoomNumber() + "\n"
				+ "| a-mailexterne = " + this.getA_mailexterne() ;
	}
}