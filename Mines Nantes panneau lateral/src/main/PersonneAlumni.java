package main;

public class PersonneAlumni extends Personne {

	/** Ville de l'entreprise */
	private String postalAddress ;
	/** Code postal de l'entreprise */
	private String postalCode ;
	/** Variable "a-oaddress", adresse de l'entreprise */
	private String a_oaddress ;
	/** Pays de l'entreprise */
	private String co ;
	/** Variable "a-classe", promotion de l'alumni */
	private String a_classe ;
	/** Variable "a-option", option de l'alumni */
	private String a_option ;
	/** Variable "a-nationality", nationalité de l'alumni */
	private String a_nationality ;
	/** variable "a-situation", situation professionnelle ("ACTIVITE" pour "En activité") */
	private String a_situation ;
	/** Adresse mail perso de l'alumni */
	private String mailperso ;
	/** Adresse mail pro de l'alumni */
	private String mailpro ;
	/**Adresse postale monobloc personnelle de l'alumni**/
	private String hpamonobloc;
	
	public PersonneAlumni(int uid) {
		super(uid);
	}

	public PersonneAlumni() {
		this(10323) ;
	}
	
	/**
	 * @return the postalAddress
	 */
	public String getPostalAddress() {
		return postalAddress;
	}

	/**
	 * @param postalAddress the postalAddress to set
	 */
	public void setPostalAddress(String postalAddress) {
		this.postalAddress = postalAddress;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the a_oaddress
	 */
	public String getA_oaddress() {
		return a_oaddress;
	}

	/**
	 * @param a_oaddress the a_oaddress to set
	 */
	public void setA_oaddress(String a_oaddress) {
		this.a_oaddress = a_oaddress;
	}

	/**
	 * @return the co
	 */
	public String getCo() {
		return co;
	}

	/**
	 * @param co the co to set
	 */
	public void setCo(String co) {
		this.co = co;
	}

	/**
	 * @return the a_classe
	 */
	public String getA_classe() {
		return a_classe;
	}

	/**
	 * @param a_classe the a_classe to set
	 */
	public void setA_classe(String a_classe) {
		this.a_classe = a_classe;
	}

	/**
	 * @return the a_option
	 */
	public String getA_option() {
		return a_option;
	}

	/**
	 * @param a_option the a_option to set
	 */
	public void setA_option(String a_option) {
		this.a_option = a_option;
	}

	/**
	 * @return the a_nationality
	 */
	public String getA_nationality() {
		return a_nationality;
	}

	/**
	 * @param a_nationality the a_nationality to set
	 */
	public void setA_nationality(String a_nationality) {
		this.a_nationality = a_nationality;
	}

	/**
	 * @return the a_situation
	 */
	public String getA_situation() {
		return a_situation;
	}

	/**
	 * @param a_situation the a_situation to set
	 */
	public void setA_situation(String a_situation) {
		this.a_situation = a_situation;
	}

	/**
	 * @return the mailperso
	 */
	public String getMailperso() {
		return mailperso;
	}

	/**
	 * @param mailperso the mailperso to set
	 */
	public void setMailperso(String mailperso) {
		this.mailperso = mailperso;
	}

	/**
	 * @return the mailpro
	 */
	public String getMailpro() {
		return mailpro;
	}

	/**
	 * @param mailpro the mailpro to set
	 */
	public void setMailpro(String mailpro) {
		this.mailpro = mailpro;
	}
	/**
	 * 
	 * @return l'adresse postale mmonobloc de l'alumni
	 */
	public String getHpaMonobloc(){
		return this.hpamonobloc;
	}
	/**
	 * 
	 * @param hpamonobloc  adresse postale monobloc de l'alumni
	 */
	public void setHpaMonobloc(String hpamonobloc){
		this.hpamonobloc=hpamonobloc;
	}

	public String getEverything() {
		return super.getEverything() + "\n"
				+ "| postalAddress = " + this.getPostalAddress() + "\n"
				+ "| postalCode = " + this.getPostalCode() + "\n"
				+ "| a-oaddress = " + this.getA_oaddress() + "\n"
				+ "| co = " + this.getCo() + "\n"
				+ "| a-classe = " + this.getA_classe() + "\n"
				+ "| a-option = " + this.getA_option() + "\n"
				+ "| a-situation = " + this.getA_situation() + "\n"
				+ "| mailperso = " + this.getMailperso() + "\n"
				+ "| mailpro = " + this.getMailpro() ;
	}
}
